//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ExtSigGenPlugin.h: Contains functions to manage DLL plugins for signal generators not
//                            built into a PicoScope.  This is intended to be a singleton class
//                            that will act as a factory to create ExtSigGen objects.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ExtSigGen.h"
#include <string>
#include <vector>

typedef enum
{
    SIG_GEN_BUILT_IN,
    SIG_GEN_EXTERNAL
} UseExtSigGen_T;

class ExtSigGenPlugin
{
    public:
        ExtSigGenPlugin(void);
        ~ExtSigGenPlugin(void);

        bool LoadExtSigGenPlugin( const std::wstring& pluginName );
        bool EnumerateExtSigGen( std::vector<std::wstring>& ids );
        ExtSigGen* OpenExtSigGen( const std::wstring id );
        void CloseExtSigGen( void );
        void UnloadExtSigGenPlugin( void );
        ExtSigGen* GetCurrentExtSigGen( void );

    private:
        ExtSigGen* pCurrentEsg;
        std::wstring loadedPluginName;
        HMODULE hLoadedModule;
        fnOpenExtSigGen ProcOpenExtSigGen;
        fnCloseExtSigGen ProcCloseExtSigGen;
        fnEnumerateExtSigGen ProcEnumerateExtSigGen;
};