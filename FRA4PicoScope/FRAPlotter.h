//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: FRAPlotter.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <Windows.h>

#include <stdint.h>
#include <string>
#include <vector>
#include <memory>
#include <tuple>
#include <stack>
#include <boost/logic/tribool.hpp>
#include "plplot.h"
using namespace boost::logic;

#define NEG3DB_MEAS 0
#define GAIN_MARGIN_MEAS 1
#define PHASE_MARGIN_MEAS 2

#define LEFT 0
#define RIGHT 1

typedef struct
{
    tuple<bool,double,double> freqAxisScale;
    tuple<bool,double,double> gainAxisScale; 
    tuple<bool,double,double> phaseAxisScale;
    tuple<double,uint8_t,bool,bool> freqAxisIntervals;
    tuple<double,uint8_t,bool,bool> gainAxisIntervals; 
    tuple<double,uint8_t,bool,bool> phaseAxisIntervals;
    bool gainMasterIntervals; 
    bool phaseMasterIntervals;
} PlotScaleSettings_T;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: class FRAPlotter
//
// Purpose: This is the class supporting plotting of Frequency Response Analysis data.  It creates
//          image pixel buffers for clients to display or save images.
//
// Parameters: N/A
//
// Notes: This class uses PLplot and Windows Imaging Component to do the bulk of the work
//
///////////////////////////////////////////////////////////////////////////////////////////////////

class FRAPlotter
{
    public:
        FRAPlotter(uint16_t width, uint16_t height);
        ~FRAPlotter(void);
        bool Initialize(void);
        void SetPlotTitle( std::string title, double scale, bool replot );
        void SetPlotSettings( bool plotGain, bool plotPhase, bool plotNeg3dB, bool plotGainMargin, bool plotPhaseMargin,
                              double gainMarginPhaseCrossover, bool unwrapPhases, bool replot, bool resetZoomStack );
        void SetPlotAxes( tuple<bool,double,double> freqAxisScale,
                          tuple<bool,double,double> gainAxisScale,
                          tuple<bool,double,double> phaseAxisScale,
                          tuple<double,uint8_t,bool,bool> freqAxisIntervals,
                          tuple<double,uint8_t,bool,bool> gainAxisIntervals,
                          tuple<double,uint8_t,bool,bool> phaseAxisIntervals,
                          bool gainMasterIntervals, bool phaseMasterIntervals, bool replot, bool resetZoomStack );
        bool BeginPlot( tuple<bool,double,double> freqAxisScale,
                        tuple<bool,double,double> gainAxisScale,
                        tuple<bool,double,double> phaseAxisScale,
                        tuple<double,uint8_t,bool,bool> freqAxisIntervals,
                        tuple<double,uint8_t,bool,bool> gainAxisIntervals,
                        tuple<double,uint8_t,bool,bool> phaseAxisIntervals,
                        bool gainMasterIntervals, bool phaseMasterIntervals,
                        bool realTime );
        bool PlotFRAPoint( double freq, double gain, double phase );

        void UpdateCursorValues( void );

        bool Zoom(tuple<int32_t,int32_t> plotOriginPoint,  // upper left screen coordinates
                  tuple<int32_t,int32_t> plotExtentPoint,  // lower right screen coordinates
                  tuple<int32_t,int32_t> zoomOriginPoint,  // screen coordinates
                  tuple<int32_t,int32_t> zoomExtentPoint); // screen coordinates

        bool UndoOneZoom(void);
        bool ZoomToOriginal(void);

        bool MoveMeasurementIndex( int measurementId, int direction );
        bool NudgeCursor( int cursorNum, int direction );

        void GetPlotScaleSettings(PlotScaleSettings_T& plotSettings);
        bool PlotDataAvailable( void );

        void EnableCursor( int cursorNum, bool enable );
        bool SetCursorOffset( int cursorNum, double offset );
        int32_t GetCursorOffset( int cursorNum );
        bool GetCursorMarkers( int cursorNum, int& gainMarkerOffset, int& phaseMarkerOffset );

        void GetPlotViewportCoordinates( int32_t &left, int32_t &right, int32_t &bottom, int32_t &top );
        void GetMeasurementTableCoordinates( int32_t& left, int32_t& top, int32_t& width, int32_t& height );
        void SetWindowHandle( HWND hWnd_ );
        void SetPlotWindowPosition( int x, int y );

        unique_ptr<uint8_t[]> GetScreenBitmapPlot32BppBGRA(void);
        unique_ptr<uint8_t[]> GetScreenBitmapMeasurementTable32BppBGRA( void );
        unique_ptr<uint8_t[]> GetPNGBitmapPlot(size_t* size);

    private:

        void InitializePLplot( void );
        void PlotFRA(void);
        void CalculateGainAndPhaseMargins( void );
        void CalculateNegative3dBPoints( void );
        void CalculateCursor( int cursorNum );
        void ProcessAxes( tuple<bool,double,double> freqAxisScale,
                          tuple<bool,double,double> gainAxisScale,
                          tuple<bool,double,double> phaseAxisScale,
                          tuple<double,uint8_t,bool,bool> freqAxisIntervals,
                          tuple<double,uint8_t,bool,bool> gainAxisIntervals,
                          tuple<double,uint8_t,bool,bool> phaseAxisIntervals,
                          bool gainMasterIntervals, bool phaseMasterIntervals,
                          bool freqScaleFromData );
        void SaveInitialAxesToZoomStack( void );
        void UpdateAutoAxes( void );
        void PlotStaticBase( void );
        void PlotDynamicBase( void );
        void EraseBase( bool eraseTitles );
        void PlotMeasurementValues( void );
        void PlotMeasurementMarkers( void );
        void TranslateCursorsToNewScale( void );
        void UnwrapLatestPhase( void );

        HANDLE hPLplotMutex = NULL;

        bool plplotInitialized;

        uint16_t plotWidth;
        uint16_t plotHeight;

        vector<uint8_t> plotBmBuffer;

        const static uint8_t bytesPerPixel = 3; // 24bpp RGB for PLPlot "memqt" driver

        // Data describing the current plot
        bool mPlotGain, mPlotPhase, mPlotNeg3dB, mPlotGainMargin, mPlotPhaseMargin;
        bool autoAxes, autoFreqAxis, autoGainAxis, autoPhaseAxis;
        bool mRealTime;
        bool initialAxesSavedToZoomStack;
        double mGainMarginPhaseCrossover;
        bool mUnwrapPhases;
        double mostRecentValidUnwrappedPhase;
        double currentFreqAxisMin; // Log Hz
        double currentFreqAxisMax; // Log Hz
        double currentGainAxisMin;
        double currentGainAxisMax;
        double currentPhaseAxisMin;
        double currentPhaseAxisMax;
        double currentFreqAxisMajorTickInterval;
        int32_t currentFreqAxisMinorTicksPerMajorInterval;
        double currentGainAxisMajorTickInterval;
        int32_t currentGainAxisMinorTicksPerMajorInterval;
        bool currentGainMasterIntervals;
        double currentPhaseAxisMajorTickInterval;
        int32_t currentPhaseAxisMinorTicksPerMajorInterval;
        bool currentPhaseMasterIntervals;
        string currentFreqAxisOpts, currentGainAxisOpts, currentPhaseAxisOpts;
        vector<double> currentFreqs, currentGains, currentPhases, currentUnwrappedPhases;
        vector<double> currentFreqsNanFree, currentGainsNanFree, currentPhasesNanFree, currentUnwrappedPhasesNanFree;
        bool dataDescending;
        bool potentialLoneGain, potentialLonePhase;
        vector<double> currentLoneFreqs, currentLoneGains, currentLonePhases, currentLoneUnwrappedPhases;
        bool dataHasNaNs;
        uint32_t numValidDataPoints;
        double minGain, maxGain, minWrappedPhase, maxWrappedPhase, minUnwrappedPhase, maxUnwrappedPhase,
            gainDelta, wrappedPhaseDelta, unwrappedPhaseDelta;
        vector<pair<double,double>> gainMargins; // frequency, gain (0 phase implied)
        vector<pair<double,double>> phaseMargins; // frequency, phase (0 gain implied)
        vector<pair<double,double>> negative3dBPoints; // frequency, phase (-3 dB gain implied)
        vector<vector<tuple<double,double,double>>> cursor; // frequency, gain, phase
        vector<tuple<double,double,double>> deltaCursor; // frequency, gain, phase
        uint32_t currentGainMarginIndex;
        uint32_t currentPhaseMarginIndex;
        uint32_t currentNegative3dBIndex;
        vector<bool> cursorEnabled;
        vector<double> cursorOffset;
        vector<double> cursorFrequency;
        int32_t measurementTableTop;
        int32_t measurementTableHeight;
        int32_t viewPortLeftEdgePx;
        int32_t viewPortRightEdgePx;
        HWND hWnd;
        int pxPlotXStart;
        int pxPlotYStart;
        std::string plotTitle;
        double plotTitleFontScale;

        // A stack of zoom values (top is current)
        stack<tuple<tuple<bool,double,double>,tuple<bool,double,double>,tuple<bool,double,double>>> zoomStack;

        PlotScaleSettings_T currentPlotScaleSettings;

        typedef enum
        {
            CMD_MAKE_SCREEN_BITMAP_PLOT_32BPP_BGRA,
            CMD_MAKE_SCREEN_BITMAP_MT_32BPP_BGRA,
            CMD_MAKE_FILE_BITMAP_PNG,
            CMD_EXIT
        } FraPlotterCommand_T;

        FraPlotterCommand_T cmd;
        uint8_t* workingBuffer;
        size_t workingBufferSize;
        tribool cmdResult;

        HANDLE hFraPlotterExecuteCommandEvent;
        HANDLE hFraPlotterCommandCompleteEvent;
        HANDLE hFraPlotterWorkerThread;

        static DWORD WINAPI WorkerThread(LPVOID lpThreadParameter);
        static int HandlePLplotError(const char* error);

        static void LabelYAxis(PLINT axis, PLFLT value, char* labelText, PLINT length, PLPointer labelData);
        void LabelYAxisExponent( PLINT scale, char* side );
        static PLINT widestYAxisLabel;
        static bool scientific;
        bool gainScientific;
        bool phaseScientific;
        static PLINT yAxisScale;

        const static double titleBarLeftEdge;
        const static double titleBarRightEdge;
        const static double titleBarBottomEdge;
        const static double titleBarTopEdge;

        const static double viewPortLeftEdge;
        const static double viewPortRightEdge;
        const static double viewPortBottomEdge;
        const static double viewPortTopEdge;

        const static double measurementTableLeftEdge;
        const static double measurementTableRightEdge;
        const static double measurementTableBottomEdge;
        const static double measurementTableTopEdge;
};

