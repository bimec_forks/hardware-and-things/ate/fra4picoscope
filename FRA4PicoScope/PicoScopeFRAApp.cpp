//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module PicoScopeFraApp.cpp: Main module for the application.
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <csignal>
#include <windowsx.h>
#include <Shlobj.h>
#include <Shlwapi.h>
#include <CommCtrl.h>
#include <Richedit.h>
#include <commdlg.h>
#include <delayimp.h>
#include <iomanip>
#include <fstream>
#include <limits>
#include <gdiplus.h>

#include "PicoScopeFRA.h"
#include "DependencyChecker.h"
#include "utility.h"
#include "StatusLog.h"
#include "ApplicationSettings.h"
#include "ScopeSelector.h"
#include "ExtSigGenPlugin.h"
#include "PicoScopeFraApp.h"
#include "FRAPlotter.h"
#include "CustomPlan.h"
#include "PlotAxesDialog.h"
#include "PlotTitleDialog.h"
#include "SettingsDialog.h"
#include "SignalGeneratorDialog.h"
#include "InteractiveRetry.h"
#include "PreRunAlert.h"
#include "psDemoImpl.h"

char* appVersionString = "0.8.2 (beta)";
char* appNameString = "Frequency Response Analyzer for PicoScope";

#define MAX_LOADSTRING 100

// Custom Windows message to handle exiting while FRA is running
#define WM_EXIT_DURING_FRA (WM_APP+0)

// Global Variables:
HINSTANCE hInst;                                // current instance
HWND hMainWnd = (HWND)NULL;
ULONG_PTR gdiplusToken;

typedef struct
{
    LOG_MESSAGE_FLAGS_T type;
    wchar_t message[1024];
} MessageLogData_T;

MessageProcessor<MessageLogData_T> * pMessageLoggingProcessor = NULL;

wstring dataDirectoryName;

bool dllLoaded[PS_NO_FAMILY-1] = { false }; // Not using a vector since that will cause a C2712 error in LoadDlls
std::vector<bool> vectDllLoaded;
CHAR sdkDllNames[PS_NO_FAMILY][32] =
{
    "ps2000.dll", "ps2000a.dll", "PS3000.dll", "ps3000a.dll", "ps4000.dll", "ps4000a.dll", "ps5000.dll", "ps5000a.dll", "ps6000.dll", "ps6000a.dll"
};
WCHAR sdkFamilyNames[PS_NO_FAMILY][32] =
{
    L"PS2000", L"PS2000A", L"PS3000", L"PS3000A", L"PS4000", L"PS4000A", L"PS5000", L"PS5000A", L"PS6000", L"PS6000A"
};

TCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

const int numAttens = NUM_ATTENS;

TCHAR attenText[numAttens][6] =
{
    TEXT("1x"), TEXT("10x"), TEXT("20x"), TEXT("25x"), TEXT("50x"), TEXT("100x"), TEXT("200x"), TEXT("250x"), TEXT("500x"), TEXT("1000x")
};

// Application Objects
PicoScopeFRA* psFRA = NULL;
FRAPlotter* fraPlotter = NULL;
ScopeSelector* pScopeSelector = NULL;
ExtSigGenPlugin* pExtSigGenPlugin = NULL;
ApplicationSettings* pSettings = NULL;

// Plot variables
bool plotInteractionAllowed = false; // true when a plot is available on the screen and we're not still plotting it
bool awaitingFirstDataPoint = false; // true when we just started FRA and are waiting on the first data point
HBITMAP hPlotBM;
HBITMAP hPlotUnavailableBM;
HBITMAP hPlotSeparatorBM;
HBITMAP hMeasurementTableBM;
HBITMAP hbmPrint = NULL;
int pxPlotXStart;
int pxPlotYStart;
int pxPlotWidth;
int pxPlotHeight;
int pxPlotSeparatorXStart;
int pxPlotSeparatorHeight;

HANDLE hExecuteFraThread;
HANDLE hExecuteFraEvent;
HANDLE hApplicationObjectsInitialized;

bool bScopePowerStateChanged = false;

// Plot zoom variables
bool detectingZoom = false;
bool zooming = false;
POINT ptZoomBegin;
POINT ptZoomEnd;

// Plot cursor variables
bool plotOnScreen = false;
int32_t plotViewportLeftWindowXCoordinate;
int32_t plotViewportRightWindowXCoordinate;
int32_t plotViewportBottomWindowYCoordinate;
int32_t plotViewportTopWindowYCoordinate;
int32_t plotViewportLeftOffset;
int32_t plotViewportRightOffset;
int32_t plotViewportBottomOffset;
int32_t plotViewportTopOffset;
int32_t measurementTableLeftWindowXCoordinate;
int32_t measurementTableTopWindowYCoordinate;
int32_t measurementTableWidth;
int32_t measurementTableHeight;
int32_t halfCursorWidthPxLarge;
int32_t halfCursorWidthPxSmall;
int32_t halfIntersectionMarkerWidthPx;
int32_t cursorOffset[2];
bool cursorHovering[2] = { false, false };
bool cursorDragging[2] = { false, false };

// Printing variables
HANDLE hDevMode = NULL;
HANDLE hDevNames = NULL;
RECT pageMargins = { 0 };
DWORD pageMarginUnits = PSD_INTHOUSANDTHSOFINCHES;
RECT pageMarginsInPageSetup = { 0 };
HBITMAP hFraPlotForPageSetupBM;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
BOOL CALLBACK       WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
void                Cleanup( HWND hWnd );
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    ScopeSelectHandler(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    SignalGeneratorSelectHandler(HWND, UINT, WPARAM, LPARAM);
BOOL                InitializeControls(HWND hDlg);
void                SmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents);
BOOL                LoadControlsData(HWND hDlg);
void                LoadDynamicChannelControls(HWND hDlg);
void                DisablePlotControls(HWND hDlg);
void                EnablePlotControls(HWND hDlg);
void                CopyLog(void);
void                ClearLog(void);

DWORD WINAPI        ExecuteFRA(LPVOID lpdwThreadParam);
void                AdjustChannelsToMatchPowerMode(void);
void                SetPlotTitle( bool replot );
bool                BeginPlot( bool& repaint );
bool                AddPlotDataPoint( double freq, double gain, double phase, bool& repaint  );
bool                GenerateFullPlot( bool resetZoomStack );
void                RepaintPlot( void );
void                InitScope( void );
void                InitEsg(bool bEnumerate = true);
void                SelectNewScope( uint8_t index );
bool                FraStatusCallback( FRA_STATUS_MESSAGE_T& fraStatus );
bool                ValidateChannelSettings(void);
bool                ValidateSettings(void);
bool                StoreSettings(void);

void                SaveRawData( wstring dataFilePath );
void                SavePlotImageFile( wstring dataFilePath );
void                EnableAllMenus( void );
void                DisableAllMenus( void );

void                LogMessageProcessor( MessageLogData_T& data );

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SignalHandler
//
// Purpose: Handle signals SIGABRT, SIGTERM, and SIGINT
//
// Parameters: signal being handled
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SignalHandler( int signum )
{
    Cleanup( hMainWnd );
    exit( signum );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: LoadDLLs
//
// Purpose: Loads demand loaded Pico SDK DLLs.
//
// Parameters: N/A
//
// Notes: Using a separate function because it helps avoid the C2712 compiler error when using
//        __try/__except handling in_tWinMain
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void LoadDLLs(void)
{
    for (uint8_t idx = 0; idx < PS_NO_FAMILY-1; idx++)
    {
        __try
        {
            HRESULT hr = __HrLoadAllImportsForDll(sdkDllNames[idx]);
            if (SUCCEEDED(hr))
            {
                dllLoaded[idx] = true;
            }
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
            continue;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: _tWinMain
//
// Purpose: Application Entry point
//
// Parameters: See Windows programming API information
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    MSG msg;
    BOOL bRet;
    HACCEL hAccelTable;
    Gdiplus::GdiplusStartupInput gdiplusStartupInput;

    WCHAR szProgramFilesFolder[MAX_PATH];
    HRESULT hr;

    WCHAR szAppPath[MAX_PATH];

    // Temporary workaround for issue 78; Can be removed if the Qt issue is fixed or Qt removed
    typedef BOOL (WINAPI *PtrSetProcessDPIAware)(VOID);
    static PtrSetProcessDPIAware ptrSetProcessDPIAware = NULL;
    ptrSetProcessDPIAware = GetProcAddress( GetModuleHandle( TEXT("user32.dll") ), "SetProcessDPIAware" );
    if (ptrSetProcessDPIAware)
    {
        ptrSetProcessDPIAware();
    }

    if (NULL == LoadLibrary(L"Riched20.dll"))
    {
        MessageBox( 0, L"Could not load Rich Edit Control Library.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    // This code is to control where the PicoScope DLLs are loaded from.  We do this so
    // that we can reliably set a preference for the ones from the installed SDK.  To do
    // this, the PicoScope DLLs are delay loaded so that we can add the SDK lib folder in
    // the DLL search path before they get loaded.  According to some sources, other methods
    // such as manifests won't work to target DLLs by specific path.
#if defined(_M_X64)
    // For future 64 bit version of this application.
    hr = SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFilesFolder);
#else
    hr = SHGetFolderPath(NULL, CSIDL_PROGRAM_FILESX86, NULL, 0, szProgramFilesFolder);
    if (FAILED(hr))
    {
        // We may be running on 32 bit Windows which does not support CSIDL_PROGRAM_FILESX86
        hr = SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, szProgramFilesFolder);
    }
#endif
    if(SUCCEEDED(hr))
    {
        wstring wsDllPath = szProgramFilesFolder + wstring(L"\\Pico Technology\\SDK\\lib");
        BOOL bRes = SetDllDirectory( wsDllPath.c_str() );
        if (!bRes)
        {
            MessageBox( 0, L"Could not set application DLL folder.", L"Fatal Error", MB_OK );
            exit(-1);
        }
        else
        {
            // Load the DLLs so that we can do all error handling in one spot
            LoadDLLs();

            vectDllLoaded = std::vector<bool>(dllLoaded, dllLoaded + PS_NO_FAMILY - 1);

            if (!CheckDependencies(vectDllLoaded))
            {
                exit(0);
            }
        }
    }
    else
    {
        MessageBox( 0, L"Could not set application DLL folder.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    // Set the PLplot library path environment variable, used to load font and color map files.
    if (0 == GetModuleFileName( NULL, szAppPath, MAX_PATH ))
    {
        MessageBox( 0, L"Could not get application path.", L"Fatal Error", MB_OK );
        exit(-1);
    }
    else
    {
        if (0 == PathRemoveFileSpec( szAppPath ))
        {
            MessageBox( 0, L"Could not get application path.", L"Fatal Error", MB_OK );
            exit(-1);
        }
        else
        {
            wstring plPlotLibEnv = wstring(L"PLPLOT_LIB=") + wstring(szAppPath);
            if (-1 == _wputenv( plPlotLibEnv.c_str() ))
            {
                MessageBox( 0, L"Could not set PLplot lib directory.", L"Fatal Error", MB_OK );
                exit(-1);
            }
        }
    }

    // Install signal handler for abort, terminate, and interrupt
    signal( SIGABRT, SignalHandler );
    signal( SIGTERM, SignalHandler );
    signal( SIGINT, SignalHandler );

    // Initialize global strings
    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_PICOSCOPEFRA, szWindowClass, MAX_LOADSTRING);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    // Initialize GDI+
    Gdiplus::GdiplusStartup( &gdiplusToken, &gdiplusStartupInput, NULL );

    hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PICOSCOPEFRA));

    // Main message loop:
    while ((bRet = GetMessage(&msg, NULL, 0, 0)) != 0)
    {
        if (bRet == -1)
        {
            return -1;
        }
        if (TranslateAccelerator(hMainWnd, hAccelTable, &msg))
        {
            continue;
        }
        if (!IsDialogMessage(hMainWnd, &msg))
        {
            TranslateMessage ( &msg );
            DispatchMessage ( &msg );
        }
    }

    return (int) msg.wParam;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SelectNewScope
//
// Purpose: Set the scope selected for use in the FRA
//
// Parameters: [in] scope: Details on the scope to swith to
//             [in] mruScope: Whether this is the most recently used scope (from the application 
//                            configuration file)
//
// Notes: Setting mruScope to true makes failure not be final (i.e., we won't report inability
//        to open the scope to the user)
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SelectNewScope( AvailableScopeDescription_T scope, bool mruScope = false )
{
    PicoScope* pScope;

    // If there is already a scope selected, close it.
    // TBD: Could implement a previous selected scope concept in the scope selector, so that
    // we can try to open the new scope first and test for success before closing the old one.
    // In that case, we'd also need to build in a way to revert the currently selected scope to
    // the previous scope
    if (pScopeSelector->GetSelectedScope())
    {
        StoreSettings();
        pSettings->WriteScopeSettings();
        pScopeSelector->GetSelectedScope()->Close();
        delete pScopeSelector->GetSelectedScope();
    }

    pScope = pScopeSelector -> OpenScope(scope);
    if (!pScope)
    {
        if (!mruScope) // If it's the most recently used scope we're tryign to open, the failure is not final.
        {
            wstringstream statusMessage;
            statusMessage << L"Error: Unable to initialize device with serial number " << scope.wSerialNumber.c_str();
            LogMessage( statusMessage.str() );

            pSettings->SetMostRecentScope( PS_NO_MODEL, PS_NO_FAMILY, string("None") );
            pSettings->SetNoScopeSettings();
        }
    }
    else
    {
        psFRA->SetInstrument(pScope);

        wstringstream statusMessage;
        wstring scopeModel, scopeSN;
        pScope->GetModel(scopeModel);
        pScope->GetSerialNumber(scopeSN);
        statusMessage << L"Status: " << scopeModel.c_str() << L" S/N: " << scopeSN.c_str() << L" successfully initialized.";
        LogMessage( statusMessage.str(), INSTRUMENT_ACCESS_DIAGNOSTICS );

        if (pScope->USB3ScopeNotOnUSB3Port())
        {
            LogMessage( L"WARNING: USB 3 scope is not connected to a USB 3 port.\r\n"
                        L"This may cause long transfer times for very large sample buffers.",
                        FRA_WARNING );
        }

        if (false == pSettings -> ReadScopeSettings( pScope ))
        {
            MessageBox( 0, L"Could not read or initialize device settings file.", L"Fatal Error", MB_OK );
            exit(-1);
        }

        // Set scope's desired timebase
        pScope->SetDesiredNoiseRejectModeTimebase(pSettings->GetNoiseRejectModeTimebaseAsInt());

        // Set the channel attenuation
        PS_CHANNEL inputChannel = (PS_CHANNEL)pSettings->GetInputChannel();
        if (inputChannel < pScope->GetNumChannels() && inputChannel != PS_CHANNEL_INVALID)
        {
            pScope->SetChannelAttenuation( inputChannel, (ATTEN_T)pSettings->GetInputAttenuation() );
        }
        PS_CHANNEL outputChannel = (PS_CHANNEL)pSettings->GetOutputChannel();
        if (outputChannel < pScope->GetNumChannels() && outputChannel != PS_CHANNEL_INVALID)
        {
            pScope->SetChannelAttenuation( outputChannel, (ATTEN_T)pSettings->GetOutputAttenuation() );
        }

        // Load current settings into the FRA object
        psFRA->SetFraSettings( pSettings->GetSamplingMode(), pSettings->GetAdaptiveStimulusMode(), pSettings->GetTargetResponseAmplitudeAsDouble(),
                               pSettings->GetSweepDescending(), pSettings->GetPhaseWrappingThresholdAsDouble() );

        // Propagate new tuning values affected by scope
        psFRA->SetFraTuning( pSettings->GetPurityLowerLimitAsFraction(), pSettings->GetExtraSettlingTimeMsAsUint16(),
                             pSettings->GetAutorangeTriesPerStepAsUint8(), pSettings->GetAutorangeToleranceAsFraction(),
                             pSettings->GetAmplitudeLowerLimitAsFraction(), pSettings->GetMaxAutorangeAmplitude(),
                             pSettings->GetInputStartingRange(), pSettings->GetOutputStartingRange(),
                             pSettings->GetAdaptiveStimulusTriesPerStepAsUint8(), pSettings->GetTargetResponseAmplitudeToleranceAsFraction(),
                             pSettings->GetLowNoiseCyclesCapturedAsUint16(), pSettings->GetNoiseRejectBandwidthAsDouble(),
                             pSettings->GetLowNoiseOversamplingAsUint16(), pSettings->GetDeviceResolution() );

        std::wstring model;
        if (pScope->GetModel(model) && 0 == model.compare(L"PSDEMO"))
        {
            ((psDemoImpl*)pScope)->SetSimulatedLpFCornerFrequency(pSettings->GetSimulatedLpfCornerFrequency());
        }

        if (!mruScope) // Don't unnecessarily dirty the settings file
        {
            pScope->GetModel(scope.model);
            pSettings->SetMostRecentScope(scope.model, scope.driverFamily, scope.serialNumber);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: InitScope
//
// Purpose: Handle scope initialization when the application is opened.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void InitScope( void )
{
    wstring statusMessage;
    ScopeDriverFamily_T mostRecentScopeFamily;
    ScopeModel_T mostRecentScopeModel;
    string mostRecentScopeSN;

    pSettings->GetMostRecentScope( mostRecentScopeModel, mostRecentScopeFamily, mostRecentScopeSN );

    // If there is a most recently used scope that's not the demo scope
    if (0 != mostRecentScopeSN.compare("None") && 0 != mostRecentScopeSN.compare("PS-DEMO-001"))
    {
        AvailableScopeDescription_T mruScope;
        mruScope.model = mostRecentScopeModel;
        mruScope.driverFamily = mostRecentScopeFamily;
        mruScope.serialNumber = mostRecentScopeSN;
        SelectNewScope( mruScope, true );
    }

    // Check to see if the above attempt to open the most recently used scope was successful.
    // If not, look for other available scopes.
    if (!(pScopeSelector->GetSelectedScope()))
    {
        vector<AvailableScopeDescription_T> scopes;

        pScopeSelector -> GetAvailableScopes( scopes );

        // It would be nice to add logic here to check whether any of the scopes found are compatible. However,
        // the PicoScope API doesn't support getting the model number during enumeration (without opening the device)
        // and thus, without other API enhancements, we can't tell whether a scope is compatible.  In leiu of that,
        // until there's a better method, we'll detect compatibility upon opening the scope, then check it before
        // executing the FRA
        if (scopes.size() == 1)
        {
            if (scopes[0].driverFamily == PSDEMO_FAMILY)
            {
                if (IDYES == MessageBox(hMainWnd, L"No PicoScope device found.  Would you like to use a demo device?", L"No PicoScope Device Found", MB_YESNO | MB_SETFOREGROUND))
                {
                    SelectNewScope(scopes[0]);
                }
                else
                {
                    statusMessage = L"Error: No compatible PicoScope device found.";
                    LogMessage( statusMessage );
                    pSettings -> SetNoScopeSettings();
                }
            }
            else
            {
                SelectNewScope(scopes[0]);
            }
        }
        else if (scopes.size() > 1)
        {
            DWORD dwDlgResp;
            dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SCOPESELECTORBOX), NULL, ScopeSelectHandler, (LPARAM)&scopes);
            if (LOWORD(dwDlgResp) == IDOK)
            {
                SelectNewScope(scopes[HIWORD(dwDlgResp)]);
            }
            else
            {
                statusMessage = L"Error: No PicoScope device selected.";
                LogMessage( statusMessage );
                pSettings -> SetNoScopeSettings();
            }
        }
        else
        {
            statusMessage = L"Error: No compatible PicoScope device found.";
            LogMessage( statusMessage );
            pSettings -> SetNoScopeSettings();
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: InitEsg
//
// Purpose: Handle external signal generator initialization when the application is opened.
//
// Parameters: [in] bEnumerate - enumerate for signal generators if the first attempt fails
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void InitEsg(bool bEnumerate)
{
    wstringstream statusMessage;
    ExtSigGen* pESG;
    bool bInitted = false;

    if (SIG_GEN_EXTERNAL == pSettings->GetSignalGeneratorSource())
    {
        if (!(pSettings->ReadEsgSettings()))
        {
            MessageBox( 0, L"Could not read or initialize signal generator settings file.", L"Fatal Error", MB_OK );
            exit(-1);
        }

        std::wstring pluginName = pSettings->GetMostRecentSignalGeneratorPlugin();
        std::wstring esgID = pSettings->GetMostRecentSignalGeneratorID();
        std::wstring esgInitString = pSettings->GetEsgSettings();
        std::wstring selectedEsgID = L"";

        // Add on DLL extension
        std::wstring pluginFilename = pluginName + L".dll";

        if (pExtSigGenPlugin->LoadExtSigGenPlugin(pluginFilename))
        {
            if (NULL != (pESG = pExtSigGenPlugin->OpenExtSigGen(esgID)) && 
                pESG->Initialize(esgInitString.c_str()))
            {
                bInitted = true;
                pSettings->InitializeEsgParameters(pESG);
            }
            else if (bEnumerate) // Try to open other available ids
            {
                std::vector<std::wstring> esgIDs;
                if (pExtSigGenPlugin->EnumerateExtSigGen(esgIDs) && esgIDs.size() != 0)
                {
                    if (esgIDs.size() > 1)
                    {
                        DWORD dwDlgResp;
                        dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SIGGENSELECTORBOX), NULL, SignalGeneratorSelectHandler, (LPARAM)&esgIDs);
                        if (LOWORD(dwDlgResp) == IDOK)
                        {
                            selectedEsgID = esgIDs[HIWORD(dwDlgResp)];
                        }
                    }
                    else
                    {
                        selectedEsgID = esgIDs[0];
                    }
                    if (selectedEsgID != L"")
                    {
                        esgID = selectedEsgID;
                        pSettings->SetMostRecentSignalGeneratorID(esgID.c_str());
                        if (!(pSettings->ReadEsgSettings()))
                        {
                            MessageBox( 0, L"Could not read or initialize signal generator settings file.", L"Fatal Error", MB_OK );
                            exit(-1);
                        }
                        esgInitString = pSettings->GetEsgSettings();
                        if (NULL != (pESG = pExtSigGenPlugin->OpenExtSigGen(esgID)) &&
                            pESG->Initialize(esgInitString.c_str()))
                        {
                            bInitted = true;
                            pSettings->InitializeEsgParameters(pESG);
                        }
                    }
                }
            }
            if (bInitted)
            {
                psFRA->SetExtSigGen(pESG);
                statusMessage << L"Status: " << pluginName.c_str() << L" ID: " << esgID.c_str() << L" successfully initialized.";
                LogMessage( statusMessage.str(), INSTRUMENT_ACCESS_DIAGNOSTICS );
            }
            else
            {
                pExtSigGenPlugin->CloseExtSigGen();
                psFRA->SetExtSigGen(NULL);
                statusMessage << L"Error: Unable to initialize external signal generator with ID " << pluginName.c_str() << L":" << esgID.c_str();
                LogMessage( statusMessage.str() );
            }
        }
        else
        {
            psFRA->SetExtSigGen(NULL);
            statusMessage << L"Error: Unable to load external signal generator plugin.";
            LogMessage( statusMessage.str() );
        }
    }
    else
    {
        if (pExtSigGenPlugin->GetCurrentExtSigGen())
        {
            pExtSigGenPlugin->CloseExtSigGen();
        }
        pExtSigGenPlugin->UnloadExtSigGenPlugin();
        psFRA->SetExtSigGen(NULL);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: InitInstance
//
// Purpose: Initialize application parameters for this instance of the application, and create the 
//          main program window.
//
// Parameters: Refer to common windows programming structure
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
    WCHAR szDocumentsFolder[MAX_PATH];

    hInst = hInstance; // Store instance handle in our global variable

    // Create the message queue used for logging status/diagnostic/warning/error messages
    pMessageLoggingProcessor = new MessageProcessor<MessageLogData_T>();

    if (NULL == pMessageLoggingProcessor)
    {
        MessageBox(0, L"Failed to create message logging queue", L"Fatal Error", MB_OK);
        exit(-1);
    }

    pMessageLoggingProcessor->SetProcessor( LogMessageProcessor );

    // Create an event that signals initialization of application objects
    hApplicationObjectsInitialized = CreateEventW( NULL, true, false, L"ApplicationObjectsInitialized" );
    if ((HANDLE)NULL == hApplicationObjectsInitialized)
    {
        MessageBox( 0, L"Could not initialize application event \"ApplicationObjectsInitialized\".", L"Fatal Error", MB_OK );
        exit(-1);
    }
    else if (ERROR_ALREADY_EXISTS == GetLastError())
    {
        MessageBox( 0, L"Cannot run multiple instances of FRA4PicoScope.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    // Create FRA execution thread and event
    hExecuteFraEvent = CreateEventW( NULL, true, false, L"ExecuteFRA" );
    if ((HANDLE)NULL == hExecuteFraEvent)
    {
        MessageBox( 0, L"Could not initialize application event \"ExecuteFRA\".", L"Fatal Error", MB_OK );
        exit(-1);
    }
    else if (ERROR_ALREADY_EXISTS == GetLastError())
    {
        MessageBox( 0, L"Cannot run multiple instances of FRA4PicoScope.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    hExecuteFraThread = CreateThread( NULL, 0, ExecuteFRA, NULL, 0, NULL );

    if ((HANDLE)NULL == hExecuteFraThread)
    {
        MessageBox( 0, L"Could not initialize application FRA execution thread.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    // Get the path of the folder we'll use to store data (raw FRA data, plot image files, etc)
    if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, szDocumentsFolder)))
    {
        dataDirectoryName = szDocumentsFolder;
        dataDirectoryName.append(L"\\FRA4PicoScope\\");
    }
    else
    {
        MessageBox( 0, L"Could not initialize application data folder.", L"Fatal Error", MB_OK );
        exit(-1);
    }

    pScopeSelector = new ScopeSelector();

    pScopeSelector->SetSDKFamiliesAvailable( vectDllLoaded );

    pExtSigGenPlugin = new ExtSigGenPlugin();

    // Create the main window
    hMainWnd = CreateDialog( hInst, MAKEINTRESOURCE(IDD_MAIN), 0, WndProc );

    if (!hMainWnd)
    {
        return FALSE;
    }

#if defined(TEST_LOG)
    FRA_STATUS_MESSAGE_T fraMsg;
    fraMsg.status = FRA_STATUS_MESSAGE;
    fraMsg.statusText = L"Test Message 1";
    FraStatusCallback( fraMsg );
    fraMsg.statusText = L"Test Message 2";
    FraStatusCallback( fraMsg );
    fraMsg.statusText = L"Test Message 3";
    FraStatusCallback( fraMsg );
#if defined( TEST_LOG_CAPACITY )
    for (int i = 0; i < 1000000; i++)
    {
        fraMsg.statusText = L"123456"; // including CR/LF, this should be 8 chars
        FraStatusCallback( fraMsg );
    }
#endif
#endif

    // Center the window, taking into consideration the actual window size
    RECT mainWindowRect;
    GetWindowRect( hMainWnd, &mainWindowRect );
    int xPos = (GetSystemMetrics(SM_CXSCREEN) - (mainWindowRect.right - mainWindowRect.left))/2;
    int yPos = (GetSystemMetrics(SM_CYSCREEN) - (mainWindowRect.bottom - mainWindowRect.top))/2;

    SetWindowPos( hMainWnd, 0, xPos, yPos, 0, 0, SWP_NOZORDER | SWP_NOSIZE );

    ShowWindow(hMainWnd, nCmdShow);
    UpdateWindow(hMainWnd);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DrawZoomRectangle
//
// Purpose: Erases the previously drawn zoom rectangle or draws a new one
//
// Parameters: [in] hWnd: Handle to the window on which the rectangle is drawn
//             [in] ptZoomBegin: coordinates for start of zoom rectangle
//             [in] ptZoomEnd: coordinate for end of zoom rectangle
//
// Notes: Used by the code that handles user mouse drag operations for zooming on the plot.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void DrawZoomRectangle (HWND hWnd, POINT ptZoomBegin, POINT ptZoomEnd)
{
     HDC hdc;

     hdc = GetDC( hWnd );

     SetROP2( hdc, R2_NOT );
     SelectObject( hdc, GetStockObject(NULL_BRUSH) );
     Rectangle( hdc, ptZoomBegin.x, ptZoomBegin.y, ptZoomEnd.x, ptZoomEnd.y );

     ReleaseDC( hWnd, hdc );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DrawCursor
//
// Purpose: Draws cursor
//
// Parameters: [in] hWnd: Handle to the window on which the cursor is drawn
//             [in] cursor: cursor number; 0 for cursor #1 and 1 for cursor #2
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void DrawCursor( HWND hWnd, int cursor )
{
    HDC hdc;
    int32_t viewportCursorOffset;
    uint32_t xPx;
    bool enabled = false;
    int gainMarkerOffset, phaseMarkerOffset;

    viewportCursorOffset = fraPlotter->GetCursorOffset( cursor );

    if (viewportCursorOffset == -1)
    {
        if (cursor == 0)
        {
            xPx = plotViewportLeftWindowXCoordinate;
        }
        else
        {
            xPx = plotViewportRightWindowXCoordinate;
        }
    }
    else if (viewportCursorOffset == -2)
    {
        xPx = plotViewportLeftWindowXCoordinate;
    }
    else if (viewportCursorOffset == -3)
    {
        xPx = plotViewportRightWindowXCoordinate;
    }
    else
    {
        xPx = plotViewportLeftWindowXCoordinate + viewportCursorOffset;
        enabled = true;
    }

    cursorOffset[cursor] = xPx - plotViewportLeftWindowXCoordinate;

    int halfCursorWidthPx = cursorHovering[cursor] || cursorDragging[cursor] ? halfCursorWidthPxLarge : halfCursorWidthPxSmall;

    hdc = GetDC( hWnd );

    Gdiplus::Graphics graphics( hdc );
    graphics.SetSmoothingMode( Gdiplus::SmoothingModeAntiAlias );

    Gdiplus::SolidBrush solidBrush = Gdiplus::Color();

    // Make bottom rectangle
    Gdiplus::Rect bottomRect( xPx - halfCursorWidthPx, plotViewportBottomWindowYCoordinate - halfCursorWidthPx, 2 * halfCursorWidthPx, 2 * halfCursorWidthPx );
    graphics.FillRectangle( &solidBrush, bottomRect );

    // Make top rectangle
    Gdiplus::Rect topRect( xPx - halfCursorWidthPx, plotViewportTopWindowYCoordinate - halfCursorWidthPx, 2 * halfCursorWidthPx, 2 * halfCursorWidthPx );
    graphics.FillRectangle( &solidBrush, topRect );

    if (enabled)
    {
        // Make connecting line
        Gdiplus::Pen connectingLinePen( Gdiplus::Color(), max( 1.0f, (float)halfIntersectionMarkerWidthPx / 3.0f ) );
        graphics.DrawLine( &connectingLinePen, xPx, plotViewportBottomWindowYCoordinate, xPx, plotViewportTopWindowYCoordinate );

        fraPlotter->GetCursorMarkers( cursor, gainMarkerOffset, phaseMarkerOffset );

        // Make gain intersection marker
        if (gainMarkerOffset >= 0)
        {
            Gdiplus::Pen gainIntersectionMarkerPen( Gdiplus::Color( 0, 0, 255 ), max( 1.0f, (float)halfIntersectionMarkerWidthPx / 4.0f ) );
            Gdiplus::Rect gainRect( xPx - halfIntersectionMarkerWidthPx, plotViewportBottomWindowYCoordinate - gainMarkerOffset - halfIntersectionMarkerWidthPx, 2 * halfIntersectionMarkerWidthPx, 2 * halfIntersectionMarkerWidthPx );
            graphics.DrawRectangle( &gainIntersectionMarkerPen, gainRect );
        }

        // Make phase intersection marker
        if (phaseMarkerOffset >= 0)
        {
            Gdiplus::Pen phaseIntersectionMarkerPen( Gdiplus::Color( 255, 0, 0 ), max( 1.0f, (float)halfIntersectionMarkerWidthPx / 4.0f ) );
            Gdiplus::Rect gainRect( xPx - halfIntersectionMarkerWidthPx, plotViewportBottomWindowYCoordinate - phaseMarkerOffset - halfIntersectionMarkerWidthPx, 2 * halfIntersectionMarkerWidthPx, 2 * halfIntersectionMarkerWidthPx );
            graphics.DrawEllipse( &phaseIntersectionMarkerPen, gainRect );
        }
    }

    ReleaseDC( hWnd, hdc );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: UpdateCursor
//
// Purpose: Restores the plot content overwritten by the last cursor update and draws new cursors
//
// Parameters: [in] hWnd: Handle to the window on which the updates are drawn
//             [in] cursor: cursor number; 0 for cursor #1 and 1 for cursor #2
//             [in] newOffset: new offset in pixels, or pass -1 to get the position from FRAPlotter
//
// Notes: Used by the code that handles user mouse drag operations for moving cursors on the plot.
//        Assumes that the SetCursorOffset function was called
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void UpdateCursor( HWND hWnd, int cursor, int32_t newOffset )
{
    HDC hdc;
    HDC hdcMem;
    HBITMAP hbmOld;
    int otherCursor;
    bool overlapping;

    otherCursor = cursor == 0 ? 1 : 0;
    overlapping = abs( cursorOffset[cursor] - cursorOffset[otherCursor] ) <= 2 * halfCursorWidthPxLarge;

    // Restore the graph content overwritten by the last cursor update.
    // When re-drawing the other cursor, need to also do this restoration otherwise anti-aliasing is undone
    hdc = GetDC( hWnd );
    hdcMem = CreateCompatibleDC( hdc );
    hbmOld = (HBITMAP)SelectObject( hdcMem, hPlotBM );
    BitBlt( hdc,
            plotViewportLeftWindowXCoordinate + cursorOffset[cursor] - halfCursorWidthPxLarge - 1,                    // x-coordinate of the upper-left corner of the destination rectangle
            plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge,                                                // y-coordinate of the upper-left corner of the destination rectangle
            2 * halfCursorWidthPxLarge + 2,                                                                           // width of the source and destination rectangles
            plotViewportBottomWindowYCoordinate - plotViewportTopWindowYCoordinate + 2 * halfCursorWidthPxLarge + 2,  // height of the source and destination rectangles
            hdcMem,
            plotViewportLeftOffset + cursorOffset[cursor] - halfCursorWidthPxLarge - 1,                               // x-coordinate of the upper-left corner of the source rectangle
            plotViewportTopOffset - halfCursorWidthPxLarge,                                                           // y-coordinate of the upper-left corner of the source rectangle
            SRCCOPY );
    if (overlapping)
    {
        BitBlt( hdc,
                plotViewportLeftWindowXCoordinate + cursorOffset[otherCursor] - halfCursorWidthPxLarge - 1,               // x-coordinate of the upper-left corner of the destination rectangle
                plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge,                                                // y-coordinate of the upper-left corner of the destination rectangle
                2 * halfCursorWidthPxLarge + 2,                                                                           // width of the source and destination rectangles
                plotViewportBottomWindowYCoordinate - plotViewportTopWindowYCoordinate + 2 * halfCursorWidthPxLarge + 2,  // height of the source and destination rectangles
                hdcMem,
                plotViewportLeftOffset + cursorOffset[otherCursor] - halfCursorWidthPxLarge - 1,                          // x-coordinate of the upper-left corner of the source rectangle
                plotViewportTopOffset - halfCursorWidthPxLarge,                                                           // y-coordinate of the upper-left corner of the source rectangle
                SRCCOPY );
    }

    SelectObject( hdcMem, hbmOld );
    DeleteDC( hdcMem );

    if (-1 != newOffset)
    {
        cursorOffset[cursor] = newOffset; // Update new cursor position
        double newOffset_ = (double)newOffset / (double)(plotViewportRightWindowXCoordinate - plotViewportLeftWindowXCoordinate);
        fraPlotter->SetCursorOffset( cursor, newOffset_ );
    }
    else
    {
        cursorOffset[cursor] = fraPlotter->GetCursorOffset( cursor );
    }

    // Plot the new cursors
    DrawCursor( hWnd, cursor );
    if (overlapping)
    {
        DrawCursor( hWnd, otherCursor );
    }

    // Update Measurement Table
    fraPlotter->UpdateCursorValues();
    unique_ptr<uint8_t[]> buffer;
    try
    {
        buffer = fraPlotter->GetScreenBitmapMeasurementTable32BppBGRA();
    }
    catch (runtime_error e)
    {
        wstringstream wss;
        wss << e.what();
        LogMessage( wss.str() );
        return;
    }

    hdc = GetDC( hWnd );
    hdcMem = CreateCompatibleDC( hdc );
    if (DeleteObject( hMeasurementTableBM ))
    {
        if (NULL != (hMeasurementTableBM = CreateBitmap( pxPlotWidth, measurementTableHeight, 1, 32, buffer.get() )))
        {
            hbmOld = (HBITMAP)SelectObject( hdcMem, hMeasurementTableBM );
            BitBlt( hdc,
                    measurementTableLeftWindowXCoordinate,  // x-coordinate of the upper-left corner of the destination rectangle
                    measurementTableTopWindowYCoordinate,   // y-coordinate of the upper-left corner of the destination rectangle
                    measurementTableWidth,                  // width of the source and destination rectangles
                    measurementTableHeight,                 // height of the source and destination rectangles
                    hdcMem,
                    0,                                      // x-coordinate of the upper-left corner of the source rectangle
                    0,                                      // y-coordinate of the upper-left corner of the source rectangle
                    SRCCOPY );
            SelectObject( hdcMem, hbmOld );
        }
    }
    DeleteDC( hdcMem );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SetPlotTitle
//
// Purpose: Sets the plot title string and font scale, handling the auto-incrementing function
//
// Parameters: [in] replot: Whether to replot the FRA plot
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SetPlotTitle( bool replot )
{
    std::wstring wcsTitle;
    if (pSettings->GetPlotTitleAutoIncrement())
    {
        std::wstringstream wssPlotTitle;
        uint16_t nextNumber = pSettings->GetPlotTitleNextNumberAsUint16();
        wssPlotTitle << pSettings->GetPlotTitle() << nextNumber;
        wcsTitle = wssPlotTitle.str();
        nextNumber++;
        pSettings->SetPlotTitleNextNumber( nextNumber );
    }
    else
    {
        wcsTitle = pSettings->GetPlotTitle();
    }
    std::string mbsTitle = ws2s( wcsTitle );
    fraPlotter->SetPlotTitle( mbsTitle, pSettings->GetPlotTitleFontScaleAsDouble(), replot );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PaintHook
//
// Purpose: Custom paints the sample page in the Page Setup Dialog
//
// Parameters: See Windows programming API information
//
// Notes: This shows the user where the plot will go on the page based on the margins, effectively
//        acting as a crude print preview
//
///////////////////////////////////////////////////////////////////////////////////////////////////

UINT_PTR CALLBACK PaintHook( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    COLORREF crMargRect;
    HDC hdc, hdcOld;

    switch (uMsg)
    {
        // Draw the margin rectangle.
        case WM_PSD_MARGINRECT:
        {
            hdc = (HDC)wParam;
            // Capture this for use in WM_PSD_GREEKTEXTRECT processing to
            // help us keep the example plot 1 pixel away from the actual margin
            pageMarginsInPageSetup = *(LPRECT)lParam;

            // Get the system highlight color.
            crMargRect = GetSysColor( COLOR_HIGHLIGHT );

            // Create a dash-dot pen of the system highlight color and
            // select it into the DC of the sample page.
            hdcOld = (HDC)SelectObject( hdc, CreatePen( PS_DASHDOT, 0, crMargRect ) );

            // Draw the margin rectangle.
            Rectangle( hdc, pageMarginsInPageSetup.left, pageMarginsInPageSetup.top, pageMarginsInPageSetup.right, pageMarginsInPageSetup.bottom );

            // Restore the previous pen to the DC.
            SelectObject( hdc, hdcOld );
            return TRUE;
        }
        // Draw the plot area.  This needs to mimick the behavior of how scaling and printing occurs.
        case WM_PSD_GREEKTEXTRECT:
        {
            LPRECT lprc;
            hdc = (HDC)wParam;
            lprc = (LPRECT)lParam;

            // Get the screen width and height of the printable rectangle
            int width = lprc->right - lprc->left;
            int height = lprc->bottom - lprc->top;

            // Assuming we take up all the available margin in a given dimension, compute what the other dimension would be
            int scaledPlotHeight = (int)((double)width * ((double)pxPlotHeight / (double)pxPlotWidth));
            int scaledPlotWidth = (int)((double)height * ((double)pxPlotWidth / (double)pxPlotHeight));

            // Figure out which dimension would spill outside the margins and adjust it to be just inside
            if (scaledPlotWidth > width)
            {
                lprc->right = pageMarginsInPageSetup.right - 1;
                lprc->bottom = lprc->top + scaledPlotHeight;
            }
            else if (scaledPlotHeight > height)
            {
                lprc->bottom = pageMarginsInPageSetup.bottom - 1;
                lprc->right = lprc->left + scaledPlotWidth;
            }
            else // Possibly neither is over
            {
                lprc->bottom = pageMarginsInPageSetup.bottom - 1;
                lprc->right = pageMarginsInPageSetup.right - 1;
            }

            // Draw the example plot
            BITMAP bm;
            HDC hdcMem;
            HBITMAP hbmOld;

            hdcMem = CreateCompatibleDC(hdc);

            hbmOld = (HBITMAP)SelectObject( hdcMem, hFraPlotForPageSetupBM );
            GetObject( hFraPlotForPageSetupBM, sizeof(bm), &bm );
            StretchBlt( hdc, lprc->left, lprc->top, lprc->right - lprc->left, lprc->bottom - lprc->top, hdcMem, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY );

            SelectObject(hdcMem, hbmOld);
            DeleteDC(hdcMem);

            return TRUE;
        }
        default:
            return FALSE;
    }
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: WndProc
//
// Purpose: Message handler for the main window
//
// Parameters: See Windows programming API information
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    HMENU hMenu;
    PAINTSTRUCT ps;
    HDC hdc;
    HDC hdcMem;
    HBITMAP hbmOld;
    vector<AvailableScopeDescription_T> scopes;

    switch (message)
    {
        case WM_INITDIALOG:
        {
            WCHAR szAppDataFolder[MAX_PATH];

            hMainWnd = hWnd; // Doing this here so that FraStatusCallback works when construct the objects below

            hMenu = LoadMenu( hInst, MAKEINTRESOURCE(IDC_PICOSCOPEFRA));
            SetMenu( hWnd, hMenu );

            InitializeControls(hWnd);

            try
            {
                psFRA = new PicoScopeFRA(FraStatusCallback, SmartProbeCallback);
            }
            catch (runtime_error e)
            {
                UNREFERENCED_PARAMETER(e);
                MessageBox( 0, L"Could not initialize FRA execution subsystem.", L"Fatal Error", MB_OK );
                exit(-1);
            }
            if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, szAppDataFolder)))
            {
                pSettings = new ApplicationSettings( szAppDataFolder );
            }
            else
            {
                MessageBox( 0, L"Could not initialize application settings folder.", L"Fatal Error", MB_OK );
                exit(-1);
            }

            if (!(pSettings->ReadApplicationSettings()))
            {
                MessageBox( 0, L"Could not read or initialize application settings file.", L"Fatal Error", MB_OK );
                exit(-1);
            }
            InitEsg();
            InitScope();

            // Warn about any SDK DLLs not found
            bool anyDllNotLoaded = false;
            bool bInsertComma = false;
            wstringstream wss;
            wss << L"WARNING: Unable to load SDK for PicoScope Families: ";
            for (uint8_t idx = 0; idx < PS_NO_FAMILY - 1; idx++)
            {
                if (!vectDllLoaded[idx])
                {
                    anyDllNotLoaded = true;
                    if (bInsertComma)
                    {
                        wss << L", ";
                    }
                    wss << sdkFamilyNames[idx];
                    bInsertComma = true;
                }
            }
            if (anyDllNotLoaded)
            {
                LogMessage(wss.str(), FRA_WARNING);
            }

            if (pSettings->GetTimeDomainPlotsEnabled())
            {
                try
                {
                    psFRA->EnableDiagnostics(dataDirectoryName);
                }
                catch (runtime_error e)
                {
                    UNREFERENCED_PARAMETER(e);
                    LogMessage(L"WARNING: Unable to enable time domain diagnostic data.", FRA_WARNING);
                }
            }
            else
            {
                psFRA->DisableDiagnostics();
            }

            LoadControlsData(hMainWnd);

            fraPlotter = new FRAPlotter(pxPlotWidth, pxPlotHeight);
            if (!fraPlotter || !(fraPlotter->Initialize()))
            {
                MessageBox( 0, L"Could not initialize plotting subsystem.", L"Fatal Error", MB_OK );
                exit(-1);
            }

            fraPlotter->SetWindowHandle( hMainWnd );
            fraPlotter->SetPlotWindowPosition( pxPlotXStart, pxPlotYStart );

            fraPlotter -> GetPlotViewportCoordinates( plotViewportLeftOffset, plotViewportRightOffset, plotViewportBottomOffset, plotViewportTopOffset );
            plotViewportLeftWindowXCoordinate = pxPlotXStart + plotViewportLeftOffset;
            plotViewportRightWindowXCoordinate = pxPlotXStart + plotViewportRightOffset;
            plotViewportBottomWindowYCoordinate = pxPlotYStart + plotViewportBottomOffset;
            plotViewportTopWindowYCoordinate = pxPlotYStart + plotViewportTopOffset;

            fraPlotter->GetMeasurementTableCoordinates( measurementTableLeftWindowXCoordinate, measurementTableTopWindowYCoordinate, measurementTableWidth, measurementTableHeight );
            measurementTableLeftWindowXCoordinate += pxPlotXStart;
            measurementTableTopWindowYCoordinate += pxPlotYStart;

            hPlotBM = (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_PLOT_UNAVAILABLE), IMAGE_BITMAP, pxPlotWidth, pxPlotHeight, 0);
            hPlotSeparatorBM = (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_SEPARATOR), IMAGE_BITMAP, 2, pxPlotSeparatorHeight, 0);
            hFraPlotForPageSetupBM = (HBITMAP)LoadImage( hInst, MAKEINTRESOURCE( IDB_FRA_PLOT_FOR_PS ), IMAGE_BITMAP, pxPlotWidth, pxPlotHeight, 0 );
            // Load dummy image here to allow first DeletObject to succeed
            hMeasurementTableBM = (HBITMAP)LoadImage( hInst, MAKEINTRESOURCE( IDB_SEPARATOR ), IMAGE_BITMAP, 2, pxPlotSeparatorHeight, 0 );

            // Initialize printer variables
            PAGESETUPDLG ps;
            ZeroMemory( &ps, sizeof ps );

            ps.lStructSize = sizeof ps;
            ps.Flags = PSD_RETURNDEFAULT;

            if (PageSetupDlg( &ps ))
            {
                hDevMode = ps.hDevMode;
                hDevNames = ps.hDevNames;
                pageMargins = ps.rtMargin;
                pageMarginUnits = ps.Flags & (PSD_INTHOUSANDTHSOFINCHES | PSD_INHUNDREDTHSOFMILLIMETERS);
            }

            // Signal that we're done initializing application objects
            SetEvent(hApplicationObjectsInitialized);

            return TRUE;
        }
        case WM_COMMAND:
            wmId    = LOWORD(wParam);
            wmEvent = HIWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
                case IDC_CUSTOM_BUTTON:
                case IDC_CUSTOM_PLAN_PLUS_BUTTON: // Alternate way to access the custom plan dialog;
                                                  // Relies on accelerator table used by top level app dialog message loop
                {
                    PicoScope* pSelectedScope = pScopeSelector->GetSelectedScope();
                    if (pSelectedScope)
                    {
                        if (!pSelectedScope->RequiresExternalSignalGenerator() ||
                            NULL != pExtSigGenPlugin->GetCurrentExtSigGen())
                        {
                            if (ValidateChannelSettings())
                            {
                                // Using a custom message loop so that we can use an accelerator table for Custom Plan Dialog
                                MSG msg;
                                BOOL bRet;
                                HACCEL hAccelTable;
                                HWND hDlg;

                                hAccelTable = LoadAccelerators( hInst, MAKEINTRESOURCE( IDC_CUSTOM_PLAN_ACCEL ) );

                                hDlg = CreateDialog( hInst, MAKEINTRESOURCE( IDD_CUSTOM_PLAN ), hMainWnd, CustomPlanDialogHandler );

                                while ((bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
                                {
                                    if (bRet == -1)
                                    {
                                        break;
                                    }
                                    if (TranslateAccelerator( hDlg, hAccelTable, &msg ))
                                    {
                                        continue;
                                    }
                                    if (!IsDialogMessage( hDlg, &msg ))
                                    {
                                        TranslateMessage( &msg );
                                        DispatchMessage( &msg );
                                    }
                                }
                            }
                        }
                        else
                        {
                            MessageBox( hMainWnd, L"Connect an external signal generator to define a custom plan.", L"Error", MB_OK );
                        }
                    }
                    else
                    {
                        MessageBox(hMainWnd, L"Connect a PicoScope to define a custom plan.", L"Error", MB_OK);
                    }
                    return TRUE;
                }
                case IDC_CUSTOM_CHECK:
                {
                    StoreSettings();
                    LoadControlsData(hMainWnd);
                    return TRUE;
                }
                case IDM_ABOUT:
                    DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                    return TRUE;
                case IDM_CONNECT_SCOPE:
                {
                    pScopeSelector->GetAvailableScopes(scopes);
                    if (scopes.size() == 0)
                    {
                        MessageBox( hWnd, L"No compatible PicoScope device found.", L"Error", MB_OK | MB_ICONEXCLAMATION );
                    }
                    else
                    {
                        DWORD dwDlgResp;
                        dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SCOPESELECTORBOX), hWnd, ScopeSelectHandler, (LPARAM)&scopes);
                        if (LOWORD(dwDlgResp) == IDOK)
                        {
                            SelectNewScope(scopes[HIWORD(dwDlgResp)]);
                            LoadControlsData(hMainWnd);
                        }
                    }
                    return TRUE;
                }
                case IDM_CONNECT_SIGNALGENERATOR:
                {
                    if (pExtSigGenPlugin->GetCurrentExtSigGen())
                    {
                        pSettings->WriteEsgSettings();
                    }
                    if (pSettings->GetSignalGeneratorSource() == SIG_GEN_BUILT_IN || pSettings->GetMostRecentSignalGeneratorPlugin() == L"")
                    {
                        // Give the user a chance to configure the plugin
                        StoreSettings();
                        DWORD dwDlgResp;
                        dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SIGNALGENERATOR), hWnd, SignalGeneratorDialogHandler, NULL);
                        // If it's still not configured, don't try to enumerate
                        if (pSettings->GetSignalGeneratorSource() == SIG_GEN_BUILT_IN || pSettings->GetMostRecentSignalGeneratorPlugin() == L"")
                        {
                            MessageBox( hWnd, L"Signal generator plugin not configured.", L"Error", MB_OK | MB_ICONEXCLAMATION );
                            return TRUE;
                        }
                    }
                    std::wstring pluginName = pSettings->GetMostRecentSignalGeneratorPlugin() + L".dll";
                    if (pExtSigGenPlugin->LoadExtSigGenPlugin(pluginName))
                    {
                        std::vector<std::wstring> esgIDs;
                        if (pExtSigGenPlugin->EnumerateExtSigGen(esgIDs))
                        {
                            if (esgIDs.size() == 0)
                            {
                                MessageBox( hWnd, L"No compatible signal generators found.", L"Error", MB_OK | MB_ICONEXCLAMATION );
                            }
                            else
                            {
                                DWORD dwDlgResp;
                                dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SIGGENSELECTORBOX), hWnd, SignalGeneratorSelectHandler, (LPARAM)&esgIDs);
                                if (LOWORD(dwDlgResp) == IDOK)
                                {
                                    pSettings->SetMostRecentSignalGeneratorID(esgIDs[HIWORD(dwDlgResp)].c_str());
                                    InitEsg(false);
                                    LoadControlsData(hMainWnd);
                                }
                            }
                        }
                        else // Enumeration not supported by plugin
                        {
                            InitEsg(false);
                            LoadControlsData(hMainWnd);
                        }
                    }
                    else
                    {
                        MessageBox( hWnd, L"No compatible signal generators found.", L"Error", MB_OK | MB_ICONEXCLAMATION );
                    }
                    return TRUE;
                }
                case IDM_DISCONNECT_SIGNALGENERATOR:
                {
                    if (pExtSigGenPlugin->GetCurrentExtSigGen())
                    {
                        if (IDOK == MessageBox( 0, L"Disconnect Signal Generator?", L"Confirm", MB_OKCANCEL ))
                        {
                            pExtSigGenPlugin->CloseExtSigGen();
                            psFRA->SetExtSigGen(NULL);
                            pSettings->WriteEsgSettings();
                            pSettings->SetSignalGeneratorSource(SIG_GEN_BUILT_IN);
                            LoadControlsData(hMainWnd);
                            std::wstring statusMessage = L"Status: Signal generator disconnected.";
                            LogMessage( statusMessage, INSTRUMENT_ACCESS_DIAGNOSTICS );
                        }
                    }
                    else
                    {
                        std::wstring statusMessage = L"Status: No signal generator connected.";
                        LogMessage(statusMessage, INSTRUMENT_ACCESS_DIAGNOSTICS);
                    }
                    return TRUE;
                }
                case IDM_SAVE:
                {
                    time_t now;
                    struct tm  currentTime;
                    wchar_t dataFileName[128];
                    now = time(0);
                    localtime_s( &currentTime, &now );
                    wcsftime( dataFileName, sizeof(dataFileName)/sizeof(wchar_t), L"FRA Data %B %d, %Y %H-%M-%S.png", &currentTime );
                    SavePlotImageFile(dataDirectoryName+dataFileName);
                    return TRUE;
                }
                case IDM_SAVEAS:
                {
                    if (!fraPlotter->PlotDataAvailable())
                    {
                        MessageBox( 0, L"No plot is available to save.", L"Error", MB_OK );
                    }
                    else
                    {
                        OPENFILENAME ofn;
                        WCHAR szFileName[MAX_PATH] = L"";
                        ZeroMemory(&ofn, sizeof(ofn));
                        ofn.lStructSize = sizeof(ofn);
                        ofn.lpstrFilter = (LPCWSTR)L"PNG Files (*.png)\0*.png\0All Files (*.*)\0*.*\0";
                        ofn.lpstrFile = (LPWSTR)szFileName;
                        ofn.lpstrInitialDir = dataDirectoryName.c_str();
                        ofn.nMaxFile = MAX_PATH;
                        ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;
                        ofn.lpstrDefExt = (LPCWSTR)L"png";

                        if (GetSaveFileName( &ofn ))
                        {
                            SavePlotImageFile(ofn.lpstrFile);
                        }
                    }
                    return TRUE;
                }
                case IDM_EXPORTDATA:
                {
                    time_t now;
                    struct tm  currentTime;
                    wchar_t dataFileName[128];
                    now = time(0);
                    localtime_s( &currentTime, &now );
                    wcsftime( dataFileName, sizeof(dataFileName)/sizeof(wchar_t), L"FRA Data %B %d, %Y %H-%M-%S.csv", &currentTime );
                    SaveRawData(dataDirectoryName+dataFileName);
                    return TRUE;
                }
                case IDM_EXPORTDATAAS:
                {
                    int numSteps;
                    double *freqsLogHz, *phasesDeg, *unwrappedPhasesDeg, *gainsDb;

                    psFRA->GetResults( &numSteps, &freqsLogHz, &gainsDb, &phasesDeg, &unwrappedPhasesDeg );

                    if (numSteps == 0)
                    {
                        MessageBox( 0, L"No data is available to export.", L"Error", MB_OK );
                    }
                    else
                    {
                        OPENFILENAME ofn;
                        WCHAR szFileName[MAX_PATH] = L"";
                        ZeroMemory(&ofn, sizeof(ofn));
                        ofn.lStructSize = sizeof(ofn);
                        ofn.lpstrFilter = (LPCWSTR)L"CSV Files (*.csv)\0*.csv\0All Files (*.*)\0*.*\0";
                        ofn.lpstrFile = (LPWSTR)szFileName;
                        ofn.lpstrInitialDir = dataDirectoryName.c_str();
                        ofn.nMaxFile = MAX_PATH;
                        ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;
                        ofn.lpstrDefExt = (LPCWSTR)L"csv";

                        if (GetSaveFileName( &ofn ))
                        {
                            SaveRawData(ofn.lpstrFile);
                        }
                    }
                    return TRUE;
                }
                case IDM_PAGESETUP:
                {
                    PAGESETUPDLG ps;
                    ZeroMemory( &ps, sizeof ps );

                    ps = { sizeof( ps ), hWnd, hDevMode, hDevNames };
                    ps.Flags = PSD_DEFAULTMINMARGINS | PSD_MARGINS | PSD_ENABLEPAGEPAINTHOOK | pageMarginUnits;
                    ps.rtMargin = pageMargins;
                    ps.lpfnPagePaintHook = PaintHook;

                    if (PageSetupDlg( &ps ))
                    {
                        hDevMode = ps.hDevMode;
                        hDevNames = ps.hDevNames;

                        pageMargins = ps.rtMargin;
                        pageMarginUnits = ps.Flags & (PSD_INTHOUSANDTHSOFINCHES | PSD_INHUNDREDTHSOFMILLIMETERS);
                    }

                    return TRUE;
                }
                case IDM_PRINTPLOT:
                {
                    if (!fraPlotter->PlotDataAvailable())
                    {
                        MessageBox( 0, L"No plot is available to print.", L"Error", MB_OK );
                    }
                    else
                    {
                        PRINTDLGEX pdlgx;
                        ZeroMemory( &pdlgx, sizeof pdlgx );
                        pdlgx = { sizeof( pdlgx ), hWnd, hDevMode, hDevNames };
                        pdlgx.Flags = PD_RETURNDC | PD_NOSELECTION | PD_NOPAGENUMS | PD_NOCURRENTPAGE | PD_NOWARNING | PD_HIDEPRINTTOFILE;
                        pdlgx.nStartPage = START_PAGE_GENERAL;

                        if (S_OK == PrintDlgEx( (LPPRINTDLGEX)&pdlgx ))
                        {
                            if (PD_RESULT_CANCEL != pdlgx.dwResultAction)
                            {
                                hDevMode = pdlgx.hDevMode;
                                hDevNames = pdlgx.hDevNames;
                            }

                            if (PD_RESULT_PRINT == pdlgx.dwResultAction)
                            {
                                if (NULL == pdlgx.hDC)
                                {
                                    MessageBox( 0, L"Error: Unable to print.  Printer device context is null.", L"Error", MB_OK );
                                }
                                else
                                {
                                    DOCINFOW di;
                                    di.cbSize = sizeof( DOCINFO );
                                    di.lpszDocName = L"FRA4PicoScope Plot";
                                    di.lpszOutput = NULL;
                                    di.lpszDatatype = NULL;
                                    di.fwType = 0;

                                    HDC hdcPrinter = pdlgx.hDC;
                                    StartDoc( hdcPrinter, &di );
                                    StartPage( hdcPrinter );

                                    // Get the bitmap from the screen instead of FRAPlotter since it will include the cursors
                                    HDC hdcWindow = GetDC( hWnd );
                                    HDC hdcMem = CreateCompatibleDC( hdcWindow );
                                    hbmPrint = CreateCompatibleBitmap( hdcWindow, pxPlotWidth, pxPlotHeight );
                                    hbmOld = (HBITMAP)SelectObject( hdcMem, hbmPrint );
                                    BitBlt( hdcMem, 0, 0, pxPlotWidth, pxPlotHeight, hdcWindow, pxPlotXStart, pxPlotYStart, SRCCOPY );
                                    SelectObject( hdcMem, hbmOld );
                                    DeleteDC( hdcMem );

                                    // Scaling
                                    int physicalWidth = GetDeviceCaps( hdcPrinter, PHYSICALWIDTH );
                                    int physicalHeight = GetDeviceCaps( hdcPrinter, PHYSICALHEIGHT );
                                    int physicalOffsetX = GetDeviceCaps( hdcPrinter, PHYSICALOFFSETX );
                                    int physicalOffsetY = GetDeviceCaps( hdcPrinter, PHYSICALOFFSETY );
                                    int leftOffset, rightOffset, topOffset, bottomOffset;

                                    // Because LOGPIXELSX/Y are in thousandths of an inch and the user may have set margins in hundreths of a millimeter
                                    int unitsScalingFactor = (pageMarginUnits & PSD_INTHOUSANDTHSOFINCHES) ? 1000 : 2540;

                                    leftOffset = MulDiv( pageMargins.left, GetDeviceCaps( pdlgx.hDC, LOGPIXELSX ), unitsScalingFactor );
                                    rightOffset = MulDiv( pageMargins.right, GetDeviceCaps( pdlgx.hDC, LOGPIXELSX ), unitsScalingFactor );
                                    topOffset = MulDiv( pageMargins.top, GetDeviceCaps( pdlgx.hDC, LOGPIXELSY ), unitsScalingFactor );
                                    bottomOffset = MulDiv( pageMargins.bottom, GetDeviceCaps( pdlgx.hDC, LOGPIXELSY ), unitsScalingFactor );

                                    // Don't let margins go outside physical printing limits
                                    leftOffset = max( leftOffset, physicalOffsetX );
                                    rightOffset = max( rightOffset, physicalOffsetX );
                                    topOffset = max( topOffset, physicalOffsetY );
                                    bottomOffset = max( bottomOffset, physicalOffsetY );

                                    SetMapMode( hdcPrinter, MM_ISOTROPIC );
                                    SetWindowExtEx( hdcPrinter, pxPlotWidth, pxPlotHeight, NULL );
                                    SetViewportExtEx( hdcPrinter, physicalWidth - leftOffset - rightOffset, physicalHeight - topOffset - bottomOffset, NULL );
                                    SetViewportOrgEx( hdcPrinter, leftOffset - physicalOffsetX, topOffset - physicalOffsetY, NULL );
                                    // End scaling

                                    hdcMem = CreateCompatibleDC( hdcPrinter );
                                    hbmOld = (HBITMAP)SelectObject( hdcMem, hbmPrint );
                                    BitBlt( hdcPrinter, 0, 0, pxPlotWidth, pxPlotHeight, hdcMem, 0, 0, SRCCOPY );

                                    SelectObject( hdcMem, hbmOld );
                                    DeleteDC( hdcMem );

                                    EndPage( hdcPrinter );
                                    EndDoc( hdcPrinter );
                                    DeleteObject( hdcPrinter );
                                }
                            }
                        }
                    }
                    return (TRUE);
                }
                case IDM_SETTINGS:
                {
                    StoreSettings();
                    DWORD dwDlgResp;
                    dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SETTINGS), hWnd, SettingsDialogHandler, (LPARAM)psFRA);
                    if (LOWORD(dwDlgResp) == IDOK)
                    {
                        LoadControlsData(hMainWnd);

                        psFRA->SetFraSettings( pSettings->GetSamplingMode(), pSettings->GetAdaptiveStimulusMode(), pSettings->GetTargetResponseAmplitudeAsDouble(),
                                               pSettings->GetSweepDescending(), pSettings->GetPhaseWrappingThresholdAsDouble() );

                        psFRA->SetFraTuning( pSettings->GetPurityLowerLimitAsFraction(), pSettings->GetExtraSettlingTimeMsAsUint16(),
                                             pSettings->GetAutorangeTriesPerStepAsUint8(), pSettings->GetAutorangeToleranceAsFraction(),
                                             pSettings->GetAmplitudeLowerLimitAsFraction(), pSettings->GetMaxAutorangeAmplitude(),
                                             pSettings->GetInputStartingRange(), pSettings->GetOutputStartingRange(),
                                             pSettings->GetAdaptiveStimulusTriesPerStepAsUint8(), pSettings->GetTargetResponseAmplitudeToleranceAsFraction(),
                                             pSettings->GetLowNoiseCyclesCapturedAsUint16(), pSettings->GetNoiseRejectBandwidthAsDouble(),
                                             pSettings->GetLowNoiseOversamplingAsUint16(), pSettings->GetDeviceResolution() );

                        if (pScopeSelector->GetSelectedScope())
                        {
                            pScopeSelector->GetSelectedScope()->SetDesiredNoiseRejectModeTimebase(pSettings->GetNoiseRejectModeTimebaseAsInt());
                        }

                        if (pSettings->GetTimeDomainPlotsEnabled())
                        {
                            try
                            {
                                psFRA->EnableDiagnostics(dataDirectoryName);
                            }
                            catch (runtime_error e)
                            {
                                UNREFERENCED_PARAMETER(e);
                                LogMessage(L"WARNING: Unable to enable time domain diagnostic data.", FRA_WARNING);
                            }
                        }
                        else
                        {
                            psFRA->DisableDiagnostics();
                        }
                    }
                    return TRUE;
                }
                case IDM_SIGNALGENERATOR:
                {
                    StoreSettings();
                    if (pExtSigGenPlugin->GetCurrentExtSigGen())
                    {
                        pSettings->WriteEsgSettings();
                    }
                    DWORD dwDlgResp;
                    dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_SIGNALGENERATOR), hWnd, SignalGeneratorDialogHandler, NULL);
                    if (LOWORD(dwDlgResp) == ID_CHANGED)
                    {
                        InitEsg();
                        LoadControlsData(hMainWnd);
                    }
                    return TRUE;
                }
                case IDM_EXIT:
                    Cleanup(hWnd);
                    return TRUE;
                case IDC_CANCEL:
                    psFRA -> CancelFRA();
                    return TRUE;
                case IDC_GO:
                    if (WaitForSingleObject(hExecuteFraEvent, 0) == WAIT_TIMEOUT) // Is the event not already signalled?
                    {
                        if (BST_CHECKED == IsDlgButtonChecked( hMainWnd, IDC_AUTOCLEAR ))
                        {
                            ClearLog();
                        }
                        // Cancel any zooming, dragging or hovering on the plot
                        SetCursor( LoadCursor( NULL, IDC_ARROW ) );
                        ReleaseCapture();
                        detectingZoom = zooming = false;
                        cursorDragging[0] = cursorHovering[0] = cursorDragging[1] = cursorHovering[1] = false;
                        // Signal to start the FRA
                        SetEvent( hExecuteFraEvent );
                    }
                    return TRUE;
                case IDM_CAL:
                    return TRUE;
                case IDC_COPY:
                    CopyLog();
                    return TRUE;
                case IDC_CLEAR:
                    ClearLog();
                    return TRUE;
                case IDC_FRA_AUTO_AXES:
                case IDC_FRA_STORED_AXES:
                case IDC_FRA_PLOT_GAIN:
                case IDC_FRA_PLOT_PHASE:
                case IDC_FRA_PLOT_NEG3DB:
                case IDC_FRA_PLOT_GM:
                case IDC_FRA_PLOT_PM:
                case IDC_FRA_UNWRAP_PHASE:
                    StoreSettings();

                    // Re-evaluate plot controls
                    EnablePlotControls(hWnd);

                    // Store again since they may have changed
                    StoreSettings();

                    if (fraPlotter->PlotDataAvailable())
                    {
                        bool rescale = (wmId == IDC_FRA_AUTO_AXES ||
                                        wmId == IDC_FRA_STORED_AXES ||
                                        wmId == IDC_FRA_UNWRAP_PHASE);
                        (void)GenerateFullPlot(rescale);
                        RepaintPlot();
                    }
                    return TRUE;
                case IDC_INPUT_CHANNEL:
                case IDC_OUTPUT_CHANNEL:
                    {
                        if (CBN_SELCHANGE == HIWORD(wParam))
                        {
                            LoadDynamicChannelControls(hWnd);
                        }
                    }
                    return TRUE;
                case IDC_INPUT_ATTEN:
                case IDC_OUTPUT_ATTEN:
                    {
                        if (CBN_SELCHANGE == HIWORD(wParam))
                        {
                            HWND hAttenCB;
                            HWND hChanCB;
                            PicoScope* pScope;
                            if (NULL != (pScope = pScopeSelector->GetSelectedScope()))
                            {
                                ATTEN_T atten;
                                PS_CHANNEL chan;
                                hAttenCB = wmId == IDC_INPUT_ATTEN ? GetDlgItem( hWnd, IDC_INPUT_ATTEN ) :
                                                                     GetDlgItem( hWnd, IDC_OUTPUT_ATTEN );
                                hChanCB = wmId == IDC_INPUT_ATTEN ? GetDlgItem( hWnd, IDC_INPUT_CHANNEL ) :
                                                                    GetDlgItem( hWnd, IDC_OUTPUT_CHANNEL );
                                atten = (ATTEN_T)ComboBox_GetCurSel( hAttenCB );
                                chan = (PS_CHANNEL)ComboBox_GetCurSel( hChanCB );
                                pScope->SetChannelAttenuation(chan, atten);
                            }
                        }
                    }
                    return TRUE;
                case IDCANCEL:
                    {
                        // Esc key was pressed.  Cancel zoom operation if it's happening.
                        if (zooming)
                        {
                            DrawZoomRectangle( hWnd, ptZoomBegin, ptZoomEnd );
                            ReleaseCapture();
                            SetCursor(LoadCursor(NULL, IDC_ARROW));
                            zooming = false;
                            detectingZoom = false;
                        }
                    }
                    return TRUE;
                default:
                    return FALSE;
            }
            return TRUE;
            break;
        case WM_LBUTTONDOWN:
            {
                POINT leftClickPoint = { GET_X_LPARAM( lParam ), GET_Y_LPARAM( lParam ) };

                uint32_t cursor1Distance = (std::numeric_limits<uint32_t>::max)();
                uint32_t cursor2Distance = (std::numeric_limits<uint32_t>::max)();

                // Check to see if it was a click on cursor 1 control box.
                if (plotInteractionAllowed && leftClickPoint.x >= plotViewportLeftWindowXCoordinate + cursorOffset[0] - halfCursorWidthPxLarge && leftClickPoint.x <= plotViewportLeftWindowXCoordinate + cursorOffset[0] + halfCursorWidthPxLarge &&
                    ((leftClickPoint.y >= plotViewportBottomWindowYCoordinate - halfCursorWidthPxLarge && leftClickPoint.y <= plotViewportBottomWindowYCoordinate + halfCursorWidthPxLarge) ||
                     (leftClickPoint.y >= plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge && leftClickPoint.y <= plotViewportTopWindowYCoordinate + halfCursorWidthPxLarge)))
                {
                    cursor1Distance = abs( leftClickPoint.x - cursorOffset[0] );
                }
                // Check to see if it was a click on cursor 2 control box.
                if (plotInteractionAllowed && leftClickPoint.x >= plotViewportLeftWindowXCoordinate + cursorOffset[1] - halfCursorWidthPxLarge && leftClickPoint.x <= plotViewportLeftWindowXCoordinate + cursorOffset[1] + halfCursorWidthPxLarge &&
                    ((leftClickPoint.y >= plotViewportBottomWindowYCoordinate - halfCursorWidthPxLarge && leftClickPoint.y <= plotViewportBottomWindowYCoordinate + halfCursorWidthPxLarge) ||
                     (leftClickPoint.y >= plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge && leftClickPoint.y <= plotViewportTopWindowYCoordinate + halfCursorWidthPxLarge)))
                {
                    cursor2Distance = abs( leftClickPoint.x - cursorOffset[1] );
                }

                if (cursor1Distance != (std::numeric_limits<uint32_t>::max)() ||
                    cursor2Distance != (std::numeric_limits<uint32_t>::max)())
                {
                    SetCapture( hWnd );
                    if (cursor1Distance <= cursor2Distance)
                    {
                        cursorDragging[0] = true;
                        UpdateCursor( hWnd, 0, cursorOffset[0] );
                    }
                    else if (cursor1Distance > cursor2Distance)
                    {
                        cursorDragging[1] = true;
                        UpdateCursor( hWnd, 1, cursorOffset[1] );
                    }
                }
                // Check to see if it was a click on the plot area for a possible zoom operation.
                else if (plotInteractionAllowed && leftClickPoint.x >= pxPlotXStart && leftClickPoint.x < pxPlotXStart+pxPlotWidth && leftClickPoint.y >= pxPlotYStart && leftClickPoint.y < pxPlotYStart+pxPlotHeight)
                {

                    ptZoomEnd.x = ptZoomBegin.x = leftClickPoint.x;
                    ptZoomEnd.y = ptZoomBegin.y = leftClickPoint.y;

                    SetCapture (hWnd) ;
                    SetCursor (LoadCursor (NULL, IDC_CROSS)) ;

                    detectingZoom = true;
                }
            }
            break;
        case WM_MOUSEMOVE:
            {
                POINT mouseMovePoint = { GET_X_LPARAM( lParam ), GET_Y_LPARAM( lParam ) };

                if (cursorDragging[0])
                {
                    if (plotInteractionAllowed)
                    {
                        int32_t newCursorOffset = mouseMovePoint.x - plotViewportLeftWindowXCoordinate;
                        // Bound to the viewport
                        int32_t boundCursorOffset = min( max( newCursorOffset, 0 ), plotViewportRightWindowXCoordinate - plotViewportLeftWindowXCoordinate );
                        if (newCursorOffset - boundCursorOffset > -15 * halfCursorWidthPxLarge)
                        {
                            fraPlotter->EnableCursor( 0, true );
                        }
                        else
                        {
                            fraPlotter->EnableCursor( 0, false );
                            cursorDragging[0] = false;
                            ReleaseCapture();
                        }
                        UpdateCursor( hWnd, 0, boundCursorOffset );
                    }
                    else
                    {
                        cursorDragging[0] = cursorHovering[0] = false;
                        ReleaseCapture();
                    }
                }
                else if (cursorDragging[1])
                {
                    if (plotInteractionAllowed)
                    {
                        int32_t newCursorOffset = mouseMovePoint.x - plotViewportLeftWindowXCoordinate;
                        // Bound to the viewport
                        int32_t boundCursorOffset = min( max( newCursorOffset, 0 ), plotViewportRightWindowXCoordinate - plotViewportLeftWindowXCoordinate );
                        if (newCursorOffset - boundCursorOffset < 15 * halfCursorWidthPxLarge)
                        {
                            fraPlotter->EnableCursor( 1, true );
                        }
                        else
                        {
                            fraPlotter->EnableCursor( 1, false );
                            cursorDragging[1] = false;
                            ReleaseCapture();
                        }
                        UpdateCursor( hWnd, 1, boundCursorOffset );
                    }
                    else
                    {
                        cursorDragging[1] = cursorHovering[1] = false;
                        ReleaseCapture();
                    }
                }
                else if (detectingZoom)
                {
                    if (plotInteractionAllowed)
                    {
                        POINT ptPossibleZoomEnd = mouseMovePoint;
                        // Clip it if it goes outside the plot area.
                        if (ptPossibleZoomEnd.x < pxPlotXStart)
                        {
                            ptPossibleZoomEnd.x = pxPlotXStart;
                        }
                        if (ptPossibleZoomEnd.x >= pxPlotXStart + pxPlotWidth)
                        {
                            ptPossibleZoomEnd.x = pxPlotXStart + pxPlotWidth - 1;
                        }
                        if (ptPossibleZoomEnd.y < pxPlotYStart)
                        {
                            ptPossibleZoomEnd.y = pxPlotYStart;
                        }
                        if (ptPossibleZoomEnd.y >= pxPlotYStart + pxPlotHeight)
                        {
                            ptPossibleZoomEnd.y = pxPlotYStart + pxPlotHeight - 1;
                        }

                        // Detect if the mouse moved enough to consider it a zoom operation
                        if (abs( ptPossibleZoomEnd.x - ptZoomBegin.x ) > 5 && abs( ptPossibleZoomEnd.y - ptZoomBegin.y ) > 5)
                        {
                            if (zooming)
                            {
                                // Erase the old rectangle
                                DrawZoomRectangle( hWnd, ptZoomBegin, ptZoomEnd );
                            }
                            else
                            {
                                zooming = true;
                            }

                            ptZoomEnd = ptPossibleZoomEnd;

                            // Draw the new rectangle
                            DrawZoomRectangle( hWnd, ptZoomBegin, ptZoomEnd );
                        }
                        else
                        {
                            if (zooming)
                            {
                                // Erase the last rectangle as we go out of zoom mode
                                DrawZoomRectangle( hWnd, ptZoomBegin, ptZoomEnd );
                                zooming = false;
                            }
                        }
                    }
                    else
                    {
                        detectingZoom = zooming = false;
                        ReleaseCapture();
                        SetCursor( LoadCursor( NULL, IDC_ARROW ) );
                    }
                }
                else // Check for hovering over cursor
                {
                    uint32_t cursor1Distance = (std::numeric_limits<uint32_t>::max)();
                    uint32_t cursor2Distance = (std::numeric_limits<uint32_t>::max)();

                    // Check to see if it's in the bounds of cursor 1 control box.
                    if (plotInteractionAllowed && mouseMovePoint.x >= plotViewportLeftWindowXCoordinate + cursorOffset[0] - halfCursorWidthPxLarge && mouseMovePoint.x <= plotViewportLeftWindowXCoordinate + cursorOffset[0] + halfCursorWidthPxLarge &&
                        ((mouseMovePoint.y >= plotViewportBottomWindowYCoordinate - halfCursorWidthPxLarge && mouseMovePoint.y <= plotViewportBottomWindowYCoordinate + halfCursorWidthPxLarge) ||
                            (mouseMovePoint.y >= plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge && mouseMovePoint.y <= plotViewportTopWindowYCoordinate + halfCursorWidthPxLarge)))
                    {
                        cursor1Distance = abs( mouseMovePoint.x - cursorOffset[0] );
                    }
                    else // It's not so, check to see if it just exited
                    {
                        if (cursorHovering[0])
                        {
                            cursorHovering[0] = false;
                            UpdateCursor( hWnd, 0, -1 );
                        }
                    }
                    // Check to see if it's in the bounds of cursor 2 control box.
                    if (plotInteractionAllowed && mouseMovePoint.x >= plotViewportLeftWindowXCoordinate + cursorOffset[1] - halfCursorWidthPxLarge && mouseMovePoint.x <= plotViewportLeftWindowXCoordinate + cursorOffset[1] + halfCursorWidthPxLarge &&
                        ((mouseMovePoint.y >= plotViewportBottomWindowYCoordinate - halfCursorWidthPxLarge && mouseMovePoint.y <= plotViewportBottomWindowYCoordinate + halfCursorWidthPxLarge) ||
                            (mouseMovePoint.y >= plotViewportTopWindowYCoordinate - halfCursorWidthPxLarge && mouseMovePoint.y <= plotViewportTopWindowYCoordinate + halfCursorWidthPxLarge)))
                    {
                        cursor2Distance = abs( mouseMovePoint.x - cursorOffset[1] );
                    }
                    else // It's not so, check to see if it just exited
                    {
                        if (cursorHovering[1])
                        {
                            cursorHovering[1] = false;
                            UpdateCursor( hWnd, 1, -1 );
                        }
                    }

                    if (cursor1Distance != (std::numeric_limits<uint32_t>::max)() ||
                        cursor2Distance != (std::numeric_limits<uint32_t>::max)())
                    {
                        if (cursor1Distance <= cursor2Distance && !cursorHovering[0])
                        {
                            cursorHovering[0] = true;
                            cursorHovering[1] = false;
                            UpdateCursor( hWnd, 0, -1 );
                        }
                        else if (cursor1Distance > cursor2Distance && !cursorHovering[1])
                        {
                            cursorHovering[1] = true;
                            cursorHovering[0] = false;
                            UpdateCursor( hWnd, 1, -1 );
                        }
                    }
                }
            }
            return 0;
            break;
        case WM_LBUTTONUP:
            {
                if (cursorDragging[0])
                {
                    cursorDragging[0] = false;
                    UpdateCursor( hWnd, 0, cursorOffset[0] );
                    ReleaseCapture();
                }
                if (cursorDragging[1])
                {
                    cursorDragging[1] = false;
                    UpdateCursor( hWnd, 1, cursorOffset[1] );
                    ReleaseCapture();
                }
                else if (detectingZoom)
                {
                    ReleaseCapture();
                    SetCursor(LoadCursor(NULL, IDC_ARROW));
                    detectingZoom = false;
                    zooming = false;

                    ptZoomEnd.x = GET_X_LPARAM(lParam);
                    ptZoomEnd.y = GET_Y_LPARAM(lParam);
                    // Clip it if it ends up outside the plot area.
                    if (ptZoomEnd.x < pxPlotXStart)
                    {
                        ptZoomEnd.x = pxPlotXStart;
                    }
                    if (ptZoomEnd.x >= pxPlotXStart+pxPlotWidth)
                    {
                        ptZoomEnd.x = pxPlotXStart+pxPlotWidth-1;
                    }
                    if (ptZoomEnd.y < pxPlotYStart)
                    {
                        ptZoomEnd.y = pxPlotYStart;
                    }
                    if (ptZoomEnd.y >= pxPlotYStart+pxPlotHeight)
                    {
                        ptZoomEnd.y = pxPlotYStart+pxPlotHeight-1;
                    }

                    if (abs(ptZoomEnd.x-ptZoomBegin.x) > 5 && abs(ptZoomEnd.y-ptZoomBegin.y) > 5)
                    {
                        // Consider it a zoom
                        if (fraPlotter->Zoom(make_tuple(pxPlotXStart, pxPlotYStart),
                                             make_tuple(pxPlotXStart+pxPlotWidth, pxPlotYStart+pxPlotHeight),
                                             make_tuple(ptZoomBegin.x, ptZoomBegin.y),
                                             make_tuple(ptZoomEnd.x, ptZoomEnd.y)))
                        {
                            RepaintPlot();
                        }
                        else
                        {
                            // Erase the rectangle
                            DrawZoomRectangle( hWnd, ptZoomBegin, ptZoomEnd );
                        }
                    }
                    else // No zoom, just a single click
                    {
                        POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
                        // Check to see if it was a click on the plot area.
                        if (pt.x >= pxPlotXStart && pt.x < pxPlotXStart+pxPlotWidth && pt.y >= pxPlotYStart && pt.y < pxPlotYStart+pxPlotHeight)
                        {
                            // Check if it's in the title area
                            if (pt.y < plotViewportTopWindowYCoordinate)
                            {
                                DWORD dwDlgResp;
                                dwDlgResp = DialogBoxParam( hInst, MAKEINTRESOURCE( IDD_PLOT_TITLE ), hWnd, PlotTitleDialogHandler, (LPARAM)pSettings );
                                if (LOWORD( dwDlgResp ) == IDOK)
                                {
                                    SetPlotTitle( true );
                                    RepaintPlot();
                                }
                            }
                            else // Otherwise consider it a request to change plot axes
                            {
                                PlotScaleSettings_T plotSettings;
                                DWORD dwDlgResp;
                                if (fraPlotter->PlotDataAvailable())
                                {
                                    fraPlotter->GetPlotScaleSettings(plotSettings);
                                }
                                else
                                {
                                    return FALSE;
                                }
                                dwDlgResp = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_AXES_DIALOG), hWnd, PlotAxesDialogHandler, (LPARAM)&plotSettings);
                                if (LOWORD(dwDlgResp) == IDOK)
                                {
                                    // Set the new settings and draw the plot
                                    try
                                    {
                                        fraPlotter->SetPlotAxes( plotSettings.freqAxisScale, plotSettings.gainAxisScale, plotSettings.phaseAxisScale,
                                                                 plotSettings.freqAxisIntervals, plotSettings.gainAxisIntervals, plotSettings.phaseAxisIntervals,
                                                                 plotSettings.gainMasterIntervals, plotSettings.phaseMasterIntervals, true, false );
                                    }
                                    catch (runtime_error e)
                                    {
                                        wstringstream wss;
                                        wss << e.what();
                                        LogMessage( wss.str() );
                                        return 0;
                                    }
                                    RepaintPlot();
                                }
                            }
                            return TRUE;
                        }
                        else
                        {
                            return FALSE;
                        }
                    }
                }
            }
            return 0; 
            break;
        case WM_RBUTTONUP:
            {
                ptZoomBegin.x = GET_X_LPARAM(lParam);
                ptZoomBegin.y = GET_Y_LPARAM(lParam);
                if (plotInteractionAllowed && ptZoomBegin.x >= pxPlotXStart && ptZoomBegin.x < pxPlotXStart+pxPlotWidth && ptZoomBegin.y >= pxPlotYStart && ptZoomBegin.y < pxPlotYStart+pxPlotHeight &&
                    fraPlotter->UndoOneZoom())
                {
                    RepaintPlot();
                }
            }
            return 0;
            break;
        case WM_RBUTTONDBLCLK:
            {
                ptZoomBegin.x = GET_X_LPARAM(lParam);
                ptZoomBegin.y = GET_Y_LPARAM(lParam);
                if (plotInteractionAllowed && ptZoomBegin.x >= pxPlotXStart && ptZoomBegin.x < pxPlotXStart+pxPlotWidth && ptZoomBegin.y >= pxPlotYStart && ptZoomBegin.y < pxPlotYStart+pxPlotHeight &&
                    fraPlotter->ZoomToOriginal())
                {
                    RepaintPlot();
                }
            }
            return 0;
            break;
        case WM_HSCROLL:
            {
                wmId = GetDlgCtrlID((HWND)lParam);
                wmEvent = LOWORD(wParam);
                bool updatePlot = false;
                bool updateCursor = false;
                switch (wmId)
                {
                    case IDC_NEG3DB_SCROLLBAR:
                        {
                            if (SB_LINELEFT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( NEG3DB_MEAS, LEFT );
                            }
                            else if (SB_LINERIGHT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( NEG3DB_MEAS, RIGHT );
                            }
                        }
                        break;
                    case IDC_GAIN_MARGIN_SCROLLBAR:
                        {
                            if (SB_LINELEFT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( GAIN_MARGIN_MEAS, LEFT );
                            }
                            else if (SB_LINERIGHT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( GAIN_MARGIN_MEAS, RIGHT );
                            }
                        }
                        break;
                    case IDC_PHASE_MARGIN_SCROLLBAR:
                        {
                            if (SB_LINELEFT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( PHASE_MARGIN_MEAS, LEFT );
                            }
                            else if (SB_LINERIGHT == wmEvent)
                            {
                                updatePlot = fraPlotter->MoveMeasurementIndex( PHASE_MARGIN_MEAS, RIGHT );
                            }
                        }
                        break;
                    case IDC_CURSOR1_SCROLLBAR:
                        {
                            if (plotInteractionAllowed)
                            {
                                if (SB_LINELEFT == wmEvent)
                                {
                                    updateCursor = fraPlotter->NudgeCursor( 0, LEFT );
                                }
                                else if (SB_LINERIGHT == wmEvent)
                                {
                                    updateCursor = fraPlotter->NudgeCursor( 0, RIGHT );
                                }
                                if (updateCursor)
                                {
                                    UpdateCursor( hWnd, 0, -1 );
                                }
                            }
                        }
                        break;
                    case IDC_CURSOR2_SCROLLBAR:
                        {
                            if (plotInteractionAllowed)
                            {
                                if (SB_LINELEFT == wmEvent)
                                {
                                    updateCursor = fraPlotter->NudgeCursor( 1, LEFT );
                                }
                                else if (SB_LINERIGHT == wmEvent)
                                {
                                    updateCursor = fraPlotter->NudgeCursor( 1, RIGHT );
                                }
                                if (updateCursor)
                                {
                                    UpdateCursor( hWnd, 1, -1 );
                                }
                            }
                        }
                        break;
                }
                if (updatePlot)
                {
                    if (fraPlotter->PlotDataAvailable())
                    {
                        (void)GenerateFullPlot(false);
                        RepaintPlot();
                    }
                }
                return 0;
            }
            break;
        case WM_PAINT:
            BITMAP bm;
            hdc = BeginPaint(hWnd, &ps);

            hdcMem = CreateCompatibleDC(hdc);

            hbmOld = (HBITMAP)SelectObject(hdcMem, hPlotBM);
            GetObject(hPlotBM, sizeof(bm), &bm);
            BitBlt(hdc, pxPlotXStart, pxPlotYStart, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

            SelectObject(hdcMem, hPlotSeparatorBM);
            GetObject(hPlotSeparatorBM, sizeof(bm), &bm);
            BitBlt(hdc, pxPlotSeparatorXStart, pxPlotYStart, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

            SelectObject(hdcMem, hbmOld);
            DeleteDC(hdcMem);

            if (detectingZoom)
            {
                if (abs( ptZoomEnd.x - ptZoomBegin.x ) > 5 || abs( ptZoomEnd.y - ptZoomBegin.y ) > 5)
                {
                    SetROP2( hdc, R2_NOT );
                    SelectObject( hdc, GetStockObject( NULL_BRUSH ) );
                    Rectangle( hdc, ptZoomBegin.x, ptZoomBegin.y, ptZoomEnd.x, ptZoomEnd.y );
                }
            }

            if (plotOnScreen)
            {
                DrawCursor( hMainWnd, 0 );
                DrawCursor( hMainWnd, 1 );
            }

            EndPaint(hWnd, &ps);
            return TRUE;
            break;
        case WM_DESTROY:
            DeleteObject(hPlotSeparatorBM);
            DeleteObject(hPlotBM);
            DeleteObject(hFraPlotForPageSetupBM);
            PostQuitMessage(0);
            return TRUE;
        case WM_CLOSE:
            if (WaitForSingleObject( hExecuteFraEvent, 0 ) != WAIT_TIMEOUT) // If FRA is running
            {
                DWORD dwDlgResp = MessageBox( hWnd, L"FRA is running.  Are you sure you want to exit?", L"Warning", MB_YESNO | MB_ICONWARNING | MB_APPLMODAL );

                if (dwDlgResp == IDYES)
                {
                    psFRA->CancelFRA();
                    PostMessage( hWnd, WM_EXIT_DURING_FRA, 0, 0 );
                }
            }
            else
            {
                Cleanup( hWnd );
            }
            return TRUE;
        case WM_EXIT_DURING_FRA:
            if (wParam < 50) // 50 * 100 ms = 5 seconds
            {
                if (WaitForSingleObject( hExecuteFraEvent, 0 ) == WAIT_TIMEOUT) // If FRA is not running
                {
                    Cleanup( hWnd );
                }
                else
                {
                    Sleep( 100 );
                    PostMessage( hWnd, WM_EXIT_DURING_FRA, wParam + 1, 0 );
                }
            }
            else
            {
                // We've waited 5 seconds, now forcibly terminate the thread and cleanup
                TerminateThread( hExecuteFraThread, 0 );
                Cleanup( hWnd );
            }
            return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: Cleanup
//
// Purpose: Handle resource cleanup as a common method to be called from various paths of program
//          exit.
//
// Parameters: hWnd: Handle to the main window.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void Cleanup( HWND hWnd )
{
    static bool cleaningUp = false;

    if (!cleaningUp)
    {
        cleaningUp = true;

        if (pScopeSelector->GetSelectedScope())
        {
            StoreSettings();
            pSettings->WriteScopeSettings();
        }
        if (SIG_GEN_EXTERNAL == pSettings->GetSignalGeneratorSource())
        {
            StoreSettings();
            pSettings->WriteEsgSettings();
        }
        pSettings->WriteApplicationSettings();

        delete psFRA;
        delete fraPlotter;
        delete pScopeSelector;
        delete pExtSigGenPlugin;
        delete pSettings;
        delete pMessageLoggingProcessor;
        Gdiplus::GdiplusShutdown( gdiplusToken );
        GlobalFree( hDevMode );
        GlobalLock( hDevNames );
        DestroyWindow( hWnd );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SmartProbeCallback
//
// Purpose: Handles smart probe events
//
// Parameters: [in] probeEvents - the events to handle
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SmartProbeCallback(std::vector<SmartProbeEvent_T> probeEvents)
{
    HWND hndInputChannelCB;
    HWND hndOutputChannelCB;
    HWND hndInputChannelAttenCB;
    HWND hndOutputChannelAttenCB;
    HWND hndInputChannelCouplingCB;
    HWND hndOutputChannelCouplingCB;
    PS_CHANNEL inputChannel;
    PS_CHANNEL outputChannel;

    WaitForSingleObject( hApplicationObjectsInitialized, INFINITE );

    hndInputChannelCB = GetDlgItem( hMainWnd, IDC_INPUT_CHANNEL );
    inputChannel = (PS_CHANNEL)ComboBox_GetCurSel(hndInputChannelCB);
    hndInputChannelAttenCB = GetDlgItem( hMainWnd, IDC_INPUT_ATTEN );

    hndOutputChannelCB = GetDlgItem( hMainWnd, IDC_OUTPUT_CHANNEL );
    outputChannel = (PS_CHANNEL)ComboBox_GetCurSel(hndOutputChannelCB);
    hndOutputChannelAttenCB = GetDlgItem( hMainWnd, IDC_OUTPUT_ATTEN );

    for each (auto probeEvent in probeEvents)
    {
        if (probeEvent.smartProbeConnected && probeEvent.autoAtten)
        {
            if (probeEvent.channel == inputChannel)
            {
                ComboBox_SetCurSel(hndInputChannelAttenCB, probeEvent.atten);
                EnableWindow(hndInputChannelAttenCB, FALSE);
            }
            else if (probeEvent.channel == outputChannel)
            {
                ComboBox_SetCurSel(hndOutputChannelAttenCB, probeEvent.atten);
                EnableWindow(hndOutputChannelAttenCB, FALSE);
            }
        }
        else
        {
            if (probeEvent.channel == inputChannel)
            {
                ComboBox_SetCurSel(hndInputChannelAttenCB, probeEvent.atten);
                EnableWindow(hndInputChannelAttenCB, TRUE);
            }
            else if (probeEvent.channel == outputChannel)
            {
                ComboBox_SetCurSel(hndOutputChannelAttenCB, probeEvent.atten);
                EnableWindow(hndOutputChannelAttenCB, TRUE);
            }
        }

        if (probeEvent.availableCouplings.size())
        {
            uint32_t newCoupling;
            if (probeEvent.channel == inputChannel)
            {
                hndInputChannelCouplingCB = GetDlgItem( hMainWnd, IDC_INPUT_COUPLING );
                newCoupling = min( ComboBox_GetCurSel( hndInputChannelCouplingCB ), probeEvent.availableCouplings.size() - 1 );
                ComboBox_ResetContent( hndInputChannelCouplingCB );
                for (uint8_t k = 0; k < probeEvent.availableCouplings.size(); k++)
                {
                    // Add string to combobox.
                    ComboBox_AddString( hndInputChannelCouplingCB, probeEvent.availableCouplings[k].c_str() );
                }
                ComboBox_SetCurSel( hndInputChannelCouplingCB, newCoupling );
            }
            else if (probeEvent.channel == outputChannel)
            {
                hndOutputChannelCouplingCB = GetDlgItem( hMainWnd, IDC_OUTPUT_COUPLING );
                newCoupling = min( ComboBox_GetCurSel( hndOutputChannelCouplingCB ), probeEvent.availableCouplings.size() - 1 );
                ComboBox_ResetContent( hndOutputChannelCouplingCB );
                for (uint8_t k = 0; k < probeEvent.availableCouplings.size(); k++)
                {
                    // Add string to combobox.
                    ComboBox_AddString( hndOutputChannelCouplingCB, probeEvent.availableCouplings[k].c_str() );
                }
                ComboBox_SetCurSel( hndOutputChannelCouplingCB, newCoupling );
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: LoadDynamicChannelControls
//
// Purpose: Configures channel controls (atten, coupling, offset) based on smart probe settings
//
// Parameters: [in] hDlg - handle to the dialog containing the controls
//
// Notes: Currently only  handles attenuation and coupling because it's not known if any smart
//        probes can set offset.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void LoadDynamicChannelControls(HWND hDlg)
{
    PicoScope* pScope;

    if (NULL != (pScope = pScopeSelector->GetSelectedScope()))
    {
        HWND hndInputChannelCB;
        HWND hndOutputChannelCB;
        HWND hndInputChannelAttenCB;
        HWND hndOutputChannelAttenCB;
        PS_CHANNEL inputChannel;
        PS_CHANNEL outputChannel;
        std::vector<std::wstring> inputCouplingText;
        std::vector<std::wstring> outputCouplingText;
        HWND hndCB;
        uint32_t newCoupling;

        hndInputChannelCB = GetDlgItem( hMainWnd, IDC_INPUT_CHANNEL );
        inputChannel = (PS_CHANNEL)ComboBox_GetCurSel(hndInputChannelCB);
        hndInputChannelAttenCB = GetDlgItem( hMainWnd, IDC_INPUT_ATTEN );

        hndOutputChannelCB = GetDlgItem( hMainWnd, IDC_OUTPUT_CHANNEL );
        outputChannel = (PS_CHANNEL)ComboBox_GetCurSel(hndOutputChannelCB);
        hndOutputChannelAttenCB = GetDlgItem( hMainWnd, IDC_OUTPUT_ATTEN );

        ATTEN_T atten;
        if (pScope->GetChannelAttenuation((PS_CHANNEL)inputChannel, atten))
        {
            ComboBox_SetCurSel(hndInputChannelAttenCB, atten);
            if (pScope->IsAutoAttenuation((PS_CHANNEL)inputChannel))
            {
                EnableWindow(hndInputChannelAttenCB, FALSE);
            }
            else
            {
                EnableWindow(hndInputChannelAttenCB, TRUE);
            }
        }
        if (pScope->GetChannelAttenuation((PS_CHANNEL)outputChannel, atten))
        {
            ComboBox_SetCurSel(hndOutputChannelAttenCB, atten);
            if (pScope->IsAutoAttenuation((PS_CHANNEL)outputChannel))
            {
                EnableWindow(hndOutputChannelAttenCB, FALSE);
            }
            else
            {
                EnableWindow(hndOutputChannelAttenCB, TRUE);
            }
        }

        pScope->GetAvailableCouplings(inputChannel, inputCouplingText);
        pScope->GetAvailableCouplings(outputChannel, outputCouplingText);

        hndCB = GetDlgItem( hDlg, IDC_INPUT_COUPLING );
        newCoupling = min( ComboBox_GetCurSel( hndCB ), inputCouplingText.size() - 1 );
        ComboBox_ResetContent( hndCB );
        for (uint8_t k = 0; k < inputCouplingText.size(); k++)
        {
            // Add string to combobox.
            ComboBox_AddString( hndCB, inputCouplingText[k].c_str() );
        }
        ComboBox_SetCurSel( hndCB, newCoupling );

        hndCB = GetDlgItem( hDlg, IDC_OUTPUT_COUPLING );
        newCoupling = min( ComboBox_GetCurSel( hndCB ), outputCouplingText.size() - 1 );
        ComboBox_ResetContent( hndCB );
        for (uint8_t k = 0; k < outputCouplingText.size(); k++)
        {
            // Add string to combobox.
            ComboBox_AddString( hndCB, outputCouplingText[k].c_str() );
        }
        ComboBox_SetCurSel( hndCB, newCoupling );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: LoadControlsData
//
// Purpose: Load data into the controls, specific to the application and scope (gotten from the global
//          setting object)
//
// Parameters: hDlg: handle to the main window (which is a dialog)
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

BOOL LoadControlsData(HWND hDlg)
{
    PicoScope* pScope;

    std::vector<std::wstring> inputCouplingText;
    std::vector<std::wstring> outputCouplingText;
    uint32_t newCoupling;

    if (NULL != (pScope = pScopeSelector->GetSelectedScope()))
    {
        pScope->GetAvailableCouplings((PS_CHANNEL)pSettings->GetInputChannel(), inputCouplingText);
        pScope->GetAvailableCouplings((PS_CHANNEL)pSettings->GetOutputChannel(), outputCouplingText);
    }
    else
    {
        inputCouplingText.resize(2);
        inputCouplingText[0] = TEXT("AC");
        inputCouplingText[1] = TEXT("DC");
        outputCouplingText = inputCouplingText;
    }

    wstring chanText;

    HWND hndCB;
    HWND hndCtrl;

    // Load the Combo-boxes with entries.
    hndCB = GetDlgItem( hDlg, IDC_INPUT_CHANNEL );
    ComboBox_ResetContent( hndCB );
    for (int k = 0; k < pSettings->GetNumChannels(); k++)
    {
        chanText = L'A' + (wchar_t)k;
        // Add string to combobox.
        ComboBox_AddString( hndCB, chanText.c_str() );
        chanText[0]++;
    }
    SendMessage(hndCB, CB_SETCURSEL, (WPARAM)(pSettings->GetInputChannel()), (LPARAM)0);

    hndCB = GetDlgItem( hDlg, IDC_OUTPUT_CHANNEL );
    ComboBox_ResetContent( hndCB );
    for (int k = 0; k < pSettings->GetNumChannels(); k++)
    {
        chanText = L'A' + (wchar_t)k;
        // Add string to combobox.
        ComboBox_AddString( hndCB, chanText.c_str() );
        chanText[0]++;
    }
    SendMessage(hndCB, CB_SETCURSEL, (WPARAM)(pSettings->GetOutputChannel()), (LPARAM)0);

    hndCB = GetDlgItem( hDlg, IDC_INPUT_ATTEN );
    ComboBox_ResetContent( hndCB );
    for (int k = 0; k < numAttens; k++)
    {
        // Add string to combobox.
        ComboBox_AddString( hndCB, attenText[k] );
    }
    SendMessage(hndCB, CB_SETCURSEL, (WPARAM)(pSettings->GetInputAttenuation()), (LPARAM)0);
    PS_CHANNEL inputChannel = (PS_CHANNEL)pSettings->GetInputChannel();
    if (pScope && inputChannel < pScope->GetNumChannels() && inputChannel != PS_CHANNEL_INVALID)
    {
        if (pScope->IsAutoAttenuation( (PS_CHANNEL)pSettings->GetInputChannel() ))
        {
            EnableWindow( hndCB, FALSE );
        }
        else
        {
            EnableWindow( hndCB, TRUE );
        }
    }
    else // Changing attenuation settings with an invalid channel will result in errors
    {
        EnableWindow( hndCB, FALSE );
    }

    hndCB = GetDlgItem( hDlg, IDC_OUTPUT_ATTEN );
    ComboBox_ResetContent( hndCB );
    for (int k = 0; k < numAttens; k++)
    {
        // Add string to combobox.
        ComboBox_AddString( hndCB, attenText[k] );
    }
    SendMessage(hndCB, CB_SETCURSEL, (WPARAM)(pSettings->GetOutputAttenuation()), (LPARAM)0);
    PS_CHANNEL outputChannel = (PS_CHANNEL)pSettings->GetOutputChannel();
    if (pScope && outputChannel < pScope->GetNumChannels() && outputChannel != PS_CHANNEL_INVALID)
    {
        if (pScope->IsAutoAttenuation( (PS_CHANNEL)pSettings->GetOutputChannel() ))
        {
            EnableWindow( hndCB, FALSE );
        }
        else
        {
            EnableWindow( hndCB, TRUE );
        }
    }
    else // Changing attenuation settings with an invalid channel will result in errors
    {
        EnableWindow( hndCB, FALSE );
    }

    hndCB = GetDlgItem( hDlg, IDC_INPUT_COUPLING );
    ComboBox_ResetContent( hndCB );
    for (uint8_t k = 0; k < inputCouplingText.size(); k++)
    {
        // Add string to combobox.
        ComboBox_AddString( hndCB, inputCouplingText[k].c_str() );
    }
    newCoupling = min( (uint32_t)(pSettings->GetInputCoupling()), inputCouplingText.size() - 1 );
    SendMessage(hndCB, CB_SETCURSEL, (WPARAM)(newCoupling), (LPARAM)0);

    hndCB = GetDlgItem( hDlg, IDC_OUTPUT_COUPLING );
    ComboBox_ResetContent( hndCB );
    for (uint8_t k = 0; k < outputCouplingText.size(); k++)
    {
        // Add string to combobox.
        ComboBox_AddString( hndCB, outputCouplingText[k].c_str() );
    }
    newCoupling = min( (uint32_t)(pSettings->GetOutputCoupling()), outputCouplingText.size() - 1 );
    SendMessage( hndCB, CB_SETCURSEL, (WPARAM)(newCoupling), (LPARAM)0 );

    // Disable offset fields if the scope does not support DC offset
    if (pScope && !pScope->SupportsChannelDcOffset())
    {
        hndCtrl = GetDlgItem( hDlg, IDC_INPUT_DC_OFFS );
        Edit_SetText( hndCtrl, L"0.0" );
        EnableWindow( hndCtrl, FALSE );

        hndCtrl = GetDlgItem( hDlg, IDC_OUTPUT_DC_OFFS );
        Edit_SetText( hndCtrl, L"0.0" );
        EnableWindow( hndCtrl, FALSE );
    }
    else
    {
        hndCtrl = GetDlgItem( hDlg, IDC_INPUT_DC_OFFS );
        EnableWindow( hndCtrl, TRUE );
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetInputDcOffsetAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_OUTPUT_DC_OFFS );
        EnableWindow( hndCtrl, TRUE );
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetOutputDcOffsetAsString().c_str() );
    }

    /////////////////////////////////////
    //// Fields for single segment FRA //
    /////////////////////////////////////
    if (pSettings->GetCustomPlanEnabled()) // In custom plan mode
    {
        hndCtrl = GetDlgItem( hDlg, IDC_STIMULUS_VPP );
        EnableWindow(hndCtrl, FALSE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStimulusVppAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STIMULUS_VPP_UNITS );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_STIMULUS_OFFSET );
        if (psFRA->SigGenSupportsDcOffset())
        {
            Edit_LimitText(hndCtrl, 16);
            Edit_SetText(hndCtrl, pSettings->GetStimulusOffsetAsString().c_str());
        }
        else
        {
            Edit_SetText(hndCtrl, L"");
        }
        EnableWindow(hndCtrl, FALSE);

        // Always populate these, otherwise store on run/exit won't work as designed
        hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
        Edit_SetText( hndCtrl, pSettings->GetTargetResponseAmplitudeAsString().c_str() );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
        Edit_SetText( hndCtrl, pSettings->GetMaxStimulusVppAsString().c_str() );
        EnableWindow(hndCtrl, FALSE);

        if (pSettings->GetAdaptiveStimulusMode())
        {
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_TITLE);
            Static_SetText(hndCtrl, L"Initial Stimulus");
            EnableWindow(hndCtrl, FALSE);

            hndCtrl = GetDlgItem(hDlg, IDC_STIMULUS_OFFSET);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );

            hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            Edit_LimitText( hndCtrl, 16 );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_TITLE);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_UNITS);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );

            hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            Edit_LimitText( hndCtrl, 16 );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_TITLE);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_UNITS);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
        }
        else
        {
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_TITLE);
            Static_SetText(hndCtrl, L"Stimulus Amplitude");
            EnableWindow(hndCtrl, FALSE);

            hndCtrl = GetDlgItem(hDlg, IDC_STIMULUS_OFFSET);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            EnableWindow(hndCtrl, FALSE);
            ShowWindow( hndCtrl, SW_SHOW );

            hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );

            hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );
        }

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_START_FREQ );
        EnableWindow(hndCtrl, FALSE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStartFreqAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_START_FREQ_TITLE );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_START_FREQ_UNITS );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_STOP_FREQ );
        EnableWindow(hndCtrl, FALSE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStopFreqAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STOP_FREQ_TITLE );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STOP_FREQ_UNITS );
        EnableWindow(hndCtrl, FALSE);

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_STEPS_PER_DECADE );
        EnableWindow(hndCtrl, FALSE);
        Edit_LimitText( hndCtrl, 5 );
        Edit_SetText( hndCtrl, pSettings->GetStepsPerDecadeAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STEPS_PER_DECADE_TITLE );
        EnableWindow(hndCtrl, FALSE);
    }
    else // Not in Custom Plan mode
    {
        hndCtrl = GetDlgItem( hDlg, IDC_STIMULUS_VPP );
        EnableWindow(hndCtrl, TRUE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStimulusVppAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STIMULUS_VPP_UNITS );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_STIMULUS_OFFSET );
        if (psFRA->SigGenSupportsDcOffset())
        {
            Edit_Enable(hndCtrl, TRUE);
            Edit_LimitText(hndCtrl, 16);
            Edit_SetText(hndCtrl, pSettings->GetStimulusOffsetAsString().c_str());
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            EnableWindow(hndCtrl, TRUE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            EnableWindow(hndCtrl, TRUE);
        }
        else
        {
            Edit_SetText(hndCtrl, L"");
            Edit_Enable(hndCtrl, FALSE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            EnableWindow(hndCtrl, FALSE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            EnableWindow(hndCtrl, FALSE);
        }

        // Always populate these, otherwise store on run/exit won't work as designed
        hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
        Edit_SetText( hndCtrl, pSettings->GetTargetResponseAmplitudeAsString().c_str() );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
        Edit_SetText( hndCtrl, pSettings->GetMaxStimulusVppAsString().c_str() );
        EnableWindow(hndCtrl, TRUE);

        if (pSettings->GetAdaptiveStimulusMode())
        {
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_TITLE);
            Static_SetText(hndCtrl, L"Initial Stimulus");
            EnableWindow(hndCtrl, TRUE);

            hndCtrl = GetDlgItem(hDlg, IDC_STIMULUS_OFFSET);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );

            hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_TITLE);
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_UNITS);
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);

            hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_TITLE);
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_UNITS);
            ShowWindow( hndCtrl, SW_SHOW );
            EnableWindow(hndCtrl, TRUE);
        }
        else
        {
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_TITLE);
            Static_SetText(hndCtrl, L"Stimulus Amplitude");
            EnableWindow(hndCtrl, TRUE);

            hndCtrl = GetDlgItem(hDlg, IDC_STIMULUS_OFFSET);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_TITLE);
            ShowWindow( hndCtrl, SW_SHOW );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_STIMULUS_OFFSET_UNITS);
            ShowWindow( hndCtrl, SW_SHOW );

            hndCtrl = GetDlgItem(hDlg, IDC_RESPONSE_TARGET);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_RESPONSE_TARGET_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );

            hndCtrl = GetDlgItem( hDlg, IDC_MAX_STIMULUS_VPP );
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_TITLE);
            ShowWindow( hndCtrl, SW_HIDE );
            hndCtrl = GetDlgItem(hDlg, IDC_TEXT_MAX_STIMULUS_VPP_UNITS);
            ShowWindow( hndCtrl, SW_HIDE );
        }

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_START_FREQ );
        EnableWindow(hndCtrl, TRUE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStartFreqAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_START_FREQ_TITLE );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_START_FREQ_UNITS );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_STOP_FREQ );
        EnableWindow(hndCtrl, TRUE);
        Edit_LimitText( hndCtrl, 16 );
        Edit_SetText( hndCtrl, pSettings->GetStopFreqAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STOP_FREQ_TITLE );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STOP_FREQ_UNITS );
        EnableWindow(hndCtrl, TRUE);

        hndCtrl = GetDlgItem( hDlg, IDC_FRA_STEPS_PER_DECADE );
        EnableWindow(hndCtrl, TRUE);
        Edit_LimitText( hndCtrl, 5 );
        Edit_SetText( hndCtrl, pSettings->GetStepsPerDecadeAsString().c_str() );

        hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STEPS_PER_DECADE_TITLE );
        EnableWindow(hndCtrl, TRUE);
    }

    hndCtrl = GetDlgItem( hDlg, IDC_CUSTOM_CHECK );
    Button_SetCheck( hndCtrl, pSettings->GetCustomPlanEnabled() );

    EnablePlotControls( hDlg );

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: DisablePlotControls
//
// Purpose: Disables all the plot controls
//
// Parameters: hDlg: handle to the main window (which is a dialog)
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void DisablePlotControls(HWND hDlg)
{
    HWND hndCtrl;

    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_AUTO_AXES_TITLE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_AUTO_AXES );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STORED_AXES_TITLE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_STORED_AXES );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_GAIN_TITLE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GAIN );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_PHASE_TITLE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PHASE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_NEG3DB );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_NEG3DB_TITLE);
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_UNWRAP_PHASE_TITLE );
    EnableWindow(hndCtrl, FALSE);
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_UNWRAP_PHASE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_GM_TITLE );
    EnableWindow( hndCtrl, FALSE  );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GM );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_PM_TITLE );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PM );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_NEG3DB_SCROLLBAR );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_GAIN_MARGIN_SCROLLBAR );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_PHASE_MARGIN_SCROLLBAR );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_CURSOR1_SCROLLBAR );
    EnableWindow( hndCtrl, FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_CURSOR2_SCROLLBAR );
    EnableWindow( hndCtrl, FALSE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: EnablePlotControls
//
// Purpose: Enables all the plot controls
//
// Parameters: hDlg: handle to the main window (which is a dialog)
//
// Notes: Also serves as a shared function to re-evaluate plot control settings
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void EnablePlotControls(HWND hDlg)
{
    HWND hndCtrl;

    // Settings

    if (pSettings->GetAutoAxes())
    {
        hndCtrl = GetDlgItem( hDlg, IDC_FRA_AUTO_AXES );
    }
    else
    {
        hndCtrl = GetDlgItem( hDlg, IDC_FRA_STORED_AXES );
    }
    Button_SetCheck( hndCtrl, TRUE );

    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GAIN );
    Button_SetCheck( hndCtrl, pSettings->GetPlotGain() ? BST_CHECKED : BST_UNCHECKED );

    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PHASE );
    Button_SetCheck( hndCtrl, pSettings->GetPlotPhase() ? BST_CHECKED : BST_UNCHECKED );

    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_NEG3DB );
    Button_SetCheck( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotNeg3dB()) ? BST_CHECKED : BST_UNCHECKED );

    hndCtrl = GetDlgItem( hDlg, IDC_FRA_UNWRAP_PHASE );
    Button_SetCheck( hndCtrl, (pSettings->GetPlotPhase() && pSettings->GetPlotUnwrappedPhase()) ? BST_CHECKED : BST_UNCHECKED );

    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GM );
    Button_SetCheck( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase() && pSettings->GetPlotGainMargin()) ? BST_CHECKED : BST_UNCHECKED );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PM );
    Button_SetCheck( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase() && pSettings->GetPlotPhaseMargin()) ? BST_CHECKED : BST_UNCHECKED );

    // Enable/Disable

    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_AUTO_AXES_TITLE );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_AUTO_AXES );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_STORED_AXES_TITLE );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_STORED_AXES );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_GAIN_TITLE );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GAIN );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_PHASE_TITLE );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PHASE );
    EnableWindow( hndCtrl, TRUE );

    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_UNWRAP_PHASE_TITLE );
    EnableWindow( hndCtrl, pSettings->GetPlotPhase() ? TRUE : FALSE);
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_UNWRAP_PHASE );
    Button_Enable(hndCtrl, pSettings->GetPlotPhase() ? TRUE : FALSE);
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_NEG3DB_TITLE );
    EnableWindow( hndCtrl, pSettings->GetPlotGain() ? TRUE : FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_NEG3DB );
    Button_Enable( hndCtrl, pSettings->GetPlotGain() ? TRUE : FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_GM_TITLE );
    EnableWindow( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase()) ? TRUE : FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_GM );
    Button_Enable( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase()) ? TRUE : FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_TEXT_PLOT_PM_TITLE );
    EnableWindow( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase()) ? TRUE : FALSE );
    hndCtrl = GetDlgItem( hDlg, IDC_FRA_PLOT_PM );
    Button_Enable( hndCtrl, (pSettings->GetPlotGain() && pSettings->GetPlotPhase() && !pSettings->GetPlotUnwrappedPhase()) ? TRUE : FALSE );

    hndCtrl = GetDlgItem( hDlg, IDC_NEG3DB_SCROLLBAR );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_GAIN_MARGIN_SCROLLBAR );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_PHASE_MARGIN_SCROLLBAR );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_CURSOR1_SCROLLBAR );
    EnableWindow( hndCtrl, TRUE );
    hndCtrl = GetDlgItem( hDlg, IDC_CURSOR2_SCROLLBAR );
    EnableWindow( hndCtrl, TRUE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: InitializeControls
//
// Purpose: Initialize main window controls with non-settings specific data.
//
// Parameters: hDlg: handle to the main window (which is a dialog)
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

BOOL InitializeControls(HWND hDlg)
{
    HWND hndCtrl;

    // Size the log
    hndCtrl = GetDlgItem( hDlg, IDC_LOG );
    SendMessage( hndCtrl, EM_SETLIMITTEXT, 8388608, 0); // 8M

    // Give the log text box a distinguished gray background and a thin frame (SS_GRAYFRAME)
    SendDlgItemMessage( hDlg, IDC_LOG, EM_SETBKGNDCOLOR, 0, RGB(230,230,230));

    RECT statRect;
    POINT tl, br;
    GetWindowRect(GetDlgItem(hDlg, IDC_LOG), &statRect);
    InflateRect(&statRect, 1, 1);
    tl.x = statRect.left;
    tl.y = statRect.top;
    br.x = statRect.right;
    br.y = statRect.bottom;
    ScreenToClient(hDlg, &tl);
    ScreenToClient(hDlg, &br);

    HWND hStatic = CreateWindow( WC_STATIC, NULL, SS_GRAYFRAME | WS_CHILD | WS_VISIBLE, tl.x, tl.y, br.x - tl.x, br.y - tl.y, hDlg, (HMENU)IDC_STATIC, hInst, 0 );

    //// Determine where the the plot and separator will go.

    // Get margin pixels from DLUs
    RECT marginRect = {0, 0, 6, 12}; // Use right for small margin (6), and bottom for large margin (12)
    MapDialogRect(hDlg, &marginRect);

    RECT largeCursorWidthRect = { 0, 0, 0, 6 };
    MapDialogRect( hDlg, &largeCursorWidthRect );
    halfCursorWidthPxLarge = largeCursorWidthRect.bottom / 2;

    RECT smallCursorWidthRect = { 0, 0, 0, 4 };
    MapDialogRect( hDlg, &smallCursorWidthRect );
    halfCursorWidthPxSmall = smallCursorWidthRect.bottom / 2;

    RECT intersectionMarkerWidthRect = { 0, 0, 0, 5 };
    MapDialogRect( hDlg, &intersectionMarkerWidthRect );
    halfIntersectionMarkerWidthPx = intersectionMarkerWidthRect.bottom / 2;

    // Find the rightmost edge of the controls
    hndCtrl = GetDlgItem( hDlg, IDC_RIGHTMOST_CTL );
    RECT rightmostCtlRect;
    GetWindowRect( hndCtrl, &rightmostCtlRect );
    POINT rightmostCtlPoint = {rightmostCtlRect.right, rightmostCtlRect.bottom};
    ScreenToClient( hDlg, &rightmostCtlPoint);
    pxPlotXStart = rightmostCtlPoint.x + marginRect.bottom;

    pxPlotYStart = marginRect.right;

    pxPlotSeparatorXStart = rightmostCtlPoint.x + (marginRect.bottom / 2);

    // Find the top of the measurement controls
    hndCtrl = GetDlgItem(hDlg, IDC_NEG3DB_SCROLLBAR);
    RECT measCtlRect;
    GetWindowRect(hndCtrl, &measCtlRect);
    POINT measCtlPoint = { measCtlRect.right, measCtlRect.top };
    ScreenToClient(hDlg, &measCtlPoint);

    // Find the size of the dialog box on this system
    RECT dlgClientRect;
    GetClientRect( hDlg, &dlgClientRect );

    pxPlotWidth = dlgClientRect.right - pxPlotXStart - marginRect.right;
    pxPlotSeparatorHeight = dlgClientRect.bottom - pxPlotYStart - marginRect.right;
    pxPlotHeight = measCtlPoint.y - pxPlotYStart - marginRect.right;

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ExecuteFRA
//
// Purpose: Carries out execution of the FRA.  
//
// Parameters:
//
// Notes: Is a thread function, which supports cancellation.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI ExecuteFRA(LPVOID lpdwThreadParam)
{
    int inputChannel = 0;
    int inputChannelCoupling = 0;
    int inputChannelAttenuation = 0;
    double inputDcOffset = 0.0;
    int outputChannel = 0;
    int outputChannelCoupling = 0;
    int outputChannelAttenuation = 0;
    double outputDcOffset = 0.0;
    double stimulusVpp = 0.0;
    double stimulusOffset = 0.0;
    double targetAmplitudeResponse = 0.0;
    double maxStimulusVpp = 0.0;
    double startFreq = 0;
    double stopFreq = 0;
    int32_t stepsPerDecade = 0;

    DWORD dwWaitResult;

    WaitForSingleObject( hApplicationObjectsInitialized, INFINITE );

    for (;;)
    {
        // Place these here because this is where we'll always return whether the FRA executes completely or not.
        if (fraPlotter->PlotDataAvailable())
        {
            plotInteractionAllowed = true;
            if (!pSettings->GetPlotRealTime())
            {
                (void)GenerateFullPlot( true );
                RepaintPlot();
            }
        }

        EnableAllMenus();
        if (bScopePowerStateChanged)
        {
            AdjustChannelsToMatchPowerMode();
            bScopePowerStateChanged = false;
        }
        if (!ResetEvent( hExecuteFraEvent ))
        {
            LogMessage( L"Fatal error: Failed to reset FRA execution start event" );
            return -1;
        }

        dwWaitResult = WaitForSingleObject( hExecuteFraEvent, INFINITE );

        if (dwWaitResult == WAIT_OBJECT_0)
        {
            if (NULL == pScopeSelector->GetSelectedScope()) // Scope not created
            {
                LogMessage( L"Error: Device not initialized." );
                continue;
            }
            else if (!(pScopeSelector->GetSelectedScope()->IsCompatible()))
            {
                LogMessage( L"Error: Selected scope is not compatible." );
                continue;
            }
            else if (!(pScopeSelector->GetSelectedScope()->Connected()))
            {
                LogMessage( L"Error: Selected scope not connected." );
                continue;
            }
            else if (pScopeSelector->GetSelectedScope()->RequiresExternalSignalGenerator() &&
                NULL == pExtSigGenPlugin->GetCurrentExtSigGen())
            {
                LogMessage( L"Error: Selected scope requires an external signal generator." );
                continue;
            }

            if (SIG_GEN_EXTERNAL == pSettings->GetSignalGeneratorSource() &&
                NULL == pExtSigGenPlugin->GetCurrentExtSigGen())
            {
                LogMessage( L"Error: External signal generator not initialized." );
                continue;
            }

            if (!ValidateSettings())
            {
                LogMessage( L"Error: Invalid inputs." );
                continue;
            }

            DisableAllMenus();
            plotInteractionAllowed = false;

            StoreSettings();

            std::vector<FRASegment_T> fraSegs;
            bool customPlanEnabled;
            customPlanEnabled = pSettings -> GetCustomPlan(fraSegs);

            inputChannel = pSettings->GetInputChannel();
            inputChannelCoupling = pSettings->GetInputCoupling();
            inputChannelAttenuation = pSettings->GetInputAttenuation();
            outputChannel = pSettings->GetOutputChannel();
            outputChannelCoupling = pSettings->GetOutputCoupling();
            outputChannelAttenuation = pSettings->GetOutputAttenuation();
            inputDcOffset = pSettings->GetInputDcOffsetAsDouble();
            outputDcOffset = pSettings->GetOutputDcOffsetAsDouble();

            if (!customPlanEnabled)
            {
                stimulusVpp = pSettings->GetStimulusVppAsDouble();
                stimulusOffset = pSettings->GetStimulusOffsetAsDouble();
                targetAmplitudeResponse = pSettings->GetTargetResponseAmplitudeAsDouble();
                maxStimulusVpp = pSettings->GetMaxStimulusVppAsDouble();
                startFreq = pSettings->GetStartFreqAsDouble();
                stopFreq = pSettings->GetStopFreqAsDouble();
                stepsPerDecade = pSettings->GetStepsPerDecadeAsInt();
            }

            psFRA->SetFraSettings( pSettings->GetSamplingMode(), (!customPlanEnabled) & (pSettings->GetAdaptiveStimulusMode()), targetAmplitudeResponse,
                                   pSettings->GetSweepDescending(), pSettings->GetPhaseWrappingThresholdAsDouble() );

            if (false == psFRA -> SetupChannels( inputChannel, inputChannelCoupling, inputChannelAttenuation, inputDcOffset,
                                                 outputChannel, outputChannelCoupling, outputChannelAttenuation, outputDcOffset,
                                                 stimulusVpp, maxStimulusVpp, stimulusOffset ))
            {
                continue;
            }

            SetPlotTitle( false );

            // This is used to defer the initial plot update with BeginPlot (which clears old data) until the first new data point arrives.
            // If we immediately call BeginPlot here, actions like canceling before the first data point arrives (including selecting
            // not to continue from the Pre-Run Alert Dialog) lead to the inability to interact with a perfectly good plot.
            awaitingFirstDataPoint = true;

            if (!customPlanEnabled)
            {
                if (false == psFRA -> ExecuteFRA( startFreq, stopFreq, stepsPerDecade ))
                {
                    continue;
                }
            }
            else
            {
                if (fraSegs.size() != 0)
                {
                    if (false == psFRA -> ExecuteFRA( fraSegs ))
                    {
                        continue;
                    }
                }
                else
                {
                    LogMessage( L"Error: No custom plan defined.  Press \"Custom\" button." );
                    continue;
                }
            }
        }
        else
        {
            LogMessage( L"Fatal error: Invalid result from waiting on FRA execution start event" );
            return -1;
        }
    }

    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: AdjustChannelsToMatchPowerMode
//
// Purpose: A function to reset the UI and instrument assigmnent when the number of channels
//          has changed due to a power state change.
//
// Parameters: None
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void AdjustChannelsToMatchPowerMode(void)
{
    PicoScope* pScope = pScopeSelector->GetSelectedScope();
    if (NULL != pScope)
    {
        pSettings->SetNumChannels(pScope->GetNumChannels());
    }
    psFRA->SetInstrument(pScope); // Causes the FRA object to reload the number of channels
    LoadControlsData(hMainWnd);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: BeginPlot
//
// Purpose: Function to generate the plot base
//
// Parameters: [out] - whether the function succeeded
//             [out] - repaint - whether we need to repaint the plot because it was updated
//
// Notes: Does error handling and can write to the log on error.
//        Assumes UI settings have been stored to aplication settings
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool BeginPlot(bool& repaint)
{
    tuple<bool,double,double> freqAxisScale, gainAxisScale, phaseAxisScale;
    tuple<double,uint8_t,bool,bool> freqAxisIntervals, gainAxisIntervals, phaseAxisIntervals;
    bool gainMasterIntervals, phaseMasterIntervals;
    double startFreq, stopFreq;

    std::vector<FRASegment_T> fraSegs;
    if (pSettings->GetCustomPlan(fraSegs) && fraSegs.size() > 0)
    {
        startFreq = fraSegs.front().startFreqHz;
        stopFreq = fraSegs.back().stopFreqHz;
    }
    else
    {
        startFreq = pSettings->GetStartFreqAsDouble();
        stopFreq = pSettings->GetStopFreqAsDouble();
    }

    if (pSettings->GetAutoAxes())
    {
        // Set scales and intervals for automatic axes/grid determination
        gainAxisScale = phaseAxisScale = tuple<bool,double,double>(true, 0.0, 0.0);
        gainAxisIntervals = phaseAxisIntervals = tuple<double,uint8_t,bool,bool>(0.0, 0, true, true);

        freqAxisScale = tuple<bool,double,double>(true, startFreq, stopFreq);
        freqAxisIntervals = pSettings->GetFreqIntervals();

        // Default to gain being the master axis
        if (pSettings->GetPlotGain() || !pSettings->GetPlotPhase())
        {
            gainMasterIntervals = true;
            phaseMasterIntervals = false;
        }
        else
        {
            gainMasterIntervals = false;
            phaseMasterIntervals = true;
        }
    }
    else
    {
        freqAxisScale = pSettings->GetFreqScale();
        gainAxisScale = pSettings->GetGainScale();
        phaseAxisScale = pSettings->GetPhaseScale();
        freqAxisIntervals = pSettings->GetFreqIntervals();
        gainAxisIntervals = pSettings->GetGainIntervals();
        phaseAxisIntervals = pSettings->GetPhaseIntervals();
        gainMasterIntervals = pSettings->GetGainMasterIntervals();
        phaseMasterIntervals = pSettings->GetPhaseMasterIntervals();

        if (get<0>(freqAxisScale))
        {
            freqAxisScale = tuple<bool,double,double>(true, startFreq, stopFreq);
        }
    }

    try
    {
        fraPlotter -> SetPlotSettings( pSettings->GetPlotGain(), pSettings->GetPlotPhase(), pSettings->GetPlotNeg3dB(), pSettings->GetPlotGainMargin(), pSettings->GetPlotPhaseMargin(),
                                       pSettings->GetGainMarginPhaseCrossoverAsDouble(), pSettings->GetPlotUnwrappedPhase(), false, false );
        repaint = fraPlotter->BeginPlot( freqAxisScale, gainAxisScale, phaseAxisScale, freqAxisIntervals, gainAxisIntervals, phaseAxisIntervals, gainMasterIntervals, phaseMasterIntervals, pSettings->GetPlotRealTime() );
    }
    catch (runtime_error e)
    {
        wstringstream wss;
        wss << e.what();
        LogMessage( wss.str() );
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: AddPlotDataPoint
//
// Purpose: Function to generate the plot, one data point at a time
//
// Parameters: [out] - whether the function succeeded
//             [out] - repaint - whether we need to repaint the plot because it was updated
//             [in] - freq, gain, phase - data point to be added to the plot
//
// Notes: Does error handling and can write to the log on error.
//        Assumes UI settings have been stored to aplication settings
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool AddPlotDataPoint( double freq, double gain, double phase, bool& repaint )
{
    try
    {
        repaint = fraPlotter->PlotFRAPoint(freq, gain, phase);
    }
    catch (runtime_error e)
    {
        wstringstream wss;
        wss << e.what();
        LogMessage( wss.str() );
        return false;
    }

    return true;

}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GenerateFullPlot
//
// Purpose: Common function to generate the plot
//
// Parameters: [in] - rescale - whether the plot is rescaled
//
// Notes: Does error handling and can write to the log on error.
//        Assumes UI settings have been stored to application settings
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool GenerateFullPlot(bool rescale)
{
    tuple<bool,double,double> freqAxisScale, gainAxisScale, phaseAxisScale;
    tuple<double,uint8_t,bool,bool> freqAxisIntervals, gainAxisIntervals, phaseAxisIntervals;
    bool gainMasterIntervals, phaseMasterIntervals;

    try
    {
        fraPlotter -> SetPlotSettings( pSettings->GetPlotGain(), pSettings->GetPlotPhase(), pSettings->GetPlotNeg3dB(), pSettings->GetPlotGainMargin(), pSettings->GetPlotPhaseMargin(),
                                       pSettings->GetGainMarginPhaseCrossoverAsDouble(), pSettings->GetPlotUnwrappedPhase(), !rescale, rescale );
        if (rescale)
        {
            if (pSettings->GetAutoAxes())
            {
                // Set scales and intervals for automatic axes/grid determination
                freqAxisScale = gainAxisScale = phaseAxisScale = tuple<bool,double,double>(true, 0.0, 0.0);
                freqAxisIntervals = gainAxisIntervals = phaseAxisIntervals = tuple<double,uint8_t,bool,bool>(0.0, 0, true, true);

                // Default to gain being the master axis
                if (pSettings->GetPlotGain() || !pSettings->GetPlotPhase())
                {
                    gainMasterIntervals = true;
                    phaseMasterIntervals = false;
                }
                else
                {
                    gainMasterIntervals = false;
                    phaseMasterIntervals = true;
                }
            }
            else
            {
                freqAxisScale = pSettings->GetFreqScale();
                gainAxisScale = pSettings->GetGainScale();
                phaseAxisScale = pSettings->GetPhaseScale();
                freqAxisIntervals = pSettings->GetFreqIntervals();
                gainAxisIntervals = pSettings->GetGainIntervals();
                phaseAxisIntervals = pSettings->GetPhaseIntervals();
                gainMasterIntervals = pSettings->GetGainMasterIntervals();
                phaseMasterIntervals = pSettings->GetPhaseMasterIntervals();
            }

            fraPlotter -> SetPlotAxes( freqAxisScale, gainAxisScale, phaseAxisScale,
                                        freqAxisIntervals, gainAxisIntervals, phaseAxisIntervals,
                                        gainMasterIntervals, phaseMasterIntervals, true, rescale );
        }
    }
    catch (runtime_error e)
    {
        wstringstream wss;
        wss << e.what();
        LogMessage( wss.str() );
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: RepaintsPlot
//
// Purpose: Common function to re-draw the generated plot to the application window
//
// Parameters: None
//
// Notes: Does error handling and can write to the log on error.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void RepaintPlot(void)
{
    unique_ptr<uint8_t[]> buffer;

    try
    {
        buffer = fraPlotter->GetScreenBitmapPlot32BppBGRA();
    }
    catch (runtime_error e)
    {
        wstringstream wss;
        wss << e.what();
        LogMessage( wss.str() );
        return;
    }

    if (DeleteObject( hPlotBM ))
    {
        if (NULL != (hPlotBM = CreateBitmap(pxPlotWidth, pxPlotHeight, 1, 32, buffer.get())))
        {
            RECT invalidRect = { pxPlotXStart, pxPlotYStart, pxPlotXStart + pxPlotWidth, pxPlotYStart + pxPlotHeight };
            if (InvalidateRect( hMainWnd, &invalidRect, true ))
            {
                plotOnScreen = true;
            }
            else
            {
                LogMessage( L"Error: InvalidateRect failed while painting plot. Plot image may be stale." );
            }
        }
        else
        {
            LogMessage( L"Error: CreateBitmap failed while painting plot. Plot image may be stale." );
        }
    }
    else
    {
        LogMessage( L"Error: DeleteObject failed while painting plot. Plot image may be stale." );
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ValidateChannelSettings
//
// Purpose: Check whether the user's provided channel settings are valid.
//
// Parameters: [out] return: true if valid, false otherwise
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ValidateChannelSettings( void )
{
    auto retVal = true;

    HWND hndCtrl;
    TCHAR editText[16];
    PS_CHANNEL inputChannel, outputChannel;
    PS_COUPLING inputChannelCoupling, outputChannelCoupling;
    ATTEN_T inputAtten, outputAtten;
    double inputDcOffset = 0.0;
    double outputDcOffset = 0.0;

    // These need to be checked in case the user switched from 4 channels to 2 (due to Flexible Power)
        // and the prior selection was channel C/D
    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_CHANNEL );
    if ((uint8_t)(inputChannel = (PS_CHANNEL)ComboBox_GetCurSel( hndCtrl )) >= pSettings->GetNumChannels())
    {
        LogMessage( L"Input channel selection is invalid." );
        throw std::exception();
    }

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_CHANNEL );
    if ((uint8_t)(outputChannel = (PS_CHANNEL)ComboBox_GetCurSel( hndCtrl )) >= pSettings->GetNumChannels())
    {
        LogMessage( L"Output channel selection is invalid." );
        throw std::exception();
    }

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_COUPLING );
    inputChannelCoupling = (PS_COUPLING)ComboBox_GetCurSel( hndCtrl );

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_COUPLING );
    outputChannelCoupling = (PS_COUPLING)ComboBox_GetCurSel( hndCtrl );

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_DC_OFFS );
    Edit_GetText( hndCtrl, editText, 16 );
    if (!WStringToDouble( editText, inputDcOffset ))
    {
        LogMessage( L"Input DC offset is not a valid number." );
        retVal = false;
    }

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_DC_OFFS );
    Edit_GetText( hndCtrl, editText, 16 );
    if (!WStringToDouble( editText, outputDcOffset ))
    {
        LogMessage( L"Output DC offset is not a valid number." );
        retVal = false;
    }

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_ATTEN );
    inputAtten = (ATTEN_T)ComboBox_GetCurSel( hndCtrl );

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_ATTEN );
    outputAtten = (ATTEN_T)ComboBox_GetCurSel( hndCtrl );

    // Setting up channels so that GetMinFrequency works appropriately.  Some parameters are arbitrary
    // values because they don't matter for this purpose.
    if (retVal && false == psFRA->SetupChannels( inputChannel, inputChannelCoupling, inputAtten, inputDcOffset,
        outputChannel, outputChannelCoupling, outputAtten, outputDcOffset,
        0.0, 0.0, 0.0 ))
    {
        LogMessage( L"Unable to setup channels while validating parameters." );
        retVal = false;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ValidateSettings
//
// Purpose: Check whether the user's provided settings are valid.
//
// Parameters: [out] return: true if valid, false otherwise
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ValidateSettings(void)
{
    auto retVal = true;

    HWND hndCtrl;
    TCHAR editText[16];
    wstringstream wss;
    double stimulusVpp;
    double stimulusOffset;
    double responseTarget;
    double startFreq;
    double stopFreq;
    tuple <bool, double, double> freqScale;
    int32_t stepsPerDecade;
    bool validStartFreq = false;
    bool validStopFreq = false;
    bool validStimulusVpp = false;
    bool validStimulusOffset = false;

    try
    {
        // Get channels first because we need to set them up so that other functions such as
        // GetMinFrequency work correctly for scopes with timebase-channel dependencies

        retVal = ValidateChannelSettings();

        // Check to make sure that manually set frequency axis limts are not excessively
        // small; in particlar also guarding against 0.0 (without comparing to 0.0).
        // This could happen if the user manually edits the settings file.
        // If they are too small, just adjust them based on scope capabilities.
        // This is important because we don't want the plotting routines trying
        // to take a log of 0.0.
        freqScale = pSettings->GetFreqScale();
        if (!get<0>(freqScale)) // If not autoscale
        {
            if (get<1>(freqScale) < psFRA->GetMinFrequency( pSettings->GetDeviceResolution() ))
            {
                wstringstream wss;
                get<1>(freqScale) = psFRA->GetMinFrequency( pSettings->GetDeviceResolution() );
                pSettings->SetFreqScale(freqScale);
                wss << L"WARNING: Frequency axis minimum too small; adjusting to: " << get<1>(freqScale);
                LogMessage( wss.str(), FRA_WARNING );
            }
            if (get<2>(freqScale) < psFRA->GetMinFrequency( pSettings->GetDeviceResolution() ))
            {
                wstringstream wss;
                get<2>(freqScale) = pScopeSelector->GetSelectedScope()->GetMaxFuncGenFreq();
                pSettings->SetFreqScale(freqScale);
                wss << L"WARNING: Frequency axis maximum too small; adjusting to: " << get<2>(freqScale);
                LogMessage( wss.str(), FRA_WARNING );
            }
        }

        if (!(pSettings->GetCustomPlanEnabled()))
        {
            // Checking frequency earlier so that it can be used for stimulus ampliude/range checks
            hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_START_FREQ );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, startFreq))
            {
                LogMessage( L"Start frequency is not a valid number." );
                retVal = false;
            }
            else if (retVal) // Don't check min frequency if SetupChannels failed
            {
                double minFreq = psFRA->GetMinFrequency( pSettings->GetDeviceResolution() );
                if (startFreq < minFreq)
                {
                    wstringstream wss;
                    wss << L"Start frequency must be >= " << minFreq << L" Hz";
                    LogMessage( wss.str() );
                    retVal = false;
                }
                else
                {
                    validStartFreq = true;
                }
            }

            hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_STOP_FREQ );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, stopFreq))
            {
                LogMessage( L"Stop frequency is not a valid number." );
                retVal = false;
            }
            else
            {
                double maxFreq = psFRA->GetMaxFrequency();
                if (stopFreq > maxFreq)
                {
                    wstringstream wss;
                    wss << L"Stop frequency must be <= " << fixed << setprecision(1) << maxFreq;
                    LogMessage( wss.str() );
                    retVal = false;
                }
                else
                {
                    validStopFreq = true;
                }
            }

            if (validStartFreq && validStopFreq && startFreq >= stopFreq)
            {
                LogMessage( L"Stop frequency must be > start frequency" );
                validStartFreq = validStopFreq = false;
                retVal = false;
            }

            hndCtrl = GetDlgItem( hMainWnd, IDC_STIMULUS_VPP );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToDouble(editText, stimulusVpp))
            {
                LogMessage( L"Stimulus amplitude is not a valid number." );
                retVal = false;
            }
            else if (stimulusVpp <= 0.0)
            {
                LogMessage( L"Stimulus amplitude must be > 0.0" );
                retVal = false;
            }
            else
            {
                validStimulusVpp = true;
            }

            if (psFRA->SigGenSupportsDcOffset())
            {
                hndCtrl = GetDlgItem( hMainWnd, IDC_STIMULUS_OFFSET );
                Edit_GetText( hndCtrl, editText, 16 );
                if (!WStringToDouble(editText, stimulusOffset))
                {
                    LogMessage( L"Stimulus offset is not a valid number." );
                    retVal = false;
                }
                else
                {
                    validStimulusOffset = true;
                }
            }
            else
            {
                stimulusOffset = 0.0;
                validStimulusOffset = true;
            }

            if (validStimulusVpp && validStimulusOffset && validStartFreq && validStopFreq)
            {
                // This logic assumes that the maximum sine amplitude is also the maximum range for the signal generator.
                // This is definitely true for PicoScopes, and for many other standalone signal generators, but it's possible
                // that an external signal generator someome wants to add support for in the future may have independently
                // adjustable amplitude and offset.  If that happens, the external signal generator API will need to change
                // to publish whether this is the case and this logic will need to adapt accordingly.

                double maxSignalAbs = psFRA->GetMaxFuncGenVpp(startFreq, stopFreq) / 2.0;
                if ((fabs(stimulusOffset) + stimulusVpp / 2.0) > maxSignalAbs)
                {
                    wstringstream wss;
                    wss << L"Stimulus exceeds signal generator limit of +/-" << fixed << setprecision(1) << maxSignalAbs << L" V";
                    LogMessage( wss.str() );
                    retVal = false;
                }

                if (stimulusVpp < psFRA->GetMinFuncGenVpp(startFreq, stopFreq))
                {
                    double minSignalAbs = psFRA->GetMinFuncGenVpp(startFreq, stopFreq) / 2.0;
                    wstringstream wss;
                    wss << L"Stimulus smaller than signal generator limit of +/-" << fixed << setprecision(1) << minSignalAbs << L" V";
                    LogMessage( wss.str() );
                    retVal = false;
                }
            }

            if (pSettings->GetAdaptiveStimulusMode())
            {
                hndCtrl = GetDlgItem( hMainWnd, IDC_RESPONSE_TARGET );
                Edit_GetText( hndCtrl, editText, 16 );
                if (!WStringToDouble(editText, responseTarget))
                {
                    LogMessage( L"Response target is not a valid number." );
                    retVal = false;
                }
                else if (responseTarget <= 0.0)
                {
                    LogMessage( L"Response target must be > 0.0" );
                    retVal = false;
                }

                hndCtrl = GetDlgItem( hMainWnd, IDC_MAX_STIMULUS_VPP );
                Edit_GetText( hndCtrl, editText, 16 );
                if (!WStringToDouble(editText, stimulusVpp))
                {
                    LogMessage( L"Maximum stimulus Vpp is not a valid number." );
                    retVal = false;
                }
                else if (stimulusVpp <= 0.0)
                {
                    LogMessage( L"Maximum stimulus Vpp must be > 0.0" );
                    retVal = false;
                }
                else if (validStartFreq && validStopFreq)
                {
                    // Adaptive stimulus does not support offset so no need to consider it here.
                    double maxSignalVpp = psFRA->GetMaxFuncGenVpp(startFreq, stopFreq);
                    if (stimulusVpp > maxSignalVpp)
                    {
                        wstringstream wss;
                        wss << L"Maximum stimulus Vpp must be <= " << fixed << setprecision(1) << maxSignalVpp;
                        LogMessage( wss.str() );
                        retVal = false;
                    }
                }
            }

            hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_STEPS_PER_DECADE );
            Edit_GetText( hndCtrl, editText, 16 );
            if (!WStringToInt32(editText, stepsPerDecade))
            {
                LogMessage( L"Steps per decade is not a valid number." );
                retVal = false;
            }
            else if (stepsPerDecade < 1)
            {
                LogMessage( L"Steps per decade must be >= 1." );
                retVal = false;
            }
        }
    }
    catch (std::exception e)
    {
        UNREFERENCED_PARAMETER(e);
        MessageBox( hMainWnd, L"Could not validate settings.", L"Error", MB_OK );
        retVal = false;
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: StoreSettings
//
// Purpose: Store user settings back to the global settings object.
//
// Parameters: [out] return: currently not used, always true
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool StoreSettings(void)
{
    HWND hndCtrl;
    TCHAR editText[16];

    // Channel Settings
    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_CHANNEL );
    pSettings -> SetInputChannel(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_COUPLING );
    pSettings -> SetInputCoupling(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_ATTEN );
    pSettings -> SetInputAttenuation(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_CHANNEL );
    pSettings -> SetOutputChannel(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_COUPLING );
    pSettings -> SetOutputCoupling(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_ATTEN );
    pSettings -> SetOutputAttenuation(ComboBox_GetCurSel( hndCtrl ));

    hndCtrl = GetDlgItem( hMainWnd, IDC_INPUT_DC_OFFS );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings -> SetInputDcOffset( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_OUTPUT_DC_OFFS );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings -> SetOutputDcOffset( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_STIMULUS_VPP );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings -> SetStimulusVpp( editText );

    if (psFRA->SigGenSupportsDcOffset())
    {
        hndCtrl = GetDlgItem( hMainWnd, IDC_STIMULUS_OFFSET );
        Edit_GetText( hndCtrl, editText, 16 );
        pSettings -> SetStimulusOffset( editText );
    }
    else
    {
        pSettings -> SetStimulusOffset (L"0.0");
    }

    hndCtrl = GetDlgItem( hMainWnd, IDC_CUSTOM_CHECK );
    pSettings->SetCustomPlanEnabled(Button_GetCheck(hndCtrl) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_RESPONSE_TARGET );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings -> SetTargetResponseAmplitude( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_MAX_STIMULUS_VPP );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings -> SetMaxStimulusVpp( editText );

    // FRA inputs
    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_START_FREQ );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings->SetStartFrequency( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_STOP_FREQ );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings->SetStopFrequency( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_STEPS_PER_DECADE );
    Edit_GetText( hndCtrl, editText, 16 );
    pSettings->SetStepsPerDecade( editText );

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_AUTO_AXES );
    pSettings->SetAutoAxes(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_PLOT_GAIN );
    pSettings->SetPlotGain(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_PLOT_PHASE );
    pSettings->SetPlotPhase(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_PLOT_NEG3DB );
    pSettings->SetPlotNeg3dB(Button_GetCheck(hndCtrl) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_PLOT_GM );
    pSettings->SetPlotGainMargin(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_PLOT_PM );
    pSettings->SetPlotPhaseMargin(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hMainWnd, IDC_FRA_UNWRAP_PHASE );
    pSettings->SetPlotUnwrappedPhase(Button_GetCheck( hndCtrl ) == BST_CHECKED);

    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: FraStatusCallback
//
// Purpose: Callback, provided as a means to indicate status of various operations.  Status is 
//          interpreted and written to the log.  Also acts as part of the messaging to support
//          interactive auto-ranging
//
// Parameters: [in] fraStatusMsg: The status
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool FraStatusCallback( FRA_STATUS_MESSAGE_T& fraStatusMsg )
{
    if (fraStatusMsg.status == FRA_STATUS_IN_PROGRESS)
    {
        HWND hndCtrl;
        TCHAR szStatus[64];

        wsprintf( szStatus, L"%d of %d steps complete",
                  get<FraStatusProgress>(fraStatusMsg.statusData).stepsComplete,
                  get<FraStatusProgress>(fraStatusMsg.statusData).numSteps );

        hndCtrl = GetDlgItem( hMainWnd, IDC_STATUS_TEXT );
        Edit_SetText( hndCtrl, szStatus );
    }
    else if (fraStatusMsg.status == FRA_STATUS_CANCELED)
    {
        HWND hndCtrl;
        TCHAR szStatus[64];

        wsprintf( szStatus, L"Canceled at %d of %d steps complete",
                  get<FraStatusCancelPoint>(fraStatusMsg.statusData).stepsComplete,
                  get<FraStatusCancelPoint>(fraStatusMsg.statusData).numSteps );

        hndCtrl = GetDlgItem( hMainWnd, IDC_STATUS_TEXT );
        Edit_SetText( hndCtrl, szStatus );
    }
    else if (fraStatusMsg.status == FRA_STATUS_FATAL_ERROR)
    {
        HWND hndCtrl;
        TCHAR szStatus[64];

        wsprintf( szStatus, L"Fatal error at %d of %d steps complete",
                  get<FraStatusProgress>(fraStatusMsg.statusData).stepsComplete,
                  get<FraStatusProgress>(fraStatusMsg.statusData).numSteps );

        hndCtrl = GetDlgItem( hMainWnd, IDC_STATUS_TEXT );
        Edit_SetText( hndCtrl, szStatus );

        LogMessage( fraStatusMsg.statusText );
    }
    else if (fraStatusMsg.status == FRA_STATUS_DATA)
    {
        bool repaint = false;

        if (awaitingFirstDataPoint)
        {
            awaitingFirstDataPoint = false;
            (void)BeginPlot( repaint );

            if (repaint)
            {
                RepaintPlot();
            }
        }

        FraStatusFraData fraData = get<FraStatusFraData>(fraStatusMsg.statusData);
        (void)AddPlotDataPoint(fraData.freqLogHz, fraData.gainDb, fraData.phaseDeg, repaint);
        if (repaint)
        {
            RepaintPlot();
        }
    }
    else if (fraStatusMsg.status == FRA_STATUS_MESSAGE)
    {
        LogMessage( fraStatusMsg.statusText, fraStatusMsg.messageType );
    }
    else if (fraStatusMsg.status == FRA_STATUS_RETRY_LIMIT)
    {
        DWORD dwDlgResp;
        dwDlgResp = DialogBoxParam( hInst, MAKEINTRESOURCE(IDD_INTERACTIVE_RETRY), hMainWnd, InteractiveRetryHandler, (LPARAM)&fraStatusMsg );

        if (IDABORT == dwDlgResp)
        {
            fraStatusMsg.responseData.proceed = false;
        }
        else if (IDCONTINUE == dwDlgResp)
        {
            fraStatusMsg.responseData.proceed = true;
            fraStatusMsg.responseData.retry = false;
        }
        else if (IDRETRY == dwDlgResp)
        {
            fraStatusMsg.responseData.proceed = true;
            fraStatusMsg.responseData.retry = true;
        }
        else
        {
            fraStatusMsg.responseData.proceed = false;
        }
    }
    else if (fraStatusMsg.status == FRA_STATUS_PRE_RUN_ALERT)
    {
        DWORD dwDlgResp;
        wstringstream wss;
        bool errorsExist = false;

        FraStatusPreRunAlert preRunAlert = get<FraStatusPreRunAlert>(fraStatusMsg.statusData);

        dwDlgResp = DialogBoxParam( hInst, MAKEINTRESOURCE( IDD_PRE_RUN_ALERT ), hMainWnd, PreRunAlertHandler, (LPARAM)&preRunAlert );

        if (dwDlgResp == IDYES)
        {
            fraStatusMsg.responseData.proceed = true;
        }
        else if (dwDlgResp == IDNO)
        {
            fraStatusMsg.responseData.proceed = false;
        }
    }
    else if (fraStatusMsg.status == FRA_STATUS_POWER_CHANGED)
    {
        bScopePowerStateChanged = true;
        if (fraStatusMsg.responseData.proceed == false)
        {
            LogMessage( L"DC power disconnected; FRA stopped", SCOPE_POWER_EVENTS );
            (void)MessageBoxEx(hMainWnd, L"DC power disconnected.  FRA will be stopped because channels other than A and B were selected.", L"Power Changed", MB_OK, 0);
        }
        else
        {
            int response;
            if (get<FraStatusPowerState>(fraStatusMsg.statusData).auxDcPowered)
            {
                LogMessage( L"DC power connected", SCOPE_POWER_EVENTS );
                response = MessageBoxEx(hMainWnd, L"DC power connected.  Continue?", L"Power Changed", MB_OKCANCEL, 0);
            }
            else
            {
                LogMessage( L"DC power disconnected", SCOPE_POWER_EVENTS );
                response = MessageBoxEx(hMainWnd, L"DC power disconnected.  Continue?", L"Power Changed", MB_OKCANCEL, 0);
            }

            if (IDCANCEL == response)
            {
                fraStatusMsg.responseData.proceed = false;
            }
            else if (IDOK == response)
            {
                fraStatusMsg.responseData.proceed = true;
            }
        }

    }
    return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: LogMessage
//
// Purpose: Provide a simpler functionality than FraStatusCallback to display log messages
//
// Parameters: [in] statusMessage: string to log/display
//             [in] type: type of message, used to determine whether to log a message based on
//                         verbosity settings
//
// Notes: This is the producer side of a system that uses a queue and worker thread to do the
//        actual work.  This function can thus be safely called from multiple threads.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void LogMessage( const wstring statusMessage, LOG_MESSAGE_FLAGS_T type )
{
    MessageLogData_T data;
    uint32_t copyLength;

    copyLength = min((sizeof(data.message)/sizeof(wchar_t))-1, statusMessage.length());

    statusMessage.copy(data.message, copyLength, 0);
    data.message[copyLength] = 0;

    data.type = type;

    pMessageLoggingProcessor->PostMsg(&data);

    if (copyLength < statusMessage.length())
    {
        wstring warningMessage = L"WARNING: Log message was truncated";
        data.type = FRA_WARNING;
        warningMessage.copy(data.message, warningMessage.length(), 0);
        data.message[warningMessage.length()] = 0;
        pMessageLoggingProcessor->PostMsg(&data);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: LogMessageWorker
//
// Purpose: This function does the work of actually logging messages
//
// Parameters: [in] data: data for a single log message
//
// Notes: This is the processing side of a MessageProcessor that uses a queue and worker thread
//        to do the actual work.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void LogMessageProcessor(MessageLogData_T& data)
{
    bool bVerbosityFlagSet = true;
    try
    {
        bVerbosityFlagSet = pSettings->GetLogVerbosityFlag(data.type);
    }
    catch (const ptree_error& pte)
    {
        UNREFERENCED_PARAMETER(pte);
        bVerbosityFlagSet = true;
    }

    if (FRA_ERROR == data.type || bVerbosityFlagSet)
    {
        HWND hndCtrl;
        hndCtrl = GetDlgItem(hMainWnd, IDC_LOG);
        int insertPoint = GetWindowTextLength(hndCtrl);
        Edit_SetSel(hndCtrl, insertPoint, insertPoint);
        wstring msg = data.message;
        msg.append(L"\r\n");

        // If it's a warning or error, make it bold with special color
        CHARFORMAT cf;
        cf.cbSize = sizeof(cf);
        cf.dwMask = CFM_BOLD | CFM_COLOR;
        cf.dwEffects = CFE_BOLD;
        if (FRA_ERROR == data.type)
        {
            cf.crTextColor = RGB(255, 0, 0); // Red
            SendMessage(hndCtrl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
        }
        if (FRA_WARNING == data.type)
        {
            cf.crTextColor = RGB(130, 130, 0); // Dark yellow
            SendMessage(hndCtrl, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&cf);
        }

        Edit_ReplaceSel(hndCtrl, msg.c_str());

        Edit_SetSel(hndCtrl, -1, -1);
        SendMessage(hndCtrl, EM_HIDESELECTION, 0, 0);
        Edit_ScrollCaret(hndCtrl);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: CopyLog
//
// Purpose: Copies the log to the clipboard
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void CopyLog(void)
{
    HWND hndCtrl;
    hndCtrl = GetDlgItem( hMainWnd, IDC_LOG );
    int length = GetWindowTextLength(hndCtrl);
    Edit_SetSel( hndCtrl, 0, length );
    SendMessage( hndCtrl, WM_COPY, 0, 0 );
    Edit_SetSel( hndCtrl, -1, -1 );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ClearLog
//
// Purpose: Erases the log from the display
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void ClearLog(void)
{
    HWND hndCtrl;
    hndCtrl = GetDlgItem( hMainWnd, IDC_LOG );
    Edit_SetText( hndCtrl, L"" );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: About
//
// Purpose: Message handler for Help->About box.
//
// Parameters: See Windows programming API information
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
        case WM_INITDIALOG:
        {
            HWND hAppIdText = GetDlgItem( hDlg, IDC_APPID );
            wstringstream AppIdSS;
            AppIdSS << appNameString << L", Version " << appVersionString;
            Static_SetText( hAppIdText, AppIdSS.str().c_str() );
            return (INT_PTR)TRUE;
        }
        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
            {
                EndDialog(hDlg, LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;
    }
    return (INT_PTR)FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ScopeSelectHandler
//
// Purpose: Message handler for Scope selection dialog box.
//
// Parameters: See Windows programming API information
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK ScopeSelectHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

    bool scopeAvail = false;
    LVCOLUMN listViewCol;
    LVITEM listViewItem;

    // Used to get text width pixels from DLUs; 220 (minus margin = 210) split three ways
    RECT textWidthRect = {50, 80, 80, 0}; // Use in ascending order for text width

    switch (message)
    {
        case WM_INITDIALOG:

            vector<AvailableScopeDescription_T>* pScopes;
            HWND hndLvCtrl;
            hndLvCtrl = GetDlgItem( hDlg, IDC_SCOPE_SELECT_LIST );

            ListView_SetExtendedListViewStyle( hndLvCtrl, ListView_GetExtendedListViewStyle(hndLvCtrl) | LVS_EX_FULLROWSELECT );

            MapDialogRect(hDlg, &textWidthRect);

            listViewCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;

            listViewCol.pszText = L"Family";
            listViewCol.fmt = LVCFMT_LEFT;
            listViewCol.cx = textWidthRect.left;
            listViewCol.iSubItem = 0;
            ListView_InsertColumn(hndLvCtrl, 0, &listViewCol );

            listViewCol.pszText = L"S/N";
            listViewCol.fmt = LVCFMT_LEFT;
            listViewCol.cx = textWidthRect.top;
            listViewCol.iSubItem = 1;
            ListView_InsertColumn(hndLvCtrl, 1, &listViewCol );

            listViewCol.pszText = L"Status";
            listViewCol.fmt = LVCFMT_LEFT;
            listViewCol.cx = textWidthRect.right;
            listViewCol.iSubItem = 2;
            ListView_InsertColumn(hndLvCtrl, 2, &listViewCol );

            pScopes = (vector<AvailableScopeDescription_T>*)lParam;

            listViewItem.mask = LVIF_TEXT;
            for (uint8_t idx = 0; idx < pScopes->size(); idx++)
            {
                listViewItem.iItem = idx;

                listViewItem.pszText = (LPWSTR)(*pScopes)[idx].wFamilyName.c_str();
                listViewItem.iSubItem = 0;
                ListView_InsertItem( hndLvCtrl, &listViewItem );

                ListView_SetItemText(hndLvCtrl, idx, 1, (LPWSTR)(*pScopes)[idx].wSerialNumber.c_str());

                if ((*pScopes)[idx].connected)
                {
                    ListView_SetItemText( hndLvCtrl, idx, 2, L"Connected" );
                }
                else
                {
                    scopeAvail = true;
                    ListView_SetItemText( hndLvCtrl, idx, 2, L"Available" );
                }
            }

            ListView_SetItemState( hndLvCtrl, 0, LVIS_SELECTED, LVIS_SELECTED );

            return (INT_PTR)TRUE;

        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCLOSE)
            {
                int index;
                HWND hndLvCtrl;
                hndLvCtrl = GetDlgItem( hDlg, IDC_SCOPE_SELECT_LIST );
                index = ListView_GetNextItem( hndLvCtrl, -1, LVNI_SELECTED );
                EndDialog(hDlg, index<<16 | LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;
        case WM_NOTIFY:
            int itemSelected;
            NMHDR* nmhdr = (NMHDR*)lParam;
            if ( nmhdr->code == LVN_ITEMCHANGED )
            {
                vector<AvailableScopeDescription_T> scopes;
                HWND hndConnectButton;
                hndConnectButton = GetDlgItem( hDlg, IDOK );

                NMLISTVIEW *nmlv = (NMLISTVIEW*)lParam;
                if (nmlv->uNewState & LVIS_SELECTED)
                {
                    itemSelected = nmlv->iItem;

                    pScopeSelector->GetAvailableScopes(scopes, false);

                    // If the scope selected is already connected, disable the Connect button
                    if (scopes[itemSelected].connected)
                    {
                        Button_Enable( hndConnectButton, FALSE );
                    }
                    else
                    {
                        Button_Enable( hndConnectButton, TRUE );
                    }
                }
                else // New state for some item is unselected
                {
                    if (-1 == ListView_GetNextItem( nmhdr->hwndFrom, -1, LVNI_SELECTED ))
                    {
                        Button_Enable( hndConnectButton, FALSE );
                    }
                }
            }
    }
    return (INT_PTR)FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SignalGeneratorSelectHandler
//
// Purpose: Message handler for Signal Generator selection dialog box.
//
// Parameters: See Windows programming API information
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK SignalGeneratorSelectHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

    bool scopeAvail = false;
    LVCOLUMN listViewCol;
    LVITEM listViewItem;

    // Used to get text width pixels from DLUs; 220 (minus margin = 210) split three ways
    RECT textWidthRect = {50, 80, 80, 0}; // Use in ascending order for text width

    switch (message)
    {
        case WM_INITDIALOG:

            std::vector<std::wstring>* pIds;
            HWND hndLvCtrl;
            hndLvCtrl = GetDlgItem( hDlg, IDC_SIGGEN_SELECT_LIST );

            ListView_SetExtendedListViewStyle( hndLvCtrl, ListView_GetExtendedListViewStyle(hndLvCtrl) | LVS_EX_FULLROWSELECT );

            MapDialogRect(hDlg, &textWidthRect);

            listViewCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;

            listViewCol.pszText = L"ID";
            listViewCol.fmt = LVCFMT_LEFT;
            listViewCol.cx = textWidthRect.left;
            listViewCol.iSubItem = 0;
            ListView_InsertColumn(hndLvCtrl, 0, &listViewCol );

            pIds = (std::vector<std::wstring>*)lParam;

            listViewItem.mask = LVIF_TEXT;
            for (uint8_t idx = 0; idx < pIds->size(); idx++)
            {
                listViewItem.iItem = idx;

                listViewItem.pszText = (LPWSTR)(*pIds)[idx].c_str();
                listViewItem.iSubItem = 0;
                ListView_InsertItem( hndLvCtrl, &listViewItem );
            }

            ListView_SetItemState( hndLvCtrl, 0, LVIS_SELECTED, LVIS_SELECTED );

            return (INT_PTR)TRUE;

        case WM_COMMAND:
            if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCLOSE)
            {
                int index;
                HWND hndLvCtrl;
                hndLvCtrl = GetDlgItem( hDlg, IDC_SIGGEN_SELECT_LIST );
                index = ListView_GetNextItem( hndLvCtrl, -1, LVNI_SELECTED );
                EndDialog(hDlg, index<<16 | LOWORD(wParam));
                return (INT_PTR)TRUE;
            }
            break;
    }
    return (INT_PTR)FALSE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SaveRawData
//
// Purpose: Saves the raw data results (frequencies, gain, phase) to a comma separated data file
//
// Parameters: dataFilePath: path to the data file.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SaveRawData( wstring dataFilePath )
{
    int numSteps;
    double *freqsLogHz, *phasesDeg, *unwrappedPhasesDeg, *gainsDb, *phases;

    ofstream dataFileOutputStream;

    psFRA->GetResults( &numSteps, &freqsLogHz, &gainsDb, &phasesDeg, &unwrappedPhasesDeg );

    if (numSteps == 0)
    {
        MessageBox( 0, L"No data is available to export.", L"Error", MB_OK );
    }
    else
    {
        dataFileOutputStream.open( dataFilePath.c_str(), ios::out );

        if (dataFileOutputStream)
        {
            phases = pSettings->GetPlotUnwrappedPhase() ? unwrappedPhasesDeg : phasesDeg;
            dataFileOutputStream << "Frequency Log(Hz), Gain (dB), Phase (deg)\n";
            dataFileOutputStream.precision(numeric_limits<double>::digits10);
            for (int idx = 0; idx < numSteps; idx++)
            {
                dataFileOutputStream << freqsLogHz[idx] << ", " << gainsDb[idx] << ", " << phases[idx] << "\n";
            }

            dataFileOutputStream.close();

            wstring message = L"Exported data to file: " + dataFilePath;
            LogMessage(message, SAVE_EXPORT_STATUS);
        }
        else
        {
            wstring message = L"Could not write data file: " + dataFilePath + L".  Check that the directory exists and has proper permissions set.";
            MessageBox( 0, message.c_str(), L"Error", MB_OK );
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SavePlotImageFile
//
// Purpose: Saves the current plot image to a PNG image file
//
// Parameters: dataFilePath: path to the image file.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void SavePlotImageFile( wstring dataFilePath )
{
    ofstream dataFileOutputStream;
    size_t bufferSize;
    unique_ptr<uint8_t[]> buffer;

    if ( !fraPlotter->PlotDataAvailable() )
    {
        MessageBox( 0, L"No plot is available to save.", L"Error", MB_OK );
    }
    else
    {
        dataFileOutputStream.open( dataFilePath.c_str(), ios::out | ios::binary );

        if (dataFileOutputStream)
        {
            try
            {
                buffer = fraPlotter->GetPNGBitmapPlot(&bufferSize);
            }
            catch (runtime_error e)
            {
                wstringstream wss;
                wss << e.what();
                LogMessage( wss.str() );
                return;
            }

            if (buffer && bufferSize)
            {
                dataFileOutputStream.write( (const char*)buffer.get(), bufferSize );

                if (dataFileOutputStream.good())
                {
                    wstring message = L"Saved plot image to file: " + dataFilePath;
                    LogMessage(message, SAVE_EXPORT_STATUS);
                }
                else
                {
                    wstring message = L"Failed to write plot image file: " + dataFilePath + L".  Internal stream error.";
                    MessageBox( 0, message.c_str(), L"Error", MB_OK );
                }
            }
            else
            {
                wstring message = L"Failed to generate plot image data: " + dataFilePath + L".  Internal error.";
                MessageBox( 0, message.c_str(), L"Error", MB_OK );
            }
            dataFileOutputStream.close();
        }
        else
        {
            wstring message = L"Could not write plot image file: " + dataFilePath + L".  Check that the directory exists and has proper permissions set.";
            MessageBox( 0, message.c_str(), L"Error", MB_OK );
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: [Enable|Disable]AllMenus
//
// Purpose: Enable/Disable the main window menus.  Used to disable the menus while the FRA is 
//          running.
//
// Parameters: N/A
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void EnableAllMenus( void )
{
    HMENU hMenu = GetMenu( hMainWnd );
    int numMenuItems = GetMenuItemCount( hMenu );
    for (int i = 0; i < numMenuItems; i++)
    {
        EnableMenuItem( hMenu, i, MF_BYPOSITION | MF_ENABLED );
    }
    DrawMenuBar( hMainWnd );
    HWND hndCtrl = GetDlgItem(hMainWnd, IDC_CUSTOM_BUTTON);
    EnableWindow(hndCtrl, TRUE);
    EnablePlotControls(hMainWnd);
}

void DisableAllMenus( void )
{
    HMENU hMenu = GetMenu( hMainWnd );
    int numMenuItems = GetMenuItemCount( hMenu );
    for (int i = 0; i < numMenuItems; i++)
    {
        EnableMenuItem( hMenu, i, MF_BYPOSITION | MF_GRAYED );
    }
    DrawMenuBar( hMainWnd );
    HWND hndCtrl = GetDlgItem(hMainWnd, IDC_CUSTOM_BUTTON);
    EnableWindow(hndCtrl, FALSE);
    DisablePlotControls(hMainWnd);
}
