//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014, 2015 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: SettingsDialog.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Suppress an unnecessary C4996 warning about use of checked
// iterators invoked from Boost
#if !defined(_SCL_SECURE_NO_WARNINGS)
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <Windowsx.h>
#include <CommCtrl.h>
#include <string>
#include <sstream>
#include "utility.h"
#include "SettingsDialog.h"
#include "PlotTitleDialog.h"
#include "PlotAxesDialog.h"
#include "PicoScopeFraApp.h"
#include "PicoScopeInterface.h"
#include "Resource.h"

const uint8_t numLogVerbosityFlags = 13;

const wchar_t logVerbosityString[numLogVerbosityFlags][128] =
{
    L"Instrument Access Diagnostics",
    L"FRA Progress",
    L"Step Trial Progress",
    L"Signal Generator Diagnostics",
    L"Vertical Resolution Diagnostics",
    L"Autorange Diagnostics",
    L"Adaptive Stimulus Diagnostics",
    L"Sample Processing Diagnostics",
    L"DFT Diagnostics",
    L"Scope Power Events",
    L"Save/Export Status",
    L"FRA Warnings",
    L"PicoScope API Calls"
};

bool logVerbositySelectorOpen = false;
PicoScopeFRA* pFRA = NULL;
PicoScope* pCurrentScope = NULL;
double currentSampleRate = 0.0;
uint32_t currentLowNoiseCycles = 0;
uint32_t currentTimebase = 0;
bool currentSampleRateValid = false;
bool currentLowNoiseCyclesValid = false;
RESOLUTION_T currentDeviceResolution = RESOLUTION_AUTO;
COLORREF noiseRejectTimebaseLabelColor = RGB(0,0,0);
COLORREF bandwidthLabelColor = RGB( 0, 0, 0 );
COLORREF noiseRejectTimebaseControlColor = RGB( 0, 0, 0 );
COLORREF bandwidthControlColor = RGB( 0, 0, 0 );

// Used to override behavior where spin control leaves the buddy control's text selected after it
// increments/decrements.  Doing this so the text color is clear while using the up-down control.
bool spinIncrDecr = false;
LRESULT CALLBACK NoiseRejectTimebaseEditWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
WNDPROC DefaultNoiseRejectTimebaseEditWndProc;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: GetNoiseRejectModeMinFrequency
//
// Purpose: Helper function to print the minimum (start) frequency when in noise reject mode
//
// Parameters: [in] sample Rate - Scope sampling rate as determined by timebase setting
//             [in] timebase - Scope timebase setting
//             [out] return - Formatted minimum start frequency string
//
// Notes: Considers the number of samples available by the scope (another input)
//
///////////////////////////////////////////////////////////////////////////////////////////////////

std::wstring GetNoiseRejectModeMinFrequency(double sampleRate, uint32_t timebase)
{
    std::wstring retVal;

    if (pFRA)
    {
        retVal = PrintFrequency(pFRA->GetMinFrequency(currentDeviceResolution, timebase));
    }
    else
    {
        retVal = L"???";
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: UpdateMinStimulusFrequency
//
// Purpose: Updates the noise reject mode minimum stimulus frequency on the screen
//
// Parameters: [in] hDlg - Handle to the dialog owning the static text control to be updated
//
// Notes: Called in response to the user changing the timebase
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void UpdateMinStimulusFrequency( HWND hDlg )
{
    HWND hndCtrl;
    wchar_t minStimulusFrequencyString[128];

    if (currentSampleRateValid)
    {
        swprintf(minStimulusFrequencyString, 128, L"Noise reject mode minimum\nstimulus frequency: %s",
                 GetNoiseRejectModeMinFrequency(currentSampleRate, currentTimebase).c_str());
    }
    else
    {
        swprintf(minStimulusFrequencyString, 128, L"Noise reject mode minimum\nstimulus frequency: INVALID");
    }
    hndCtrl = GetDlgItem(hDlg, IDC_STATIC_NOISE_REJECT_MINIMUM_STIMULUS_FREQUENCY);
    Static_SetText(hndCtrl, minStimulusFrequencyString);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: UpdateControlColor
//
// Purpose: Updates the color of the Bandwidth and Noise Reject Timebase controls to red to warn
//          that the combination would exceed the scope's buffer size
//
// Parameters: [in] hDlg - Handle to the dialog owning the controls to be updated
//
// Notes: Called in response to the user changing the timebase, bandwidth, or vertical resolution
//
///////////////////////////////////////////////////////////////////////////////////////////////////

void UpdateControlColors( HWND hDlg )
{
    WCHAR numberStr[32];
    HWND hndCtrl;
    HWND hndNoiseRejectLabel;
    HWND hndBandwidthLabel;
    HWND hndNoiseRejectControl;
    HWND hndBandwidthControl;
    COLORREF newColor;
    bool noiseRejectModeBandwidthValid = false;
    bool noiseRejectModeTimebaseValid = false;
    double noiseRejectModeBandwidth = 0.0;
    wstring noiseRejectModeBandwidthStr;
    uint32_t noiseRejectModeTimebase = 0;

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_NOISE_REJECT_BW );
    Edit_GetText( hndCtrl, numberStr, sizeof( numberStr ) / sizeof( WCHAR ) );
    noiseRejectModeBandwidthStr = numberStr;
    if (WStringToDouble( noiseRejectModeBandwidthStr, noiseRejectModeBandwidth ) && noiseRejectModeBandwidth > 0.0)
    {
        noiseRejectModeBandwidthValid = true;
    }

    if (pCurrentScope)
    {
        hndCtrl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
        Edit_GetText( hndCtrl, numberStr, sizeof( numberStr ) / sizeof( WCHAR ) );
        if (WStringToUint32( numberStr, noiseRejectModeTimebase ))
        {
            uint32_t maxTimebase = pCurrentScope->GetMaxTimebase();
            if (noiseRejectModeTimebase <= maxTimebase)
            {
                uint32_t minTimebase;
                if (RESOLUTION_AUTO == currentDeviceResolution)
                {
                    minTimebase = pCurrentScope->GetMinTimebase();
                }
                else
                {
                    minTimebase = pCurrentScope->GetMinTimebaseForResolution( currentDeviceResolution );
                }
                if (noiseRejectModeTimebase >= minTimebase)
                {
                    noiseRejectModeTimebaseValid = true;
                }
            }
        }
    }

    if (noiseRejectModeBandwidthValid && noiseRejectModeTimebaseValid)
    {
        double noiseRejectModeSampleRate;
        uint32_t maxScopeSamplesPerChannel;
        RESOLUTION_T resolution;

        if (RESOLUTION_AUTO == currentDeviceResolution)
        {
            resolution = pCurrentScope->GetMaxResolutionForTimebase( noiseRejectModeTimebase );
        }
        else
        {
            resolution = currentDeviceResolution;
        }

        if (pCurrentScope->GetMaxSamples( &maxScopeSamplesPerChannel, resolution, noiseRejectModeTimebase ) &&
            pCurrentScope->GetFrequencyFromTimebase( noiseRejectModeTimebase, noiseRejectModeSampleRate ))
        {
            if (noiseRejectModeBandwidth < (noiseRejectModeSampleRate / maxScopeSamplesPerChannel))
            {
                newColor = RGB( 255, 0, 0 ); // Red
            }
            else
            {
                newColor = RGB( 0, 0, 0 ); // Black
            }
        }
        else
        {
            newColor = RGB( 255, 0, 0 ); // Red
        }
    }
    else
    {
        newColor = RGB( 255, 0, 0 ); // Red
    }

    noiseRejectTimebaseLabelColor = bandwidthLabelColor = noiseRejectTimebaseControlColor = bandwidthControlColor = newColor;

    hndNoiseRejectLabel = GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_TIMEBASE );
    hndBandwidthLabel = GetDlgItem( hDlg, IDC_STATIC_FRA_NOISE_REJECT_BW );
    hndNoiseRejectControl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
    hndBandwidthControl = GetDlgItem( hDlg, IDC_EDIT_FRA_NOISE_REJECT_BW );

    RedrawWindow( hndNoiseRejectLabel, NULL, NULL, RDW_INVALIDATE );
    RedrawWindow( hndBandwidthLabel, NULL, NULL, RDW_INVALIDATE );
    RedrawWindow( hndNoiseRejectControl, NULL, NULL, RDW_INVALIDATE );
    RedrawWindow( hndBandwidthControl, NULL, NULL, RDW_INVALIDATE );
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ValidateAndStoreSettings
//
// Purpose: Checks settings for validity and stores them if they are valid
//
// Parameters: [in] hDlg: handle to the settings dialog
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ValidateAndStoreSettings( HWND hDlg )
{
    bool retVal = true;
    std::wstringstream errorText;
    WCHAR numberStr[32];
    HWND hndCtrl;
    wstring errorConditions[32];
    uint8_t numErrors = 0;
    bool noiseRejectModeBandwidthValid = false;
    bool noiseRejectModeTimebaseValid = false;

    SamplingMode_T sampleMode = LOW_NOISE;
    bool sweepDescending = false;
    uint16_t extraSettlingTime = 0;
    bool adaptiveStimulusMode = false;
    uint8_t adaptiveStimulusTriesPerStep = 0;
    double adaptiveStimulusTargetTolerance = 0.0;
    wstring adaptiveStimulusTargetToleranceStr;
    bool qualityLimitsEnable = false;
    double amplitudeLowerLimit = 0.0;
    wstring amplitudeLowerLimitStr;
    double purityLowerLimit = 0.0;
    wstring purityLowerLimitStr;
    bool excludeDcFromNoise = false;
    int32_t inputStartRange = 0;
    int32_t outputStartRange = 0;
    uint8_t autorangeTriesPerStep = 0;
    double autorangeTolerance = 0.0;
    wstring autorangeToleranceStr;
    uint16_t lowNoiseCyclesCaptured = 0;
    uint16_t lowNoiseOversampling = 0;
    double noiseRejectModeBandwidth = 0.0;
    wstring noiseRejectModeBandwidthStr;
    uint32_t noiseRejectModeTimebase = 0;
    RESOLUTION_T deviceResolution = RESOLUTION_MIN;
    bool plotRealTime = false;
    double phaseWrappingThreshold = 0.0;
    wstring phaseWrappingThresholdStr;
    double gainMarginPhaseCrossover = 0.0;
    wstring gainMarginPhaseCrossoverStr;
    bool timeDomainDiagnosticPlots = false;
    uint16_t logVerbosityFlags = 0;
    int curSel;

    hndCtrl = GetDlgItem( hDlg, IDC_RADIO_NOISE_REJECT_MODE );
    if (Button_GetCheck( hndCtrl ) ==  BST_CHECKED)
    {
        sampleMode = HIGH_NOISE;
    }
    else
    {
        sampleMode = LOW_NOISE;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SWEEP_DESCENDING );
    sweepDescending = (Button_GetCheck( hndCtrl ) ==  BST_CHECKED);

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_EXTRA_SETTLING_TIME );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    if (!WStringToUint16( numberStr, extraSettlingTime ))
    {
        errorConditions[numErrors++] = L"Extra settling time is not a valid number";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_ADAPTIVE_STIMULUS_ENABLE );
    adaptiveStimulusMode = (Button_GetCheck( hndCtrl ) == BST_CHECKED);

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ADAPTIVE_STIMULUS_TRIES_PER_STEP );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    if (!WStringToUint8( numberStr, adaptiveStimulusTriesPerStep ))
    {
        errorConditions[numErrors++] = L"Adaptive stimulus tries/step is not a valid number";
        retVal = false;
    }
    else if (adaptiveStimulusTriesPerStep < 1)
    {
        errorConditions[numErrors++] = L"Adaptive stimulus tries/step must be >= 1";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ADAPTIVE_STIMULUS_RESPONSE_TARGET_TOLERANCE );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    adaptiveStimulusTargetToleranceStr = numberStr;
    if (!WStringToDouble( adaptiveStimulusTargetToleranceStr, adaptiveStimulusTargetTolerance ))
    {
        errorConditions[numErrors++] = L"Adaptive stimulus target tolerance is not a valid number";
        retVal = false;
    }
    // Allow negative values because they can have meaning

    // Autorange Settings
    if (pCurrentScope)
    {
        hndCtrl = GetDlgItem( hDlg, IDC_COMBO_INPUT_START_RANGE );
        if (CB_ERR != (curSel = ComboBox_GetCurSel(hndCtrl)))
        {
            inputStartRange = ComboBox_GetItemData(hndCtrl, curSel);
        }
        else
        {
            errorConditions[numErrors++] = L"Input start range selection is invalid";
            retVal = false;
        }

        hndCtrl = GetDlgItem( hDlg, IDC_COMBO_OUTPUT_START_RANGE );
        if (CB_ERR != (curSel = ComboBox_GetCurSel(hndCtrl)))
        {
            outputStartRange = ComboBox_GetItemData(hndCtrl, curSel);
        }
        else
        {
            errorConditions[numErrors++] = L"Output start range selection is invalid";
            retVal = false;
        }
    }

    // Device Resolution
    if (pCurrentScope)
    {
        hndCtrl = GetDlgItem( hDlg, IDC_COMBO_VERTICAL_RESOLUTION );

        if (CB_ERR != (curSel = ComboBox_GetCurSel(hndCtrl)))
        {
            deviceResolution = (RESOLUTION_T)ComboBox_GetItemData(hndCtrl, curSel);
        }
        else
        {
            errorConditions[numErrors++] = L"Vertical resolution selection is invalid";
            retVal = false;
        }
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AUTORANGE_TRIES_PER_STEP );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    if (!WStringToUint8( numberStr, autorangeTriesPerStep ))
    {
        errorConditions[numErrors++] = L"Autorange tries/step is not a valid number";
        retVal = false;
    }
    else if (autorangeTriesPerStep < 1)
    {
        errorConditions[numErrors++] = L"Autorange tries/step must be >= 1";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AUTORANGE_TOLERANCE );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    autorangeToleranceStr = numberStr;
    if (!WStringToDouble( autorangeToleranceStr, autorangeTolerance ))
    {
        errorConditions[numErrors++] = L"Autorange tolerance is not a valid number";
        retVal = false;
    }
    else  if (autorangeTolerance <= 0.0)
    {
        errorConditions[numErrors++] = L"Autorange tolerance must be > 0.0";
        retVal = false;
    }

    // FRA Sample Settings
    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_LOW_NOISE_CYCLES_CAPTURED );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    if (!WStringToUint16( numberStr, lowNoiseCyclesCaptured ))
    {
        errorConditions[numErrors++] = L"Minimum cycles captured is not a valid number";
        retVal = false;
    }
    else if (lowNoiseCyclesCaptured < 1)
    {
        errorConditions[numErrors++] = L"Minimum cycles captured must be >= 1";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_LOW_NOISE_OVERSAMPLING );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    if (!WStringToUint16( numberStr, lowNoiseOversampling ))
    {
        errorConditions[numErrors++] = L"Low noise oversampling is not a valid number";
        retVal = false;
    }
    else if (lowNoiseOversampling < 2)
    {
        errorConditions[numErrors++] = L"Low noise oversampling must be >= 2";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_NOISE_REJECT_BW );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    noiseRejectModeBandwidthStr = numberStr;
    if (!WStringToDouble( noiseRejectModeBandwidthStr, noiseRejectModeBandwidth ))
    {
        errorConditions[numErrors++] = L"Noise reject bandwidth is not a valid number";
        retVal = false;
    }
    else if (noiseRejectModeBandwidth <= 0.0)
    {
        errorConditions[numErrors++] = L"Noise reject bandwidth must be > 0.0";
        retVal = false;
    }
    else
    {
        noiseRejectModeBandwidthValid = true;
    }

    if (pCurrentScope)
    {
        hndCtrl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
        Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
        if (!WStringToUint32( numberStr, noiseRejectModeTimebase ))
        {
            errorConditions[numErrors++] = L"Noise reject timebase is not a valid number";
            retVal = false;
        }
        else
        {
            uint32_t maxTimebase = pCurrentScope->GetMaxTimebase();
            if (noiseRejectModeTimebase > maxTimebase)
            {
                errorText.clear();
                errorText.str(L"");
                errorText << L"Noise reject timebase must not be greater than " << maxTimebase;
                errorConditions[numErrors++] = errorText.str().c_str();
                retVal = false;
            }
            else
            {
                uint32_t minTimebase;
                if (RESOLUTION_AUTO == currentDeviceResolution)
                {
                    minTimebase = pCurrentScope->GetMinTimebase();
                }
                else
                {
                    minTimebase = pCurrentScope->GetMinTimebaseForResolution(currentDeviceResolution);
                }
                if (noiseRejectModeTimebase < minTimebase)
                {
                    errorText.clear();
                    errorText.str(L"");
                    errorText << L"Noise reject timebase must not be less than " << minTimebase;
                    errorConditions[numErrors++] = errorText.str().c_str();
                    retVal = false;
                }
                else
                {
                    noiseRejectModeTimebaseValid = true;
                }
            }
        }
    }

    if (HIGH_NOISE == sampleMode && noiseRejectModeBandwidthValid && noiseRejectModeTimebaseValid)
    {
        double noiseRejectModeSampleRate;
        uint32_t maxScopeSamplesPerChannel;
        RESOLUTION_T resolution;

        if (RESOLUTION_AUTO == currentDeviceResolution)
        {
            resolution = pCurrentScope->GetMaxResolutionForTimebase( noiseRejectModeTimebase );
        }
        else
        {
            resolution = currentDeviceResolution;
        }

        if (pCurrentScope->GetMaxSamples(&maxScopeSamplesPerChannel, resolution, noiseRejectModeTimebase) &&
            pCurrentScope->GetFrequencyFromTimebase(noiseRejectModeTimebase, noiseRejectModeSampleRate))
        {
            if (noiseRejectModeBandwidth < (noiseRejectModeSampleRate / maxScopeSamplesPerChannel))
            {
                errorConditions[numErrors++] = L"Combination of bandwidth and sample rate exceed scope's buffer capacity, increase bandwidth or decrease sample rate";
                retVal = false;
            }
        }
    }

    // Quality Limits
    hndCtrl = GetDlgItem( hDlg, IDC_QUALITY_LIMITS_ENABLE );
    qualityLimitsEnable = (Button_GetCheck(hndCtrl) == BST_CHECKED);

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AMPLITUDE_LOWER_QUALITY_LIMIT );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    amplitudeLowerLimitStr = numberStr;
    if (!WStringToDouble( amplitudeLowerLimitStr, amplitudeLowerLimit ))
    {
        errorConditions[numErrors++] = L"Amplitude lower quality limit is not a valid number";
        retVal = false;
    }
    else if (amplitudeLowerLimit < 0.0)
    {
        errorConditions[numErrors++] = L"Amplitude lower quality limit must be >= 0.0";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PURITY_LOWER_QUALITY_LIMIT );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    purityLowerLimitStr = numberStr;
    if (!WStringToDouble( purityLowerLimitStr, purityLowerLimit ))
    {
        errorConditions[numErrors++] = L"Purity lower quality limit is not a valid number";
        retVal = false;
    }
    else if (purityLowerLimit < 0.0)
    {
        errorConditions[numErrors++] = L"Purity lower quality limit must be >= 0.0";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EXCLUDE_DC_FROM_NOISE );
    excludeDcFromNoise = (Button_GetCheck(hndCtrl) == BST_CHECKED );

    // FRA Bode Plot Options
    hndCtrl = GetDlgItem( hDlg, IDC_PLOT_REAL_TIME );
    plotRealTime = (Button_GetCheck( hndCtrl) == BST_CHECKED);

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PHASE_WRAPPING_THRESHOLD );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    phaseWrappingThresholdStr = numberStr;
    if (!WStringToDouble( phaseWrappingThresholdStr, phaseWrappingThreshold ))
    {
        errorConditions[numErrors++] = L"Phase wrapping threshold is not a valid number";
        retVal = false;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_GAIN_MARGIN_PHASE_CROSSOVER );
    Edit_GetText( hndCtrl, numberStr, sizeof(numberStr)/sizeof(WCHAR) );
    gainMarginPhaseCrossoverStr = numberStr;
    if (!WStringToDouble( gainMarginPhaseCrossoverStr, gainMarginPhaseCrossover ))
    {
        errorConditions[numErrors++] = L"Gain margin phase crossover is not a valid number";
        retVal = false;
    }

    // Diagnostic settings
    hndCtrl = GetDlgItem( hDlg, IDC_TIME_DOMAIN_DIAGNOSTIC_PLOTS_ENABLE );
    timeDomainDiagnosticPlots = (Button_GetCheck(hndCtrl) == BST_CHECKED);

    hndCtrl = GetDlgItem( hDlg, IDC_LIST_LOG_VERBOSITY );
    for (int i = 0; i < numLogVerbosityFlags; i++)
    {
        if (ListView_GetCheckState(hndCtrl, i) != 0)
        {
            logVerbosityFlags |= (1 << i);
        }
    }

    if (!retVal)
    {
        uint8_t i;
        wstring errorMessage = L"The following are invalid:\n";
        for (i = 0; i < numErrors-1; i++)
        {
            errorMessage += L"- " + errorConditions[i] + L",\n";
        }
        errorMessage += L"- " + errorConditions[i];

        MessageBox( hDlg, errorMessage.c_str(), L"Error", MB_OK );
    }
    else
    {
        pSettings->SetSamplingMode(sampleMode);
        pSettings->SetSweepDescending(sweepDescending);
        pSettings->SetExtraSettlingTimeMs(extraSettlingTime);
        pSettings->SetAdaptiveStimulusMode(adaptiveStimulusMode);
        pSettings->SetAdaptiveStimulusTriesPerStep(adaptiveStimulusTriesPerStep);
        pSettings->SetTargetResponseAmplitudeTolerance(adaptiveStimulusTargetToleranceStr);
        pSettings->SetInputStartingRange(inputStartRange);
        pSettings->SetOutputStartingRange(outputStartRange);
        pSettings->SetAutorangeTriesPerStep(autorangeTriesPerStep);
        pSettings->SetAutorangeTolerance(autorangeToleranceStr);
        pSettings->SetLowNoiseCyclesCaptured(lowNoiseCyclesCaptured);
        pSettings->SetLowNoiseOversampling(lowNoiseOversampling);
        pSettings->SetNoiseRejectBandwidth(noiseRejectModeBandwidthStr);
        pSettings->SetNoiseRejectModeTimebase(noiseRejectModeTimebase);
        pSettings->SetDeviceResolution(deviceResolution);
        pSettings->SetQualityLimitsState(qualityLimitsEnable);
        pSettings->SetAmplitudeLowerLimit(amplitudeLowerLimitStr);
        pSettings->SetPurityLowerLimit(purityLowerLimitStr);
        pSettings->SetDcExcludedFromNoiseState(excludeDcFromNoise);
        pSettings->SetPlotRealTime(plotRealTime);
        pSettings->SetPhaseWrappingThreshold(phaseWrappingThresholdStr);
        pSettings->SetGainMarginPhaseCrossover(gainMarginPhaseCrossoverStr);
        pSettings->SetTimeDomainPlotsEnabled(timeDomainDiagnosticPlots);
        pSettings->SetLogVerbosityFlags(logVerbosityFlags);
    }

    return retVal;

}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SettingsDialogHandler
//
// Purpose: Dialog procedure for the Settings Dialog.  Handles initialization and user actions.
//
// Parameters: See Windows API documentation
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK SettingsDialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    HWND hndCtrl;
    switch (message)
    {
        case WM_INITDIALOG:
        {
            LVCOLUMN listViewCol;
            LVITEM listViewItem;

            pFRA = (PicoScopeFRA*)lParam;
            if (pFRA)
            {
                pCurrentScope = pFRA->GetInstrument();
            }

            // Replace the window procedure for the Noise Reject Timbase Edit control, so we can override the
            // behavior where spin control leaves the buddy control's text selected after it increments/decrements.
            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
            DefaultNoiseRejectTimebaseEditWndProc = (WNDPROC)SetWindowLongPtrW( hndCtrl, GWL_WNDPROC, (LONG_PTR)NoiseRejectTimebaseEditWndProc );

            if (pCurrentScope)
            {
                // Do this so that we don't have to keep changing resolution later, since
                // changing resolution on FlexRes scopes can cause relay switching.
                // Functions like PicoScope::GetMaxSamples may only work if the timebase passed
                // is compatible with the resolution currently set for the scope.  The lowest
                // resolution of the scope should be compatible with all timebases allowed by
                // channel configuration.
                RESOLUTION_T actualResolution;
                pCurrentScope->SetResolution(RESOLUTION_MIN, actualResolution);
                // Some scopes have timebase dependencies based on which channels are in use.
                // Functions like PicoScope::GetMaxSamples may only work if the timebase passed
                // is compatible with the channels currently set for the scope.
                pCurrentScope->SetChannelConfigurationForMinimumTimebase();
            }

            // FRA Execution Options
            if (HIGH_NOISE == pSettings->GetSamplingMode())
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_NOISE_REJECT_MODE );
                Button_SetCheck( hndCtrl, BST_CHECKED );
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_LOW_NOISE_MODE );
                Button_SetCheck( hndCtrl, BST_CHECKED );
            }

            if (pSettings->GetSweepDescending())
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SWEEP_DESCENDING );
                Button_SetCheck( hndCtrl, BST_CHECKED );
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SWEEP_ASCENDING );
                Button_SetCheck( hndCtrl, BST_CHECKED );
            }

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_EXTRA_SETTLING_TIME );
            Edit_SetText( hndCtrl, pSettings->GetExtraSettlingTimeMsAsString().c_str() );

            // Adaptive Stimulus
            hndCtrl = GetDlgItem( hDlg, IDC_ADAPTIVE_STIMULUS_ENABLE );
            Button_SetCheck( hndCtrl, pSettings->GetAdaptiveStimulusMode() ? BST_CHECKED : BST_UNCHECKED );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ADAPTIVE_STIMULUS_TRIES_PER_STEP );
            Edit_SetText( hndCtrl, pSettings->GetAdaptiveStimulusTriesPerStepAsString().c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ADAPTIVE_STIMULUS_RESPONSE_TARGET_TOLERANCE );
            Edit_SetText( hndCtrl, pSettings->GetTargetResponseAmplitudeToleranceAsString().c_str() );

            // Autorange Settings
            if (pCurrentScope)
            {
                int inputMinRange, inputMaxRange, outputMinRange, outputMaxRange;
                int currentStartingRange;

                pCurrentScope->SetChannelAttenuation((PS_CHANNEL)pSettings->GetInputChannel(), (ATTEN_T)(pSettings->GetInputAttenuation()));
                pCurrentScope->SetChannelAttenuation((PS_CHANNEL)pSettings->GetOutputChannel(), (ATTEN_T)(pSettings->GetOutputAttenuation()));
                inputMinRange = pCurrentScope->GetMinRange((PS_CHANNEL)pSettings->GetInputChannel(), (PS_COUPLING)(pSettings->GetInputCoupling()));
                inputMaxRange = pCurrentScope->GetMaxRange((PS_CHANNEL)pSettings->GetInputChannel(), (PS_COUPLING)(pSettings->GetInputCoupling()));
                outputMinRange = pCurrentScope->GetMinRange((PS_CHANNEL)pSettings->GetOutputChannel(), (PS_COUPLING)(pSettings->GetOutputCoupling()));
                outputMaxRange = pCurrentScope->GetMaxRange((PS_CHANNEL)pSettings->GetOutputChannel(), (PS_COUPLING)(pSettings->GetOutputCoupling()));
                const RANGE_INFO_T* inputRangeInfo = pCurrentScope->GetRangeCaps((PS_CHANNEL)pSettings->GetInputChannel());
                const RANGE_INFO_T* outputRangeInfo = pCurrentScope->GetRangeCaps((PS_CHANNEL)pSettings->GetOutputChannel());

                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_INPUT_START_RANGE );
                ComboBox_AddString( hndCtrl, L"Stimulus" );
                ComboBox_SetItemData( hndCtrl, 0, -1 );
                for (int rangeIndex = inputMinRange; rangeIndex <= inputMaxRange; rangeIndex++)
                {
                    ComboBox_SetItemData( hndCtrl, ComboBox_AddString( hndCtrl, inputRangeInfo[rangeIndex].name ), rangeIndex );
                }

                currentStartingRange = pSettings->GetInputStartingRange();
                if (-1 == currentStartingRange)
                {
                    ComboBox_SetCurSel(hndCtrl, 0);
                }
                else
                {
                    ComboBox_SetCurSel(hndCtrl, currentStartingRange - inputMinRange + 1);
                }

                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_OUTPUT_START_RANGE );
                ComboBox_AddString( hndCtrl, L"Stimulus" );
                ComboBox_SetItemData( hndCtrl, 0, -1 );
                for (int rangeIndex = outputMinRange; rangeIndex <= outputMaxRange; rangeIndex++)
                {
                    ComboBox_SetItemData( hndCtrl, ComboBox_AddString( hndCtrl, outputRangeInfo[rangeIndex].name ), rangeIndex );
                }

                currentStartingRange = pSettings->GetOutputStartingRange();
                if (-1 == currentStartingRange)
                {
                    ComboBox_SetCurSel(hndCtrl, 0);
                }
                else
                {
                    ComboBox_SetCurSel(hndCtrl, currentStartingRange - outputMinRange + 1);
                }
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_INPUT_START_RANGE );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_INPUT_START_RANGE );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_OUTPUT_START_RANGE );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_OUTPUT_START_RANGE );
                EnableWindow( hndCtrl, FALSE );
            }

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AUTORANGE_TRIES_PER_STEP );
            Edit_SetText( hndCtrl, pSettings->GetAutorangeTriesPerStepAsString().c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AUTORANGE_TOLERANCE );
            Edit_SetText( hndCtrl, pSettings->GetAutorangeToleranceAsString().c_str() );

            // FRA Sample Settings
            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_LOW_NOISE_CYCLES_CAPTURED );
            Edit_SetText( hndCtrl, pSettings->GetLowNoiseCyclesCapturedAsString().c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_LOW_NOISE_OVERSAMPLING );
            Edit_SetText( hndCtrl, pSettings->GetLowNoiseOversamplingAsString().c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_FRA_NOISE_REJECT_BW );
            Edit_SetText( hndCtrl, pSettings->GetNoiseRejectBandwidthAsString().c_str() );

            // Scope vertical resolution settings
            if (pCurrentScope)
            {
                const std::vector<RESOLUTION_T> resolutions = pCurrentScope->GetResolutionCaps();

                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_VERTICAL_RESOLUTION );

                ComboBox_AddString( hndCtrl, L"Auto" );
                ComboBox_SetItemData( hndCtrl, 0, RESOLUTION_AUTO );
                for (auto res : resolutions)
                {
                    ComboBox_SetItemData( hndCtrl, ComboBox_AddString( hndCtrl, (pCurrentScope->GetResolutionString(res)).c_str() ), res );
                }

                currentDeviceResolution = pSettings->GetDeviceResolution();
                std::wstring selResStr;

                // Auto is really only meaningful for FlexRes scopes
                if (RESOLUTION_AUTO == currentDeviceResolution && resolutions.size() >= 2)
                {
                    selResStr = L"Auto";
                }
                else
                {
                    selResStr = pCurrentScope->GetResolutionString(currentDeviceResolution);
                }
                RESOLUTION_T fr = RESOLUTION_AUTO;
                ComboBox_SelectItemData(hndCtrl, 0, (LPARAM)selResStr.c_str());

                if (resolutions.size() < 2) // It's not a FlexRes scope, so don't give the appearance that something is adjustable
                {
                    hndCtrl = GetDlgItem( hDlg, IDC_COMBO_VERTICAL_RESOLUTION );
                    EnableWindow( hndCtrl, FALSE );

                    hndCtrl = GetDlgItem( hDlg, IDC_STATIC_VERTICAL_RESOLUTION );
                    EnableWindow( hndCtrl, FALSE );
                }
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_COMBO_VERTICAL_RESOLUTION );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_VERTICAL_RESOLUTION );
                EnableWindow( hndCtrl, FALSE );
            }

            if (pCurrentScope)
            {
                uint32_t minTimebase;
                hndCtrl = GetDlgItem( hDlg, IDC_UPDOWN_NOISE_REJECT_TIMEBASE );
                SendMessage( hndCtrl, UDM_SETBUDDY, (WPARAM)GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE ), 0L );

                currentTimebase = pSettings->GetNoiseRejectModeTimebaseAsInt();
                if (RESOLUTION_AUTO == currentDeviceResolution)
                {
                    minTimebase = pCurrentScope->GetMinTimebase();
                }
                else
                {
                    minTimebase = pCurrentScope->GetMinTimebaseForResolution(currentDeviceResolution);
                }

                // Up/Down (Spin) controls with buddies only support signed 32 bit range.
                // That OK because it's contradictory to have very large timebases in noise reject mode anyway.
                SendMessage( hndCtrl, UDM_SETRANGE32, minTimebase, min((uint32_t)(std::numeric_limits<int32_t>::max)(), pCurrentScope->GetMaxTimebase()) );
                SendMessage( hndCtrl, UDM_SETPOS32, 0L, currentTimebase );
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
                EnableWindow( hndCtrl, FALSE );
                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_TIMEBASE );
                EnableWindow( hndCtrl, FALSE );
                hndCtrl = GetDlgItem( hDlg, IDC_UPDOWN_NOISE_REJECT_TIMEBASE );
                EnableWindow( hndCtrl, FALSE );
                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_SAMPLING_RATE );
                EnableWindow( hndCtrl, FALSE );
                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_VERTICAL_RESOLUTION );
                EnableWindow( hndCtrl, FALSE );
                hndCtrl = GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_MINIMUM_STIMULUS_FREQUENCY );
                EnableWindow( hndCtrl, FALSE );
            }

            // Quality Limits
            hndCtrl = GetDlgItem( hDlg, IDC_QUALITY_LIMITS_ENABLE );
            Button_SetCheck( hndCtrl, pSettings->GetQualityLimitsState() ? BST_CHECKED : BST_UNCHECKED );
            EnableWindow( hndCtrl, FALSE );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_AMPLITUDE_LOWER_QUALITY_LIMIT );
            Edit_SetText( hndCtrl, pSettings->GetAmplitudeLowerLimitAsString().c_str() );
            EnableWindow( hndCtrl, FALSE );

            hndCtrl = GetDlgItem( hDlg, IDC_STATIC_AMPLITUDE_LOWER_QUALITY_LIMIT );
            EnableWindow( hndCtrl, FALSE );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PURITY_LOWER_QUALITY_LIMIT );
            Edit_SetText( hndCtrl, pSettings->GetPurityLowerLimitAsString().c_str() );
            EnableWindow( hndCtrl, FALSE );

            hndCtrl = GetDlgItem( hDlg, IDC_STATIC_PURITY_LOWER_QUALITY_LIMIT );
            EnableWindow( hndCtrl, FALSE );

            hndCtrl = GetDlgItem( hDlg, IDC_EXCLUDE_DC_FROM_NOISE );
            Button_SetCheck( hndCtrl, pSettings->GetDcExcludedFromNoiseState() ? BST_CHECKED : BST_UNCHECKED );
            EnableWindow( hndCtrl, FALSE );

            // FRA Bode Plot Options
            hndCtrl = GetDlgItem( hDlg, IDC_PLOT_REAL_TIME );
            Button_SetCheck( hndCtrl, pSettings->GetPlotRealTime() ? BST_CHECKED : BST_UNCHECKED );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PHASE_WRAPPING_THRESHOLD );
            Edit_SetText( hndCtrl, pSettings->GetPhaseWrappingThresholdAsString().c_str() );

            hndCtrl = GetDlgItem( hDlg, IDC_EDIT_GAIN_MARGIN_PHASE_CROSSOVER );
            Edit_SetText( hndCtrl, pSettings->GetGainMarginPhaseCrossoverAsString().c_str() );

            // Diagnostic settings
            hndCtrl = GetDlgItem( hDlg, IDC_TIME_DOMAIN_DIAGNOSTIC_PLOTS_ENABLE );
            Button_SetCheck( hndCtrl, pSettings->GetTimeDomainPlotsEnabled() ? BST_CHECKED : BST_UNCHECKED );

            hndCtrl = GetDlgItem( hDlg, IDC_LIST_LOG_VERBOSITY );
            ListView_SetExtendedListViewStyleEx( hndCtrl, LVS_EX_CHECKBOXES, LVS_EX_CHECKBOXES );

            listViewCol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH | LVCF_SUBITEM;
            listViewCol.pszText = L"";
            listViewCol.fmt = LVCFMT_LEFT;
            listViewCol.cx = 100; // Initial value does not matter because we're going to auto-size
            listViewCol.iSubItem = 0;
            ListView_InsertColumn( hndCtrl, 0, &listViewCol );

            listViewItem.mask = LVIF_TEXT;
            listViewItem.iSubItem = 0;
            for (int i = 0; i < numLogVerbosityFlags; i++)
            {
                listViewItem.iItem = i;
                listViewItem.pszText = (LPWSTR)logVerbosityString[i];
                ListView_InsertItem( hndCtrl, &listViewItem );
                if (pSettings->GetLogVerbosityFlag((LOG_MESSAGE_FLAGS_T)(1 << i)))
                {
                    ListView_SetCheckState(hndCtrl, i, TRUE);
                }
            }

            ListView_SetColumnWidth( hndCtrl, 0, LVSCW_AUTOSIZE );

            // Set initial state to closed
            SetWindowPos( hndCtrl, 0, 0, 0, LOWORD(ListView_ApproximateViewRect( hndCtrl, -1, -1, numLogVerbosityFlags )), 0, SWP_NOZORDER | SWP_NOMOVE);
            logVerbositySelectorOpen = false;

            return (INT_PTR)TRUE;
            break;
        }
        case WM_NOTIFY:
        {
            // Don't let items be selected in the log verbosity list, and toggle check when the item is clicked (even outside the checkbox)
            NMHDR* pNMHDR = ((NMHDR*)lParam);
            if (IDC_LIST_LOG_VERBOSITY == pNMHDR->idFrom &&
                LVN_ITEMCHANGED == pNMHDR->code)
            {
                NMLISTVIEW* pNMLV = ((NMLISTVIEW*)lParam);
                if (((pNMLV->uChanged & LVIF_STATE) == LVIF_STATE) && ((pNMLV->uNewState & LVIS_SELECTED) == LVIS_SELECTED))
                {
                    ListView_SetItemState(pNMHDR->hwndFrom, pNMLV->iItem, 0, LVIS_SELECTED);
                    ListView_SetCheckState( pNMHDR->hwndFrom, pNMLV->iItem, !ListView_GetCheckState(pNMHDR->hwndFrom, pNMLV->iItem) );
                }
            }
            else if (IDC_UPDOWN_NOISE_REJECT_TIMEBASE == pNMHDR->idFrom)
            {
                // Signal to indicate an update to the buddy edit control was from using the up-down (spin) control.
                spinIncrDecr = true;
            }
            return (INT_PTR)TRUE;
            break;
        }
        case WM_COMMAND:
        {
            switch (LOWORD(wParam))
            {
                case IDOK:
                {
                    if (ValidateAndStoreSettings(hDlg))
                    {
                        EndDialog(hDlg, IDOK);
                    }
                    break;
                }
                case IDCANCEL:
                {
                    EndDialog(hDlg, IDCANCEL);
                    break;
                }
                case IDC_BUTTON_LOG_VERBOSITY:
                {
                    if (!logVerbositySelectorOpen)
                    {
                        RECT rect;
                        rect.left = 7;
                        MapDialogRect(hDlg, &rect);
                        hndCtrl = GetDlgItem( hDlg, IDC_LIST_LOG_VERBOSITY );
                        DWORD lvDims = ListView_ApproximateViewRect( hndCtrl, -1, -1, numLogVerbosityFlags );
                        SetWindowPos( hndCtrl, 0, 0, 0, LOWORD(lvDims)+rect.left, HIWORD(lvDims), SWP_NOZORDER | SWP_NOMOVE );
                        logVerbositySelectorOpen = true;
                    }
                    else
                    {
                        hndCtrl = GetDlgItem( hDlg, IDC_LIST_LOG_VERBOSITY );
                        SetWindowPos( hndCtrl, 0, 0, 0, LOWORD(ListView_ApproximateViewRect( hndCtrl, -1, -1, numLogVerbosityFlags )), 0, SWP_NOZORDER | SWP_NOMOVE);
                        logVerbositySelectorOpen = false;
                    }
                    break;
                }
                case IDC_EDIT_FRA_LOW_NOISE_CYCLES_CAPTURED:
                {
                    if (EN_CHANGE == HIWORD(wParam))
                    {
                        wchar_t newLowNoiseCyclesText[16];
                        Edit_GetText((HWND)lParam, newLowNoiseCyclesText, 16);
                        if (wcslen(newLowNoiseCyclesText))
                        {
                            currentLowNoiseCycles = _wtol(newLowNoiseCyclesText);
                            if (currentLowNoiseCycles >= 1)
                            {
                                currentLowNoiseCyclesValid = true;
                            }
                            else
                            {
                                currentLowNoiseCyclesValid = false;
                            }
                        }
                        else
                        {
                            currentLowNoiseCyclesValid = false;
                        }

                        if (pCurrentScope)
                        {
                            UpdateMinStimulusFrequency( hDlg );
                        }
                    }
                    break;
                }
                case IDC_EDIT_NOISE_REJECT_TIMEBASE:
                {
                    if (EN_CHANGE == HIWORD(wParam))
                    {
                        uint32_t newTimebase;
                        double newFrequency;
                        wchar_t newTimebaseText[16];
                        wchar_t sampleRateDisplayString[128];
                        wchar_t verticalResolutionDisplayString[128];
                        Edit_GetText((HWND)lParam, newTimebaseText, 16);
                        if (wcslen(newTimebaseText))
                        {
                            RESOLUTION_T newResolution;
                            newTimebase = _wtol(newTimebaseText);
                            wstring resolutionString;

                            currentTimebase = newTimebase;

                            if (RESOLUTION_AUTO == currentDeviceResolution)
                            {
                                if (pCurrentScope->GetFrequencyFromTimebase(newTimebase, newFrequency))
                                {
                                    newResolution = pCurrentScope->GetMaxResolutionForTimebase(newTimebase);
                                    swprintf(sampleRateDisplayString, 128, L"Noise reject sample rate: %s", PrintFrequency(newFrequency).c_str());
                                    resolutionString = pCurrentScope->GetResolutionString(newResolution);
                                    swprintf(verticalResolutionDisplayString, 128, L"Noise reject vertical resolution: %s", resolutionString.c_str());
                                    currentSampleRate = newFrequency;
                                    currentSampleRateValid = true;
                                }
                                else
                                {
                                    currentSampleRateValid = false;
                                }
                            }
                            else
                            {
                                // First check to see if the timebase is valid for the fixed resolution.
                                if (newTimebase >= pCurrentScope->GetMinTimebaseForResolution(currentDeviceResolution))
                                {
                                    if (pCurrentScope->GetFrequencyFromTimebase(newTimebase, newFrequency))
                                    {
                                        swprintf(sampleRateDisplayString, 128, L"Noise reject sample rate: %s", PrintFrequency(newFrequency).c_str());
                                        resolutionString = pCurrentScope->GetResolutionString(currentDeviceResolution);
                                        swprintf(verticalResolutionDisplayString, 128, L"Noise reject vertical resolution: %s", resolutionString.c_str());
                                        currentSampleRate = newFrequency;
                                        currentSampleRateValid = true;
                                    }
                                    else
                                    {
                                        currentSampleRateValid = false;
                                    }
                                }
                                else
                                {
                                    currentSampleRateValid = false;
                                }
                            }
                        }
                        else
                        {
                            currentSampleRateValid = false;
                        }
                        if (!currentSampleRateValid)
                        {
                            swprintf(sampleRateDisplayString, 128, L"Noise reject sample rate: INVALID");
                            swprintf(verticalResolutionDisplayString, 128, L"Noise reject vertical resolution: INVALID");
                        }
                        hndCtrl = GetDlgItem(hDlg, IDC_STATIC_NOISE_REJECT_SAMPLING_RATE);
                        Static_SetText(hndCtrl, sampleRateDisplayString);
                        hndCtrl = GetDlgItem(hDlg, IDC_STATIC_NOISE_REJECT_VERTICAL_RESOLUTION);
                        Static_SetText(hndCtrl, verticalResolutionDisplayString);

                        UpdateMinStimulusFrequency( hDlg );
                        UpdateControlColors( hDlg );
                    }
                    break;
                }
                case IDC_EDIT_FRA_NOISE_REJECT_BW:
                {
                    if (EN_CHANGE == HIWORD( wParam ))
                    {
                        UpdateControlColors( hDlg );
                    }
                    break;
                }
                case IDC_COMBO_VERTICAL_RESOLUTION:
                {
                    if (CBN_SELCHANGE == HIWORD(wParam))
                    {
                        int curSel;
                        uint32_t minTimebase;

                        if (CB_ERR != (curSel = ComboBox_GetCurSel((HWND)lParam)))
                        {
                            currentDeviceResolution = (RESOLUTION_T)ComboBox_GetItemData((HWND)lParam, curSel);
                        }

                        if (RESOLUTION_AUTO == currentDeviceResolution)
                        {
                            minTimebase = pCurrentScope->GetMinTimebase();
                        }
                        else
                        {
                            minTimebase = pCurrentScope->GetMinTimebaseForResolution(currentDeviceResolution);
                        }

                        hndCtrl = GetDlgItem( hDlg, IDC_UPDOWN_NOISE_REJECT_TIMEBASE );
                        // Change bounds on the spin control
                        SendMessage( hndCtrl, UDM_SETRANGE32, minTimebase, min((uint32_t)(std::numeric_limits<int32_t>::max)(), pCurrentScope->GetMaxTimebase()) );
                        // Adjust timebase if the new resolution is not supported by the current one
                        currentTimebase = max(currentTimebase, pCurrentScope->GetMinTimebaseForResolution(currentDeviceResolution));
                        SendMessage( hndCtrl, UDM_SETPOS32, 0L, currentTimebase );

                        // Force a EN_UPDATE on IDC_EDIT_NOISE_REJECT_TIMEBASE to update the static control text fields in case the timebase wasn't changed
                        hndCtrl = GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE );
                        WPARAM wp = MAKEWPARAM(IDC_EDIT_NOISE_REJECT_TIMEBASE, EN_CHANGE);
                        SendMessage(hDlg, WM_COMMAND, wp, (LPARAM)hndCtrl);

                        UpdateControlColors( hDlg );
                    }
                    break;
                }
                case IDC_BUTTON_AXIS_SETTINGS:
                {
                    PlotScaleSettings_T plotSettings;
                    DWORD dwDlgResp;

                    plotSettings.freqAxisScale = pSettings->GetFreqScale();
                    plotSettings.gainAxisScale = pSettings->GetGainScale();
                    plotSettings.phaseAxisScale = pSettings->GetPhaseScale();
                    plotSettings.freqAxisIntervals = pSettings->GetFreqIntervals();
                    plotSettings.gainAxisIntervals = pSettings->GetGainIntervals();
                    plotSettings.phaseAxisIntervals = pSettings->GetPhaseIntervals();
                    plotSettings.gainMasterIntervals = pSettings->GetGainMasterIntervals();
                    plotSettings.phaseMasterIntervals = pSettings->GetPhaseMasterIntervals();

                    dwDlgResp = DialogBoxParam(NULL, MAKEINTRESOURCE(IDD_AXES_DIALOG), hDlg, PlotAxesDialogHandler, (LPARAM)&plotSettings);

                    if (IDOK == dwDlgResp)
                    {
                        pSettings->SetFreqScale(plotSettings.freqAxisScale);
                        pSettings->SetGainScale(plotSettings.gainAxisScale);
                        pSettings->SetPhaseScale(plotSettings.phaseAxisScale);
                        pSettings->SetFreqIntervals(plotSettings.freqAxisIntervals);
                        pSettings->SetGainIntervals(plotSettings.gainAxisIntervals);
                        pSettings->SetPhaseIntervals(plotSettings.phaseAxisIntervals);
                        pSettings->SetGainMasterIntervals(plotSettings.gainMasterIntervals);
                        pSettings->SetPhaseMasterIntervals(plotSettings.phaseMasterIntervals);
                    }
                    break;
                }
                case IDC_BUTTON_PLOT_TITLE:
                {
                    (void)DialogBoxParam( NULL, MAKEINTRESOURCE( IDD_PLOT_TITLE ), hDlg, PlotTitleDialogHandler, NULL );
                    break;
                }
                default:
                    break;
            }
            return (INT_PTR)TRUE;
            break;
        }
        case WM_CTLCOLORSTATIC:
        {
            if ((HWND)lParam == GetDlgItem( hDlg, IDC_STATIC_NOISE_REJECT_TIMEBASE ))
            {
                SetBkMode( (HDC)wParam, TRANSPARENT );
                SetTextColor( (HDC)wParam, noiseRejectTimebaseLabelColor );
                return (INT_PTR)GetSysColorBrush( COLOR_3DFACE );
            }
            else if ((HWND)lParam == GetDlgItem( hDlg, IDC_STATIC_FRA_NOISE_REJECT_BW ))
            {
                SetBkMode( (HDC)wParam, TRANSPARENT );
                SetTextColor( (HDC)wParam, bandwidthLabelColor );
                return (INT_PTR)GetSysColorBrush( COLOR_3DFACE );
            }
            else
            {
                return (INT_PTR)NULL;
            }
            break;
        }
        case WM_CTLCOLOREDIT:
        {
            if ((HWND)lParam == GetDlgItem( hDlg, IDC_EDIT_NOISE_REJECT_TIMEBASE ))
            {
                SetBkMode( (HDC)wParam, TRANSPARENT );
                SetTextColor( (HDC)wParam, noiseRejectTimebaseControlColor );
                return (INT_PTR)GetSysColorBrush( COLOR_WINDOW );
            }
            else if ((HWND)lParam == GetDlgItem( hDlg, IDC_EDIT_FRA_NOISE_REJECT_BW ))
            {
                SetBkMode( (HDC)wParam, TRANSPARENT );
                SetTextColor( (HDC)wParam, bandwidthControlColor );
                return (INT_PTR)GetSysColorBrush( COLOR_WINDOW );
            }
            else
            {
                return (INT_PTR)NULL;
            }
            break;
        }
        default:
            return (INT_PTR)FALSE;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: NoiseRejectTimebaseEditWndProc
//
// Purpose: Window procedure for the Noise Reject Timebase Edit control.
//
// Parameters: See Windows API documentation
//
// Notes: Used to override behavior where spin control leaves the buddy control's text selected
//        after it increments/decrements.  Doing this so the text color is clear while using the
//        up-down control.
//
///////////////////////////////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK NoiseRejectTimebaseEditWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
    if (spinIncrDecr && EM_SETSEL == msg) // Block selection update by doing nothing
    {
        spinIncrDecr = false; // Clear this so user can directly select text
        return FALSE;
    }
    else // Pass along all other messages to original default edit control window procedure
    {
        return CallWindowProcW( DefaultNoiseRejectTimebaseEditWndProc, hWnd, msg, wParam, lParam );
    }
}
