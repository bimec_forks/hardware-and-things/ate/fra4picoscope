//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2020 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: SignalGeneratorDialog.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

// Suppress an unnecessary C4996 warning about use of checked
// iterators invoked from Boost
#if !defined(_SCL_SECURE_NO_WARNINGS)
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <Windowsx.h>
#include <Shlwapi.h>
#include <CommCtrl.h>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include "utility.h"
#include "SignalGeneratorDialog.h"
#include "PicoScopeFraApp.h"
#include "PicoScopeInterface.h"
#include "Resource.h"

UseExtSigGen_T originalUseExtSigGen = SIG_GEN_BUILT_IN;
std::wstring originalEsgID;
std::wstring originalPluginName;
std::wstring originalInitString;

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ValidateAndStoreSignalGeneratorSettings
//
// Purpose: Checks settings for validity and stores them if they are valid
//
// Parameters: [in] hDlg: handle to the settings dialog
//             [out] bChanged: whether the settings were changed
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ValidateAndStoreSignalGeneratorSettings( HWND hDlg, bool& bChanged )
{
    bool retVal = true;
    WCHAR settingsStr[MAX_PATH+64];
    HWND hndCtrl;
    WCHAR szAppPath[MAX_PATH];
    WCHAR szPluginPath[MAX_PATH];
    wstring errorConditions[32];
    uint8_t numErrors = 0;

    UseExtSigGen_T useExtSigGen;
    std::wstring esgID;
    std::wstring pluginName;
    std::wstring initString;

    hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SG_BUILT_IN );
    if (Button_GetCheck( hndCtrl ) ==  BST_CHECKED)
    {
        useExtSigGen = SIG_GEN_BUILT_IN;
    }
    else
    {
        useExtSigGen = SIG_GEN_EXTERNAL;
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PLUGIN_NAME );
    Edit_GetText( hndCtrl, settingsStr, sizeof(settingsStr)/sizeof(WCHAR) );
    pluginName = settingsStr;

    if (SIG_GEN_EXTERNAL == useExtSigGen)
    {
        // Validate that the plugin file exists
        if (0 == GetModuleFileName( NULL, szAppPath, MAX_PATH ))
        {
            errorConditions[numErrors++] = L"Could not find plugin file.";
            retVal = false;
        }
        else
        {
            if (0 == PathRemoveFileSpec( szAppPath ))
            {
                errorConditions[numErrors++] = L"Could not find plugin file.";
                retVal = false;
            }
            else
            {
                wsprintf(szPluginPath, L"%s\\%s.dll", szAppPath, pluginName.c_str());
                WIN32_FIND_DATA FindFileData;
                if (INVALID_HANDLE_VALUE == FindFirstFile(szPluginPath, &FindFileData))
                {
                    WCHAR szErrorText[64+MAX_PATH];
                    wsprintf(szErrorText, L"Could not find plugin file.  Expecting: %s", szPluginPath);
                    errorConditions[numErrors++] = szErrorText;
                    retVal = false;
                }
            }
        }
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_ID );
    Edit_GetText( hndCtrl, settingsStr, sizeof(settingsStr)/sizeof(WCHAR) );
    esgID = settingsStr;
    if (esgID == L"")
    {
        esgID = L"Generic";
    }

    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_INIT_STRING );
    Edit_GetText( hndCtrl, settingsStr, sizeof(settingsStr)/sizeof(WCHAR) );
    initString = settingsStr;

    if (!retVal)
    {
        uint8_t i;
        wstring errorMessage = L"The following are invalid:\n";
        for (i = 0; i < numErrors-1; i++)
        {
            errorMessage += L"- " + errorConditions[i] + L",\n";
        }
        errorMessage += L"- " + errorConditions[i];

        MessageBox( hDlg, errorMessage.c_str(), L"Error", MB_OK );
    }
    else
    {
        pSettings->SetSignalGeneratorSource(useExtSigGen);
        if (SIG_GEN_EXTERNAL == useExtSigGen)
        {
            pSettings->SetMostRecentSignalGeneratorPlugin(pluginName.c_str());
            pSettings->SetMostRecentSignalGeneratorID(esgID.c_str());
            // Re-read in case it's a different signal generator than what's currently open
            pSettings->ReadEsgSettings();
            pSettings->SetEsgSettings(initString.c_str());
            pSettings->WriteEsgSettings();
        }
    }

    bChanged = (originalUseExtSigGen != useExtSigGen ||
                (useExtSigGen == SIG_GEN_EXTERNAL && (originalPluginName != pluginName ||
                originalEsgID != esgID || originalInitString != initString)));

    return retVal;

}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: SignalGeneratorDialogHandler
//
// Purpose: Dialog procedure for the Signal Generator Settings Dialog.  Handles initialization and
//          user actions.
//
// Parameters: See Windows API documentation
//
// Notes: N/A
//
///////////////////////////////////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK SignalGeneratorDialogHandler(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    HWND hndCtrl;
    switch (message)
    {
        case WM_INITDIALOG:
        {
            originalEsgID = L"";
            originalPluginName = L"";

            originalUseExtSigGen = pSettings->GetSignalGeneratorSource();
            if (SIG_GEN_BUILT_IN == originalUseExtSigGen)
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SG_BUILT_IN );
                Button_SetCheck( hndCtrl, BST_CHECKED );

                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PLUGIN_NAME );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_ID );
                EnableWindow( hndCtrl, FALSE );

                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_INIT_STRING );
                EnableWindow( hndCtrl, FALSE );
            }
            else
            {
                hndCtrl = GetDlgItem( hDlg, IDC_RADIO_SG_EXTERNAL );
                Button_SetCheck( hndCtrl, BST_CHECKED );

                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PLUGIN_NAME );
                originalPluginName = pSettings->GetMostRecentSignalGeneratorPlugin();
                Edit_SetText( hndCtrl, originalPluginName.c_str() );

                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_ID );
                originalEsgID = pSettings->GetMostRecentSignalGeneratorID();
                if (originalEsgID == L"Generic")
                {
                    Edit_SetText( hndCtrl, L"" );
                }
                else
                {
                    Edit_SetText( hndCtrl, originalEsgID.c_str() );
                }

                originalInitString = pSettings->GetEsgSettings().c_str();
                hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_INIT_STRING );
                Edit_SetText( hndCtrl, originalInitString.c_str() );
            }

            return (INT_PTR)TRUE;
            break;
        }
        case WM_NOTIFY:
        {
            return (INT_PTR)TRUE;
            break;
        }
        case WM_COMMAND:
        {
            switch (LOWORD(wParam))
            {
                case IDOK:
                {
                    bool bChanged;
                    if (ValidateAndStoreSignalGeneratorSettings(hDlg, bChanged))
                    {
                        if (bChanged)
                        {
                            EndDialog(hDlg, ID_CHANGED);
                        }
                        else
                        {
                            EndDialog(hDlg, IDOK);
                        }
                    }
                    break;
                }
                case IDCANCEL:
                {
                    EndDialog(hDlg, IDCANCEL);
                    break;
                }
                case IDC_RADIO_SG_BUILT_IN:
                {
                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PLUGIN_NAME );
                    Edit_SetText( hndCtrl, L"" );
                    EnableWindow( hndCtrl, FALSE );

                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_ID );
                    Edit_SetText( hndCtrl, L"" );
                    EnableWindow( hndCtrl, FALSE );

                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_INIT_STRING );
                    Edit_SetText( hndCtrl, L"" );
                    EnableWindow( hndCtrl, FALSE );
                    break;
                }
                case IDC_RADIO_SG_EXTERNAL:
                {
                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_PLUGIN_NAME );
                    EnableWindow( hndCtrl, TRUE );
                    Edit_SetText( hndCtrl, (pSettings->GetMostRecentSignalGeneratorPlugin()).c_str() );

                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_ID );
                    EnableWindow( hndCtrl, TRUE );
                    std::wstring esgID = pSettings->GetMostRecentSignalGeneratorID();
                    if (esgID == L"Generic")
                    {
                        Edit_SetText( hndCtrl, L"" );
                    }
                    else
                    {
                        Edit_SetText( hndCtrl, esgID.c_str() );
                    }

                    hndCtrl = GetDlgItem( hDlg, IDC_EDIT_ESG_INIT_STRING );
                    EnableWindow( hndCtrl, TRUE );
                    if (pSettings->ReadEsgSettings(false))
                    {
                        Edit_SetText( hndCtrl, (pSettings->GetEsgSettings()).c_str() );
                    }
                    else
                    {
                        Edit_SetText( hndCtrl, L"" );
                    }

                    break;
                }
                default:
                    break;
            }
            return (INT_PTR)TRUE;
            break;
        }
        default:
            return (INT_PTR)FALSE;
    }
}
