//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014, 2015 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: ps4000aImpl.cpp
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "utility.h"
#include "ps4000aApi.h"
#include "picoStatus.h"
#include "ps4000aImpl.h"
#include "StatusLog.h"

#define PS4000A_IMPL
#include "psCommonImpl.cpp"

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps4000aImpl::GetTimebase
//
// Purpose: Get a timebase from a desired frequency, rounding such that the frequency is at least
//          as high as requested, if possible.
//
// Parameters: [in] desiredFrequency: caller's requested frequency in Hz
//             [in] resolution: the vertical resolution to consider; can use RESOLUTION_CURRENT
//             [out] actualFrequency: the frequency corresponding to the returned timebase.
//             [out] timebase: the timebase that will achieve the requested freqency or greater
//
// Notes:
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps4000aImpl::GetTimebase( double desiredFrequency, RESOLUTION_T resolution, double* actualFrequency, uint32_t* timebase )
{
    bool retVal = false;

    if (RESOLUTION_CURRENT == resolution)
    {
        resolution = (RESOLUTION_T)currentResolution;
    }

    if (desiredFrequency != 0.0 && actualFrequency && timebase)
    {
        if (model == PS4824 || model == PS4224A || model == PS4424A || model == PS4824A)
        {
            *timebase = saturation_cast<uint32_t,double>((80.0e6/desiredFrequency) - 1.0); // ps4000apg.en r5: p17
            retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
        }
        else if (model == PS4444)
        {
            if (desiredFrequency > 50.0e6)
            {
                *timebase = saturation_cast<uint32_t,double>(log(400.0e6/desiredFrequency) / M_LN2); // ps4000apg.en r5: p17 (with correction for typo); log2(n) implemented as log(n)/log(2)
            }
            else
            {
                *timebase = saturation_cast<uint32_t,double>((50.0e6/desiredFrequency) + 2.0); // ps4000apg.en r5: p17
            }
            switch (resolution)
            {
                case PS4000A_DR_14BIT:
                    *timebase = max(*timebase,3);
                    break;
                case PS4000A_DR_12BIT:
                    *timebase = max(*timebase,1);
                    break;
                default:
                    break;
            }
            retVal = GetFrequencyFromTimebase(*timebase, *actualFrequency);
        }
    }
    return retVal;
}

bool ps4000aImpl::GetFrequencyFromTimebase(uint32_t timebase, double &frequency)
{
    bool retVal = false;
    if (timebase >= minTimebase && timebase <= maxTimebase)
    {
        if (model == PS4824 || model == PS4224A || model == PS4424A || model == PS4824A)
        {
            frequency = 80.0e6 / ((double)(timebase + 1)); // ps4000apg.en r5: p17
            retVal = true;
        }
        else if (model == PS4444)
        {
            if (timebase <= 3)
            {
                frequency = 400.0e6 / (double)(1<<(timebase)); // ps4000apg.en r5: p17 (with correction for typo)
            }
            else
            {
                frequency = 50.0e6 / ((double)(timebase-2));
            }
            retVal = true;
        }
    }
    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: ps4000aImpl::InitializeScope
//
// Purpose: Initialize scope/family-specific implementation details.
//
// Parameters: N/A
//
// Notes: Assumes scope was opened with max resolution
//
///////////////////////////////////////////////////////////////////////////////////////////////////

bool ps4000aImpl::InitializeScope(void)
{
    bool retVal = false;
    wstringstream scopeStatusText;
    PS_RANGE minRange, maxRange;

    timebaseNoiseRejectMode = defaultTimebaseNoiseRejectMode = 0;

    minTimebase = model == PS4444 ? 1 : 0;
    maxTimebase = (std::numeric_limits<uint32_t>::max)();

    // Map is in decending order of min timebase
    // Only for PS4444
    flexResMinTbMap = {{PS4000A_DR_14BIT, 3},
                       {PS4000A_DR_12BIT, 1}};

    currentResolution = maxResolution = model == PS4444 ? PS4000A_DR_14BIT : PS4000A_DR_12BIT;

    // Even though the DDS parameters would suggest that a lower frequency is possible,
    // the API limits to this value.
    minFuncGenFreq = PS4000A_MIN_FREQUENCY;

    signalGeneratorPrecision = 80.0e6 / (double)UINT32_MAX;

    minRange = (PS_RANGE)PS4000A_10MV;
    maxRange = (PS_RANGE)PS4000A_50V;

    // Save the value set by the scope factory (ScopeSelector)
    numActualChannels = numAvailableChannels;

    if (PICO_POWER_SUPPLY_NOT_CONNECTED == initialPowerState ||
        PICO_USB3_0_DEVICE_NON_USB3_0_PORT == initialPowerState)
    {
        scopeStatusText << L"ps4000aChangePowerSource( " << handle << ", " << initialPowerState << L" )";
        LogMessage( scopeStatusText.str(), PICO_API_CALL );
        ps4000aChangePowerSource(handle, initialPowerState);
        numAvailableChannels = 2;
    }
    else
    {
        numAvailableChannels = numActualChannels;
    }

    for (uint8_t chan = PS_CHANNEL_A; chan < numActualChannels; chan++)
    {
        channelNonSmartMinRanges[chan] = channelMinRanges[chan] = minRange;
        channelNonSmartMaxRanges[chan] = channelMaxRanges[chan] = maxRange;
    }

    if (retVal = AllocateBuffers())
    {
        retVal = InitProbeEventHandling();
    }

    return retVal;
}