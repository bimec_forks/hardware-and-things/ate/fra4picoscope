//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: psDemoImpl.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PicoScopeInterface.h"
#include <vector>
#include <boost/align/aligned_allocator.hpp>

typedef enum enPSDemoRange
{
    PSDEMO_10MV,
    PSDEMO_20MV,
    PSDEMO_50MV,
    PSDEMO_100MV,
    PSDEMO_200MV,
    PSDEMO_500MV,
    PSDEMO_1V,
    PSDEMO_2V,
    PSDEMO_5V,
    PSDEMO_10V,
    PSDEMO_20V,
    PSDEMO_50V,
    PSDEMO_MAX_RANGES
} PSDEMO_RANGE;

#define PSDEMO_MAX_VALUE 32767
#define PSDEMO_MIN_VALUE -32767

class psDemoImpl : public PicoScope
{
    public:
        psDemoImpl( int16_t _handle );
        ~psDemoImpl();
        void SetSimulatedLpFCornerFrequency( double cornerFrequency );
        static bool open;
    private:
        static const uint8_t simdLanesSine;
        static const uint8_t simdLanesVolts;
        static const uint8_t simdLanesSamples;
        static const uint32_t ddsTableSize;
        double currentChannelADdsPhase;
        double currentChannelBDdsPhase;
        double simulatedLpfCornerFrequency;
        float currentChannelBGain;
        double currentSigGenVpp;
        double currentSigGenOffset;
        double currentSigGenOffsetChannelA;
        double currentSigGenOffsetChannelB;
        double currentSigGenFrequency;
        double currentSamplingFrequency;
        std::vector<PS_COUPLING> currentChannelCoupling;
        std::vector<PS_RANGE> currentChannelRange;
        std::vector<double> currentChannelOffset;
        std::vector<PS_COUPLING> simDataChannelCouplingBasis;
        std::vector<PS_RANGE> simDataChannelRangeBasis;
        std::vector<float> simDataChannelOffsetBasis;
        uint32_t simDataTimebaseBasis;
        double simDataSigGenVppBasis;
        double simDataSigGenOffsetBasis;
        double simDataSigGenOffsetBasisChannelA;
        double simDataSigGenOffsetBasisChannelB;
        double simDataSigGenFrequencyBasis;
        uint32_t sineDataSimdIterations;
        uint32_t voltsDataSimdIterations;
        uint32_t samplesDataSimdIterations;
        bool channelAOv, channelBOv;
        int16_t channelAMinSample, channelAMaxSample, channelBMinSample, channelBMaxSample;
        uint32_t currentBlockNum;
        uint32_t numSamplesInCurrentBlock;

        std::vector<float> ddsTable;
        std::vector<int16_t, boost::alignment::aligned_allocator<int16_t,16>> channelASamples;
        std::vector<int16_t, boost::alignment::aligned_allocator<int16_t,16>> channelBSamples;
        std::vector<float, boost::alignment::aligned_allocator<float,32>> channelASines;
        std::vector<float, boost::alignment::aligned_allocator<float,32>> channelAVolts;
        std::vector<float, boost::alignment::aligned_allocator<float,32>> channelBSines;
        std::vector<float, boost::alignment::aligned_allocator<float,32>> channelBVolts;

        bool recomputeSines;
        bool recomputeVolts;
        bool recomputeChannelASamples;
        bool recomputeChannelBSamples;

        void GenerateDDSTable( void );
        void GenerateSines( void );
        void GenerateVolts( void );
        void GenerateSamples( bool genChannelA, bool genChannelB );
        void UpdateTransferFunction( void );
        static DWORD WINAPI SimulateCapture( LPVOID lpdwThreadParam );
        HANDLE hSimulatedCaptureThread;
        HANDLE hStartCaptureEvent;
        HANDLE hCaptureTimer;
        psBlockReady lpReady;
        void *pParameter;

#include "psCommonImpl.h"
};