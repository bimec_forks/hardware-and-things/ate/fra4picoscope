//////////////////////////////////////////////////////////////////////////////////////////////////
//
// Frequency Response Analyzer for PicoScope
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the Frequency Response Analyzer for PicoScope program.
//
// Frequency Response Analyzer for PicoScope is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// Frequency Response Analyzer for PicoScope is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Frequency Response Analyzer for PicoScope.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: utility.h
//
//////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "stdafx.h"
#include <locale>
#include <codecvt>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <stdint.h>
#include <boost/numeric/conversion/cast.hpp> 
#include <limits>
#include <immintrin.h>
#include <thread>
#include <queue>
#include <mutex>
#include <atomic>
#include <condition_variable>

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: WStringTo[type]
//
// Purpose: Strictly convert a string to a number, while indicating whether the string was properly
//          formatted.
//
// Parameters: [in] myString: The string to convert
//             [out] d/i/u: The resulting number
//             [out] return: Whether the conversion succeeded.
//
// Notes: 
//
///////////////////////////////////////////////////////////////////////////////////////////////////

inline bool WStringToDouble( wstring myString, double& d )
{
    wstringstream wss(myString);
    wss >> noskipws >> d; // Consider leading whitespace invalid
    // Valid only if whole string was consumed and neither failbit nor badbit is set
    return (wss.eof() && !wss.fail());
}

inline bool WStringToInt16( wstring myString, int16_t& i )
{
    wstringstream wss(myString);
    wss >> noskipws >> i; // Consider leading whitespace invalid
    // Valid only if whole string was consumed and neither failbit nor badbit is set
    return (wss.eof() && !wss.fail());
}

inline bool WStringToUint16( wstring myString, uint16_t& i )
{
    wstringstream wss(myString);
    wss >> noskipws >> i; // Consider leading whitespace invalid
                          // Valid only if whole string was consumed and neither failbit nor badbit is set
    return (wss.eof() && !wss.fail());
}

inline bool WStringToInt32( wstring myString, int32_t& i )
{
    wstringstream wss(myString);
    wss >> noskipws >> i; // Consider leading whitespace invalid
                          // Valid only if whole string was consumed and neither failbit nor badbit is set
    return (wss.eof() && !wss.fail());
}

inline bool WStringToUint32( wstring myString, uint32_t& i )
{
    wstringstream wss(myString);
    wss >> noskipws >> i; // Consider leading whitespace invalid
                          // Valid only if whole string was consumed and neither failbit nor badbit is set
    return (wss.eof() && !wss.fail());
}

inline bool WStringToUint8( wstring myString, uint8_t& u )
{
    // Need to use a uint16_t because extract operator for a uint8_t will
    // treat it as a character extract
    uint16_t u16;
    wstringstream wss(myString);
    wss >> noskipws >> u16; // Consider leading whitespace invalid
    // Valid only if whole string was consumed and neither failbit nor badbit is set
    if (wss.eof() && !wss.fail())
    {
        // Valid only if it can fit in a uint8_t
        if (u16 <= UINT8_MAX)
        {
            u = (uint8_t)u16;
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: s2ws / ws2s
//
// Purpose: Converts string to wide string or wide string to string
//
// Parameters: [in] str/wstr - the string to convert
//             [out] return - the converted string
//
// Notes: https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
//
///////////////////////////////////////////////////////////////////////////////////////////////////

inline std::wstring s2ws(const std::string& str)
{
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    return converterX.from_bytes(str);
}

inline std::string ws2s(const std::wstring& wstr)
{
    using convert_typeX = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_typeX, wchar_t> converterX;

    return converterX.to_bytes(wstr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PrintFrequency
//
// Purpose: Prints frequnecy to a string with fixed precision and standard units (GHz, MHz, kHz)
//
// Parameters: [in] freq - frequency to print
//             [out] return - formatted result
//
// Notes: None
//
///////////////////////////////////////////////////////////////////////////////////////////////////

inline std::wstring PrintFrequency(double freq)
{
    std::wstring units;
    std::wstringstream valueSS;
    std::wstring valueStr;

    if (freq >= 999.5e6)
    {
        freq /= 1.0e9;
        units = L" GHz";
    }
    else if (freq >= 999.5e3)
    {
        freq /= 1.0e6;
        units = L" MHz";
    }
    else if (freq >= 999.5)
    {
        freq /= 1.0e3;
        units = L" kHz";
    }
    else
    {
        units = L" Hz";
    }

    // Output using fixed precision
    valueSS.precision(3);
    valueSS << std::fixed << freq;
    valueStr = valueSS.str();

    // Smash trailing zeros right of a decimal point
    if (string::npos != valueStr.find(L"."))
    {
        boost::algorithm::trim_right_if(valueStr, boost::algorithm::is_any_of(L"0"));
        // If there's a decimal point remaining on the end, it needs to be stripped too
        boost::algorithm::trim_right_if(valueStr, boost::algorithm::is_any_of(L"."));
    }
    // Finally, correct for possibility of "-0"
    if (0 == valueStr.compare(L"-0"))
    {
        valueStr = L"0";
    }

    valueStr += units;

    return (valueStr);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: saturation_cast<Target,Source>
//
// Purpose: Perform a numerical cast with Boost numeric_cast, but bound the results to fit into the
//          destination type
//
// Parameters: [in] src: The source number to cast
//             [out] return: The result of the cast
//
// Notes: From http://stackoverflow.com/questions/4424168/using-boost-numeric-cast with minor mods
//
///////////////////////////////////////////////////////////////////////////////////////////////////

using boost::numeric_cast;
using boost::numeric::bad_numeric_cast;
using boost::numeric::positive_overflow;
using boost::numeric::negative_overflow;

template<typename Target, typename Source>
Target saturation_cast(Source src) 
{
    try 
    {
        return numeric_cast<Target>(src);
    }
    catch (const negative_overflow &e) 
    {
        UNREFERENCED_PARAMETER(e);
        // Use of parens to avoid conflict with min macro
        return (numeric_limits<Target>::min)();
    }
    catch (const positive_overflow &e) 
    {
        UNREFERENCED_PARAMETER(e);
        // Use of parens to avoid conflict with max macro
        return (numeric_limits<Target>::max)();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: PlatformSupportsAVX
//
// Purpose: Determine whether the current CPU supports the AVX instruction set.
//
// Parameters: [out] return: Whether the CPU supports the AVX instruction set.
//
// Notes: Adapted from https://stackoverflow.com/questions/6121792/how-to-check-if-a-cpu-supports-the-sse3-instruction-set
//        Has to test operating system support
//
///////////////////////////////////////////////////////////////////////////////////////////////////

inline bool PlatformSupportsAVX(void)
{
    bool avxSupported = false;

    int cpuInfo[4];
    __cpuidex(cpuInfo, 1, 0);

    bool osUsesXSAVE_XRSTORE = cpuInfo[2] & (1 << 27) || false;
    bool cpuAVXSuport = cpuInfo[2] & (1 << 28) || false;

    if (osUsesXSAVE_XRSTORE && cpuAVXSuport)
    {
        unsigned long long xcrFeatureMask = _xgetbv(_XCR_XFEATURE_ENABLED_MASK);
        avxSupported = (xcrFeatureMask & 0x6) == 0x6;
    }
    return avxSupported;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Name: MessageProcessor Class
//
// Purpose: Provide a class that supports a simple worker thread to process messages in a queue
//
// Parameters: Template Type T - the type of message to process
//
// Notes: Adaptation of https://github.com/endurodave/StdWorkerThread
//
///////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
class MessageProcessor
{
    public:
        typedef void (*MsgProc_T)(T& msg);

        // Constructor
        MessageProcessor() : m_thread(nullptr), m_msg_proc(nullptr), m_stop(false)
        {
        }

        // Destructor
        ~MessageProcessor()
        {
            ExitThread();
        }

        // Sets the function that processes messages and creates the worker thread if it hasn't been created yet
        void SetProcessor( MsgProc_T msg_proc )
        {
            if (!m_thread)
            {
                m_thread = std::unique_ptr<std::thread>( new std::thread( &MessageProcessor::Process, this ) );
            }
            m_msg_proc = msg_proc;
        }

        // Add a message to the thread queue
        void PostMsg( T* msg )
        {
            // Add msg to queue and notify worker thread
            std::unique_lock<std::mutex> lk( m_mutex );
            m_queue.push( *msg );
            m_cv.notify_one();
        }

    private:
        MessageProcessor( const MessageProcessor& ) = delete;
        MessageProcessor& operator=( const MessageProcessor& ) = delete;

        /// Entry point for the worker thread
        void Process()
        {
            while (!m_stop)
            {
                T msg;
                {
                    // Wait for a message to be added to the queue
                    std::unique_lock<std::mutex> lk( m_mutex );
                    while (!m_stop && m_queue.empty())
                    {
                        m_cv.wait( lk );
                    }

                    if (m_queue.empty())
                    {
                        continue;
                    }

                    msg = m_queue.front();
                    m_queue.pop();
                }
                if (m_msg_proc)
                {
                    m_msg_proc( msg );
                }
            }
            m_stop = false;
        }

        // Called to exit the worker thread
        void ExitThread()
        {
            if (m_thread)
            {
                m_stop = true;
                m_cv.notify_one();

                for (int i = 0; i < 5; i++) // Give the thread up to 500 ms to stop
                {
                    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
                    if (!m_stop)
                    {
                        m_thread->join();
                        m_thread = nullptr;
                        return;
                    }
                }

                // Give up and terminate it anyway
                m_thread->detach();
                m_thread = nullptr;
            }
        }

        std::unique_ptr<std::thread> m_thread;
        std::queue<T> m_queue;
        std::mutex m_mutex;
        std::condition_variable m_cv;
        MsgProc_T m_msg_proc;
        std::atomic<bool> m_stop;
};